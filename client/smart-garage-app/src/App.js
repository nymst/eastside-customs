/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from "react";
import "./App.css";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Header from "./components/Base/Header/Header";
import AuthContext, { getUser } from "./providers/AuthContext";
import Footer from "./components/Base/Footer/Footer";
import Home from "./components/Pages/Home/Home";
import Operations from "./components/Pages/Operations/Operations";
import GuardedRoute from "./providers/GuardedRoute";
import Login from "./components/Auth/Login/Login";
import ResetPassword from "./components/Auth/Login/ResetPassword";
import Brands from "./components/Pages/BrandsPage/Brands";
import ContactUs from "./components/Pages/ContactUs Page/ContactUs/ContactUs";
import Services from "./components/Pages/ServicesPage/Services";
import UsersAccount from "./components/Users/UsersAccount/UsersAccount";
import Order from "./components/Orders/SingleOrder/Order";
import AddUpdateService from "./components/Pages/ServicesPage/AddUpdateService.js/AddUpdateService";
import NotFound from "./components/Pages/NotFound/NotFound";

function App() {
  const [authValue, setAuthValue] = useState({
    user: getUser(),
    isLoggedIn: Boolean(getUser()),
  });

  return (
    <BrowserRouter>
      <AuthContext.Provider value={{ ...authValue, setAuthValue }}>
        <Header />
        <Switch>
          <Redirect path="/" exact to="/home" />
          <Route
            path="/login"
            isLoggedIn={!authValue.isLoggedIn}
            exact
            component={Login}
          />

          <Route path="/reset" exact component={ResetPassword} />
          <Route
            path="/contact_us"
            isLoggedIn={authValue.isLoggedIn}
            exact
            component={ContactUs}
          />
          <GuardedRoute
            path="/orders/:id"
            isLoggedIn={authValue.isLoggedIn}
            exact
            component={Order}
          />
          <GuardedRoute
            path="/user"
            isLoggedIn={authValue.isLoggedIn}
            exact
            component={UsersAccount}
          />
          <GuardedRoute
            path="/operations"
            isLoggedIn={
              authValue.isLoggedIn && authValue.user.role === "employee"
            }
            component={Operations}
          />
          <GuardedRoute
            path="/services/add"
            isLoggedIn={
              authValue.isLoggedIn && authValue.user.role === "employee"
            }
            component={AddUpdateService}
          />
          <GuardedRoute
            path="/services/:id/edit"
            isLoggedIn={
              authValue.isLoggedIn && authValue.user.role === "employee"
            }
            component={AddUpdateService}
          />
          <Route path="/home" exact component={Home} />
          <Route path="/brands" component={Brands} />
          <Route path="/services" component={Services} />
          <Route path="*" exact component={NotFound} />
        </Switch>
        <Footer />
      </AuthContext.Provider>
    </BrowserRouter>
  );
}

export default App;
