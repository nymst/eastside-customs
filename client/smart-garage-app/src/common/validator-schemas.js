import * as yup from 'yup';

export const loginSchema = yup.object().shape({
  email: yup.string().email('Please enter a valid email').required('This field is required'),
  password: yup.string().min(8, "Password should be at least 8 symbols long").required('This field is required')
});

export const forgottenPassSchema = yup.object().shape({
  email: yup.string().email('Please enter a valid email').required('This field is required')
});
export const resetPasswordSchema = yup.object().shape({
  password: yup.string().min(8, "Password should be at least 8 symbols long").required('This field is required'),
  passwordConfirmation: yup.mixed().test(
    "match",
    "Passwords do not match",
    function () {
      return this.parent.password === this.parent.passwordConfirmation;
    }
  ).required('This field is required')
});

export const changePasswordSchema = yup.object().shape({
  oldPassword: yup.string().min(8, "Password should be at least 8 symbols long").required('This field is required'),
  password: yup.string().min(8, "Password should be at least 8 symbols long").required('This field is required'),
  passwordConfirmation: yup.mixed().test(
    "match",
    "Passwords do not match",
    function () {
      return this.parent.password === this.parent.passwordConfirmation;
    }
  ).required('This field is required')
});

export const serviceSchema = yup.object().shape({
  name: yup
  .string()
  .min(8, "This field requires 8 symbols or more")
  .max(100, "Exceeding length limits").required('This field is required'),
  price: yup.number().required('This field is required'),
  description: yup
  .string()
  .min(3, "This field requires 3 symbols or more")
  .max(500, "Exceeding length limits").required('This field is required'),
});
