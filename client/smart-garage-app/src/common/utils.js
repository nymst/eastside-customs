import { getToken } from "../providers/AuthContext";
import { BASE_URL } from "./constants";

export const getAllServicesNoAuth = () => {
  return fetch(`${BASE_URL}/services`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  })
};
export const getAllServicesAuth = () => {
  return fetch(`${BASE_URL}/services`, {
    headers: {
      Authorization: `Bearer ${getToken()}`,
      "Content-type": "application/json"
    },
    method: "GET",
  })
};

export const calculateTotal = (carClass, sum) => {
  const classes = {
    A: 1.05,
    B: 1.15,
    C: 1.30,
    D: 1.50,
    E: 1.65,
    F: 1.8,
    S: 2
  }

  if (classes.hasOwnProperty(carClass)) {
    return (sum * classes[carClass]).toFixed(2);
  }

  return sum;
}
