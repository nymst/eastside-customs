import { createContext } from "react";

const BrandContext = createContext({
  brand: null,
  setBrandState: () => {},
});

export default BrandContext;

