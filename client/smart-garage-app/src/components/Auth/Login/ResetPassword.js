/* eslint-disable react/prop-types */
import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from "mdbreact";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { resetPasswordSchema } from "../../../common/validator-schemas";
import { BASE_URL } from "../../../common/constants";
import { useHistory } from "react-router";
import { errorToast, successToast } from "../../Toasters/Toasters";
import { ToastContainer } from "react-toastify";

const ResetPassword = ({ match }) => {
  const password = {
    password: "",
  };
  const params = new URLSearchParams(window.location.search);
  const token = params.get("id");
  const history = useHistory();

  const reset = (password, { setSubmitting }) => {
    fetch(`${BASE_URL}/auth/reset?id=${token}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(password),
    })
      .then((response) => response.json())
      .then((result) => {
        if (
          result.message === "The link has expired. Please request a new one."
        ) {
          errorToast(result.message, {
            onOpen: () => setSubmitting(true),
            onClose: () => history.push("/login"),
          });
        } else {
          successToast("Your password has been successfully updated.", {
            onOpen: () => setSubmitting(true),
            onClose: () => history.push("/login"),
          });
        }
      });
  };

  return (
    <MDBContainer className="container-reset-password">
      <ToastContainer />
      <MDBRow>
        <MDBCol md="6">
          <Formik
            validationSchema={resetPasswordSchema}
            initialValues={password}
            onSubmit={reset}
          >
            {(props) => (
              <Form>
                <p className="h5 text-center mb-4">Enter your new password</p>
                <div className="grey-text">
                  <Field
                    as={MDBInput}
                    label="Type your new password"
                    icon="lock"
                    name="password"
                    type="password"
                    disabled={props.isSubmitting}
                  >
                    <div className="validator-error">
                      <ErrorMessage name="password" style={{ color: "red" }} />
                    </div>
                  </Field>

                  <Field
                    as={MDBInput}
                    label="Confirm your new password"
                    icon="lock"
                    name="passwordConfirmation"
                    type="password"
                    disabled={props.isSubmitting}
                  >
                    <div className="validator-error">
                      <ErrorMessage
                        name="passwordConfirmation"
                        style={{ color: "red" }}
                      />
                    </div>
                  </Field>
                </div>
                .
                <div className="text-center">
                  <MDBBtn type="submit" disabled={props.isSubmitting}>
                    Confirm
                  </MDBBtn>
                </div>
              </Form>
            )}
          </Formik>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
};

export default ResetPassword;
