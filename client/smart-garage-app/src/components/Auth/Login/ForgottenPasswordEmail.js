/* eslint-disable react/prop-types */
import "./Login.css";
import React, { useState } from "react"
import { Formik, Form, Field, ErrorMessage } from 'formik';
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBIcon,
  MDBInput,
} from "mdbreact";
import ClearIcon from '@material-ui/icons/Clear';
import { forgottenPassSchema } from "../../../common/validator-schemas";
import { BASE_URL } from "../../../common/constants";
import { useHistory } from "react-router";
import { successToast, errorToast } from "../../Toasters/Toasters";

const ForgottenPasswordEmail = () => {
  const [open, setOpen] = useState(true);
  const history = useHistory();
  const email = {
    email: ''
  };

  const requestForgottenPass = (values, { setSubmitting }) => {
    fetch(`${BASE_URL}/auth/forgot`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(values),
    })
      .then(response => response.json())
      .then(result => {
        if (result.message) {
          errorToast(result.message, {
            onOpen: () => setSubmitting(true),
            onClose: () => setSubmitting(false),
          })
        } else {
          successToast('A reset-password link has been sent to your email', {
            onOpen: () => setSubmitting(true),
            onClose: () => history.push('/home'),
          })
        }
      })
  }

  return (
    open ?
      <MDBContainer className="container-reset-password-email" icon={<ClearIcon />}>
        <button className="button-reset-password-email" onClick={() => setOpen(false)} icon={<ClearIcon />}>{<ClearIcon />}</button>
        <MDBRow>
          <MDBCol md="6">
            <Formik initialValues={email} validationSchema={forgottenPassSchema} onSubmit={requestForgottenPass}>
              {props => (
                <Form>
                  <p className="h5 text-center mb-4">Reset Your Password</p>

                  <p className="text-center mb-4">
                    {" "}
              Please enter your email and we'll send you a link to reset
              your password
                  </p>

                  <div className="grey-text">
                    <Field
                      as={MDBInput}
                      label="Your email"
                      icon="envelope"
                      name="email"
                      type="email"
                      disabled={props.isSubmitting}>

                      <div className="validator-error">
                        <ErrorMessage name="email" style={{ color: "red" }} />
                      </div>

                    </Field>
                  </div>

                  <div className="text-center">
                    <MDBBtn outline color="secondary" type="submit" disabled={props.isSubmitting}>
                      Send
                  <MDBIcon far icon="paper-plane" className="ml-1" />
                    </MDBBtn>
                  </div>

                </Form>
              )}
            </Formik>
          </MDBCol>
        </MDBRow>
      </MDBContainer >
      : ""

  );
};

export default ForgottenPasswordEmail;
