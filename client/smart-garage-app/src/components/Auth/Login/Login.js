/* eslint-disable react/prop-types */
import React, { useContext, useState } from "react";
import "./Login.css";
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from "mdbreact";
import { MDBModalFooter } from "mdb-react-ui-kit";
import { Link, useHistory } from "react-router-dom";
import ResetForgottenPassword from "./ForgottenPasswordEmail";
import AuthContext from "../../../providers/AuthContext";
import jwtDecode from "jwt-decode";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { ToastContainer } from "react-toastify";
import { loginSchema } from "../../../common/validator-schemas";
import { BASE_URL } from "../../../common/constants";
import { errorToast } from "../../Toasters/Toasters";


const Login = () => {
  const [clicked, setClicked] = useState(false);
  const auth = useContext(AuthContext);
  const history = useHistory();
  const user = {
    email: "",
    password: "",
  };

  const clickedButton = () => {
    clicked ? setClicked(false) : setClicked(true);
  };

  const login = (values, { setSubmitting }) => {
    fetch(`${BASE_URL}/auth/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(values),
    })
      .then((r) => r.json())
      .then(({ token }) => {
        try {
          const user = jwtDecode(token);
          localStorage.setItem("token", token);
          auth.setAuthValue({ user, isLoggedIn: true });
          history.push("/user");
        } catch (error) {
          throw error;
        }
      })
      .catch((e) => {
        if (e.message === "Invalid token specified") {
          errorToast("Invalid login credentials! Please try again.", {
            onOpen: () => setSubmitting(true),
            onClose: () => setSubmitting(false),
          });
          setSubmitting(false);
        }
      });
  };

  return (
    <MDBContainer className="container-login">
      <ToastContainer />
      <MDBRow>
        <MDBCol md="6">
          <Formik
            initialValues={user}
            validationSchema={loginSchema}
            onSubmit={login}
          >
            {(props) => (
              <Form>
                <p className="h5 text-center mb-4">Sign in</p>

                <div className="grey-text">
                  <Field
                    as={MDBInput}
                    className="email-input"
                    type="email"
                    name="email"
                    label="Type your email"
                    placeholder="Type your email"
                    icon="envelope"
                    disabled={props.isSubmitting}
                  >
                    <div className="validator-error">
                      <ErrorMessage name="email" style={{ color: "red" }} />
                    </div>
                  </Field>

                  <Field
                    className="pass-input"
                    as={MDBInput}
                    type="password"
                    name="password"
                    label="Type your password"
                    placeholder="Type your password"
                    icon="lock"
                    disabled={props.isSubmitting}
                  >
                    <div className="validator-error">
                      <ErrorMessage name="password" style={{ color: "red" }} />
                    </div>
                  </Field>
                </div>

                <div className="text-center">
                  <MDBBtn type="submit" disabled={props.isSubmitting}>
                    Login
                  </MDBBtn>
                </div>
              </Form>
            )}
          </Formik>

          <MDBModalFooter>
            <div className="font-weight-light">
              <Link onClick={clickedButton}>Forgot your password?</Link>
            </div>
          </MDBModalFooter>
        </MDBCol>
      </MDBRow>
      {clicked ? <ResetForgottenPassword /> : ""}
    </MDBContainer>
  );
};

export default Login;
