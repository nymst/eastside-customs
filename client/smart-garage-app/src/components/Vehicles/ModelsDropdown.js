/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { BASE_URL } from "../../common/constants";
import { getToken } from "../../providers/AuthContext";
import { useEffect, useState } from "react";
import makeAnimated from 'react-select/animated';
import Select from 'react-select'

const ModelsDropdown = ({ row }) => {
  const animatedComponents = makeAnimated();
  const [options, setOptions] = useState([]);
  const [selectedModel, setSelectedModel] = useState({});

  useEffect(() => {
    const changedBrandId = row?.rowData.brand?.id;
    const defaultBrandId = row?.rowData?.brand_id;

    fetch(`${BASE_URL}/models/brands/${changedBrandId ?? defaultBrandId}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json"
      },
      method: "GET",
    })
      .then(response => response.json())
      .then(result => {
        setSelectedModel(result.find((el) => el.id === row.rowData.models_id))
        setOptions(result)
      });
  }, [row?.rowData.brand]);

  return (
    <Select
      value={selectedModel}
      getOptionLabel={(option) => option.name}
      getOptionValue={(option) => option}
      onChange={(value) => { 
        row.onChange(value.name) 
        setSelectedModel(value);
      }}
      placeholder="Select model"
      components={animatedComponents}
      name="models"
      options={options}
      className="basic-single"
      classNamePrefix="select"
    />
  )
};

export default ModelsDropdown;
