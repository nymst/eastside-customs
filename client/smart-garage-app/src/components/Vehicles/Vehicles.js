import MaterialTable from "material-table";
import React, { useState, useEffect } from "react";
import { ToastContainer } from "react-toastify";
import { BASE_URL } from "../../common/constants";
import { tableIcons } from "../../common/material-icons";
import { getToken } from "../../providers/AuthContext";
import BrandsDropdown from "./BrandsDropdown";
import ModelsDropdown from "./ModelsDropdown";
import UsersDropdown from "./UsersDropdown";
import { motion } from "framer-motion"
import { errorToast, successToast } from "../Toasters/Toasters";

const Vehicles = () => {
  const [vehicles, setVehicles] = useState([]);
  const [isClicked, setIsClicked] = useState(false);

  const columns = [
    {
      title: "Customer",
      field: "full_name",
      editable: 'onAdd',
      editComponent: (props) => <UsersDropdown row={props} />,
    },
    { title: "Plate", field: "reg_plate" },
    { title: "Year", field: "year" },
    {
      title: "Class",
      field: "class",
      lookup: { A: "A", B: "B", C: "C", D: "D", E: "E", F: "F", S: "S" },
    },
    {
      title: "Brand",
      field: "brand",
      editComponent: (props) => <BrandsDropdown row={props} />,
    },
    {
      title: "Model",
      field: "model",
      editComponent: (props) => <ModelsDropdown row={props} />,
    },
    {
      title: "VIN",
      field: "vin",
    },
    {
      title: "Transmissions",
      field: "transmissions",
      lookup: { manual: "manual", auto: "auto" },
    },
    {
      title: "Engine",
      field: "engine",
      lookup: { petrol: "petrol", diesel: "diesel", hybrid: "hybrid" },
    },
  ];

  const getAllVehicles = () => {
    fetch(`${BASE_URL}/vehicles`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-Type": "application/json",
      },
      method: "GET",
    })
      .then((response) => response.json())
      .then((result) => {
        if (Array.isArray(result)) {
          setVehicles(result);
        } else {
          return;
        }
      })
  };

  const deleteVehicle = (id) => {
    fetch(`${BASE_URL}/vehicles/${id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json",
      },
      method: "DELETE",
    })
      .then((response) => response.json())
      .then(() => setIsClicked(true));
  };

  const updateVehicle = (id, updateData) => {
    fetch(`${BASE_URL}/vehicles/${id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json",
      },
      method: "PUT",
      body: JSON.stringify(updateData),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.errors) {
          if (result.errors.year) {
            errorToast('Vehicle not updated! Invalid year!', { ...null })
          } else if (result.errors.vin) {
            errorToast('Vehicle not updated! Expected a valid BG 17 symbols VIN!', { ...null })
          } else if (result.errors.reg_plate) {
            errorToast(`'Vehicle not updated! ${result.errors.reg_plate}`, { ...null })
          }
        }
        successToast('Vehicle updated successfully!', { ...null })
        setIsClicked(true)
      });
  };

  const createVehicle = (createData) => {
    fetch(`${BASE_URL}/vehicles`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(createData),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.errors) {
          if (result.errors.year) {
            return errorToast('Vehicle not created! Invalid year!', { ...null })
          } else if (result.errors.vin) {
            return errorToast('Vehicle not created! Expected a valid BG 17 symbols VIN!', { ...null })
          } else if (result.errors.reg_plate) {
            return errorToast(`Vehicle not created! ${result.errors.reg_plate}`, { ...null })
          }
        }
        successToast('Vehicle created successfully!', { ...null })
        setIsClicked(true)
      });
  };

  useEffect(() => {
    getAllVehicles();
  }, [isClicked]);

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ delay: 0.3, duration: 0.35 }}
      >

        <ToastContainer />
        <MaterialTable
          icons={tableIcons}
          title="Vehicles"
          data={vehicles}
          columns={columns}
          options={{
            tableLayout: 'auto',
            filtering: "true",
            addRowPosition: 'first',
            padding: 'dense',
            pageSize: 10,
            doubleHorizontalScroll: false,
            headerStyle: {
              width: 26,
              whiteSpace: "nowrap",
              textAlign: "center",
              flexDirection: "row",
              overflow: "hidden",
              textOverflow: "ellipsis",
              paddingRight: 2,
              backgroundColor: "#DC3D24",
              color: "whitesmoke",
              fontWeight: "bold",
              fontSize: "18px",
              letterSpacing: "1px",
              paddingLeft: "25px"
            },
            cellStyle: {
              textAlign: "center",
              letterSpacing: "0.5px",
            },
            rowStyle: { backgroundColor: "#E5E5E5" },
          }
          }
          editable={{
            onRowDelete: (selectedRow) =>
              new Promise((resolve, reject) => {
                setTimeout(() => {
                  const index = selectedRow.id;
                  const updatedRows = [...vehicles];
                  updatedRows.splice(index, 1);
                  deleteVehicle(index);
                  setVehicles(updatedRows);
                  setIsClicked(false);

                  resolve();
                }, 1000);
              }),

            onRowUpdate: (updatedRow, oldRow) =>
              new Promise((resolve, reject) => {
                const updateOrderData = {
                  reg_plate: updatedRow.reg_plate,
                  year: updatedRow.year.toString(),
                  class: updatedRow.class,
                  vin: updatedRow.vin,
                  transmissions: updatedRow.transmissions,
                  engine: updatedRow.engine,
                  model: updatedRow.model,
                  brand: updatedRow.brand.id ? updatedRow.brand.name : updatedRow.brand,
                };

                setTimeout(() => {
                  updateVehicle(oldRow.id, updateOrderData);
                  setIsClicked(false);

                  resolve();
                }, 1000);
              }),

            onRowAdd: (newRow) =>
              new Promise((resolve, reject) => {
                const createVehicleData = {
                  reg_plate: newRow.reg_plate,
                  year: newRow.year,
                  class: newRow.class,
                  vin: newRow.vin,
                  transmissions: newRow.transmissions,
                  engine: newRow.engine,
                  model: newRow.model,
                  brand: newRow.brand.name,
                  customer_id: newRow.full_name,
                };

                setTimeout(() => {
                  createVehicle(createVehicleData);
                  setIsClicked(false);

                  resolve();
                }, 1000);
              }),
          }}
        />
      </motion.div>
    </>
  );
};

export default Vehicles;
