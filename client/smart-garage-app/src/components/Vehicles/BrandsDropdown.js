/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/prop-types */
import { BASE_URL } from "../../common/constants";
import { getToken } from "../../providers/AuthContext";
import { React, useEffect, useState } from "react";
import makeAnimated from 'react-select/animated';
import Select from 'react-select'

const BrandsDropdown = ({ row }) => {
  const animatedComponents = makeAnimated();
  const [options, setOptions] = useState([]);
  const [selectedBrand, setSelectedBrand] = useState({});

  useEffect(() => {
    fetch(`${BASE_URL}/brands`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json"
      },
      method: "GET",
    })
      .then(response => response.json())
      .then(result => {
        setSelectedBrand(result.find((el) => el.name === row.rowData.brand));
        setOptions(result)
      });
  }, []);

  return (
    <Select
      value={selectedBrand}
      getOptionLabel={(option) => option.name}
      getOptionValue={(option) => option}
      onChange={(value) => { 
        row.onChange({ ...value }) 
        setSelectedBrand(value);
      }}
      placeholder="Select brand"
      components={animatedComponents}
      name="brands"
      options={options}
      className="basic-single"
      classNamePrefix="select"
    />

  )
};

export default BrandsDropdown;

