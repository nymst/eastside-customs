/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { BASE_URL } from "../../common/constants";
import { getToken } from "../../providers/AuthContext";
import { useEffect, useState } from "react";
import makeAnimated from 'react-select/animated';
import Select from 'react-select'

const UsersDropdown = ({ row }) => {
  const animatedComponents = makeAnimated();
  const [options, setOptions] = useState([]);

  useEffect(() => {
    fetch(`${BASE_URL}/users`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json"
      },
      method: "GET",
    })
      .then(response => response.json())
      .then(result => {
        setOptions(result)
      });
  }, []);


  return (
    <Select
      defaultValue
      getOptionLabel={(option) => option.fullname}
      getOptionValue={(option) => option.id}
      onChange={(value) => row.onChange(value.id)}
      placeholder="Select customer"
      components={animatedComponents}
      name="customers"
      options={options}
      className="basic-single"
      classNamePrefix="select"
    />
  )
};

export default UsersDropdown;





