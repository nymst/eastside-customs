import { toast, Bounce } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';

export const successToast = (message, {...props}) => {
  toast.success(message, {
    position: toast.POSITION.TOP_CENTER,
    autoClose: 3500,
    transition: Bounce,
    draggable: true,
    ...props,
  })
};

export const errorToast = (message, {...props}) => {
  toast.error(message, {
    position: toast.POSITION.TOP_CENTER,
    autoClose: 5000,
    transition: Bounce,
    draggable: true,
    ...props,
  })
};
