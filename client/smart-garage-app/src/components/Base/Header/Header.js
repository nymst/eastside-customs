import React, { useContext } from "react";
import {
  MDBNavbar, MDBNavbarNav, MDBNavItem, MDBNavLink,
  MDBIcon,
  MDBCollapse
} from "mdbreact";
import { Link, useHistory } from 'react-router-dom';
import "./Header.css";
import ModalLogout from "../../Modals/ModalLogout/ModalLogout";
import AuthContext from "../../../providers/AuthContext";
import { motion } from "framer-motion"

const Header = () => {

  const history = useHistory();
  const auth = useContext(AuthContext);


  return (
    <motion.div
    initial={{ opacity: 0}}
    animate={{ opacity: 1}}
    >
      <MDBNavbar className="navbar-header" color="default-color" dark expand="md">
        <Link to={'/home'}>
          <img className="logo" src="../../../logoCar.svg" alt="logo car" />
        </Link>
        <MDBCollapse id="navbarCollapse3" navbar>
          <MDBNavbarNav left>

            <MDBNavItem>
              <MDBNavLink to="/services">Services</MDBNavLink>
            </MDBNavItem>

            <MDBNavItem>
              <MDBNavLink to="/brands">Brands</MDBNavLink>
            </MDBNavItem>

            <MDBNavItem>
              <MDBNavLink className="contact-us-nav-bar" to="/contact_us">Contact us</MDBNavLink>
            </MDBNavItem>

            {auth.isLoggedIn && auth.user.role === "employee" ?
              <MDBNavItem>
                <MDBNavLink to="/operations">Operations</MDBNavLink>
              </MDBNavItem> : ""}
          </MDBNavbarNav>
          <MDBNavbarNav right>

            <MDBNavItem>
              {!auth.isLoggedIn ?
                <MDBIcon onClick={() => history.push('/login')} icon="user" /> :
                <MDBIcon onClick={() => history.push('/user')} icon="user" />}
            </MDBNavItem>

            {auth.isLoggedIn ?
              <MDBNavItem>
                <ModalLogout />
              </MDBNavItem> : ""}

          </MDBNavbarNav>
        </MDBCollapse>
      </MDBNavbar>

    </motion.div>
  );
}

export default Header;
