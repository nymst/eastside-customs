import React, { useContext } from "react";
import { Link } from "react-router-dom";
import AuthContext from "../../../providers/AuthContext";
import "./Footer.css";

const Footer = () => {
  const auth = useContext(AuthContext);
  const handleScrollToStats = () => {
    window.scrollTo({
      top: 0,
    });
  };
  return (
    <div className="footer">
      <footer className="text-center text-lg-start bg-light text-muted">
        <section className="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
          <div className="me-5 d-none d-lg-block">
            <span>Get connected with us on social networks:</span>
          </div>
          <div>
            <a href="A" className="me-4 text-reset">
              <i className="fab fa-facebook-f"></i>
            </a>
            <a href="B" className="me-4 text-reset">
              <i className="fab fa-twitter"></i>
            </a>
            <a href="c" className="me-4 text-reset">
              <i className="fab fa-google"></i>
            </a>
            <a href="d" className="me-4 text-reset">
              <i className="fab fa-instagram"></i>
            </a>
            <a href="E" className="me-4 text-reset">
              <i className="fab fa-linkedin"></i>
            </a>
            <a href="F" className="me-4 text-reset">
              <i className="fab fa-github"></i>
            </a>
          </div>
        </section>
        <hr></hr>
        <section className="">
          <div className="container text-center text-md-start mt-5">
            <div className="row mt-3">
              <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                <h6 className="text-uppercase fw-bold mb-4">
                  <i className="fas fa-gem me-3"></i>Eastside Customs
                </h6>
                <p>
                  We are dedicated automotive professionals and experienced
                  problem solvers. Our pledge is to deliver quality and value,
                  and we will work to deserve your trust and the trust of your
                  friends and family.
                </p>
              </div>
              <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                <h6 className="text-uppercase fw-bold mb-4">Useful links</h6>
                <p>
                  <Link
                    to="/services"
                    onClick={handleScrollToStats}
                    className="text-reset"
                  >
                    Services
                  </Link>
                </p>
                <p>
                  <Link
                    to="/brands"
                    onClick={handleScrollToStats}
                    className="text-reset"
                  >
                    Brands
                  </Link>
                </p>
                {!auth.isLoggedIn ? <p>
                  <Link
                    to="/login"
                    onClick={handleScrollToStats}
                    className="text-reset"
                  >
                    Login
                  </Link>
                </p> : ""}

              </div>
              <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                <h6 className="text-uppercase fw-bold mb-4">Contact</h6>
                <p>
                  <i className="fas fa-home me-3"></i> Varna, 9000, Bulgaria
                </p>
                <p className="email-footer">
                  <i className="fas fa-envelope me-3"></i>
                  eastside.customs.garage@gmail.com
                </p>
                <p>
                  <i className="fas fa-phone me-3"></i> + 359 887866 092
                </p>
                <p>
                  <i className="fas fa-print me-3"></i> + 359 887866 095
                </p>
              </div>
            </div>
          </div>
        </section>
        <div className="text-center p-4">
          <p>Copyright &copy; 2021 All Rights reserved</p>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
