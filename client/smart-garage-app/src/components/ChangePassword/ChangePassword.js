/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBInput } from "mdbreact";
import { BASE_URL } from "../../common/constants";
import { errorToast, successToast } from "../Toasters/Toasters";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { ToastContainer } from "react-toastify";
import { changePasswordSchema} from "../../common/validator-schemas";
import "./ChangePassword.css";
import { getToken } from "../../providers/AuthContext";
import jwtDecode from "jwt-decode";

const ChangePassword = () => {
  const password = {
    password: "",
  };

  const id = jwtDecode(localStorage.getItem("token")).id;
  const [open, setOpen] = useState(true);

  const changePassword = (password, { setSubmitting }) => {
    fetch(`${BASE_URL}/users/${id}/change_password`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(password),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.msg === "Password not correct") {
          errorToast("Incorrect current password", {
            onOpen: () => setSubmitting(true),
            onClose: () => setSubmitting(false),
          });
        } else {
          successToast("Your password has been successfully updated.", {
            onOpen: () => setSubmitting(true),
            onClose: () => setSubmitting(false),
          });
        }
      });
  };

  return open ? (
    <MDBContainer className="container-change-password">
      <ToastContainer />
      <MDBRow className="change-pass-row">
        <MDBCol className="change-pass-col" md="6">
          <Formik
            validationSchema={changePasswordSchema}
            initialValues={password}
            onSubmit={changePassword}
          >
            {(props) => (
              <Form>
                <hr></hr>
                <div className="change-pass-header">
                <p className="h5 text-center mb-4">Change your password</p>
                <p className="additional-text">
                  It's good idea to use strong password you don't use elsewhere!
                </p>
                </div>
                <hr></hr>
                <div className="black-text">
                  <Field className="change-pass-field"
                    as={MDBInput} 
                    label ="Enter your current password "
                    name="oldPassword"
                    type="password"
                    disabled = {props.isSubmitting}
                  >
                    <div className="validator-error">
                      <ErrorMessage
                        name="oldPassword"
                        style={{ color: "red" }}
                      />
                    </div>
                  </Field>
                  <Field
                    as={MDBInput}
                    label="Type your new password"
                    name="password"
                    type="password"
                    disabled = {props.isSubmitting}
                  >
                    <div className="validator-error">
                      <ErrorMessage name="password" style={{ color: "red" }} />
                    </div>
                  </Field>

                  <Field
                    as={MDBInput}
                    label="Confirm your new password"
                    name="passwordConfirmation"
                    type="password"
                    disabled = {props.isSubmitting}
                  >
                    <div className="validator-error">
                      <ErrorMessage
                        name="passwordConfirmation"
                        style={{ color: "red" }}
                      />
                    </div>
                  </Field>
                </div>
                .
                <div className="text-center">
                  <MDBBtn className="change-pass-btn" type="submit" disabled={props.isSubmitting}>
                    Confirm
                  </MDBBtn>
                </div>
              </Form>
            )}
          </Formik>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  ) : (
    ""
  );
};

export default ChangePassword;
