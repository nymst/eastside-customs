/* eslint-disable no-unused-vars */

import { useState } from "react";
import { css } from "@emotion/react";
import ClipLoader from "react-spinners/ClipLoader";
import React from "react";

const override = css`
  display: block;
  margin: 100px auto;
  border-color: #2BBBAD;
`;

const Loader = () => {
  let [loading, setLoading] = useState(true);
  let [color, setColor] = useState("#ffffff");

  return (
    <div className="sweet-loading">
  
      <ClipLoader color={color} loading={loading} css={override} size={150} />
    </div>
  );
};

export default Loader;
