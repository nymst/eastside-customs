/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";
import MaterialTable from "material-table";
import { tableIcons } from "../../../common/material-icons";
import { getToken } from "../../../providers/AuthContext";
import {
  BASE_URL,
  CITY_MAX_LENGTH,
  CITY_MIN_LENGTH,
  COUNTRY_MAX_LENGTH,
  COUNTRY_MIN_LENGTH,
  FIRST_NAME_MAX_LENGTH,
  FIRST_NAME_MIN_LENGTH,
  LAST_NAME_MAX_LENGTH,
  LAST_NAME_MIN_LENGTH,
  PHONE_NUMBER_LENGTH,
  POSTAL_CODE_MAX_LENGTH,
  POSTAL_CODE_MIN_LENGTH,
} from "../../../common/constants";
import { motion } from "framer-motion";
import { errorToast, successToast } from "../../Toasters/Toasters";
import { ToastContainer } from "react-toastify";


const Users = () => {
  const columns = [
    {
      title: "Name",
      field: "first_name",
      validate: (rowData) =>
        rowData.first_name?.length >= FIRST_NAME_MIN_LENGTH &&
        rowData.first_name?.length <= FIRST_NAME_MAX_LENGTH,
    },
    {
      title: "Surname",
      field: "last_name",
      validate: (rowData) =>
        rowData.last_name?.length >= LAST_NAME_MIN_LENGTH &&
        rowData.last_name?.length <= LAST_NAME_MAX_LENGTH,
    },
    { title: "Email", field: "email" },
    {
      title: "Phone", field: "phone_number", validate: (rowData) =>
        rowData.phone_number?.length <= PHONE_NUMBER_LENGTH
    },
    {
      title: "Role",
      field: "role",
      lookup: { employee: "employee", customer: "customer" },
    },
    {
      title: "Postal code", field: "postal_code", validate: (rowData) =>
        rowData.postal_code?.length >= POSTAL_CODE_MIN_LENGTH &&
        rowData.postal_code?.length <= POSTAL_CODE_MAX_LENGTH
    },
    {
      title: "Country", field: "country", validate: (rowData) =>
        rowData.country?.length >= COUNTRY_MIN_LENGTH &&
        rowData.country?.length <= COUNTRY_MAX_LENGTH
    },
    {
      title: "City", field: "city", validate: (rowData) =>
        rowData.city?.length >= CITY_MIN_LENGTH &&
        rowData.city?.length <= CITY_MAX_LENGTH
    },
    {
      title: "Address", field: "street_address", validate: (rowData) =>
        typeof rowData.street_address === "string"
    },
  ];

  const [rows, setRows] = useState([]);
  const [users, setUsers] = useState([]);
  const [isClicked, setIsClicked] = useState(false);

  useEffect(() => {
    setRows(users);
  }, [users]);

  const getAllUsers = () => {
    fetch(`${BASE_URL}/users`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setUsers(result);
      })
  };

  useEffect(() => {
    getAllUsers();
  }, [isClicked]);

  const createUser = (newUser) => {
    fetch(`${BASE_URL}/users`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(newUser),
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.errors) {
          if (result.errors.email) {
            return errorToast('Registration failed! Invalid email format.', { ...null })
          } else if (result.errors.phone_number) {
            return errorToast('Registration failed! Invalid phone number format', { ...null })
          }
        }
        successToast('User registered successfully!', { ...null });
        setIsClicked(true)
      });
  };

  const updateUser = (id, updatedUser) => {
    fetch(`${BASE_URL}/users/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(updatedUser),
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.errors) {
          if (result.errors.email) {
            return errorToast('Order failed to update! Invalid email format.', { ...null })
          } else if (result.errors.phone_number) {
            return errorToast('Order failed to update! Invalid phone number format', { ...null })
          }
        }
        successToast('Order updated successfully', { ...null })
        setIsClicked(true)
      })
      .catch((error) => {
        throw new Error(error.message);
      });
  };

  const deleteUser = (id) => {
    fetch(`${BASE_URL}/users/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((r) => r.json())
      .then((r) => setIsClicked(true));
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ delay: 0.3, duration: 0.35 }}>
      <ToastContainer />

      <MaterialTable
        icons={tableIcons}
        title="Users"
        columns={columns}
        data={rows}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
              const newUser = {
                first_name: newData.first_name,
                last_name: newData.last_name,
                email: newData.email,
                phone_number: newData.phone_number,
                role: newData.role,
                postal_code: newData.postal_code,
                country: newData.country,
                city: newData.city,
                street_address: newData.street_address,
              };

              setTimeout(() => {
                createUser(newUser);
                setIsClicked(false)
                resolve();
              }, 1000);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                const updatedUser = {
                  first_name: newData.first_name,
                  last_name: newData.last_name,
                  email: newData.email,
                  phone_number: newData.phone_number,
                  role: newData.role,
                  postal_code: newData.postal_code,
                  country: newData.country,
                  city: newData.city,
                  street_address: newData.street_address,
                };

                updateUser(newData.id, updatedUser);
                setIsClicked(false);

                resolve();
              }, 1000);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                deleteUser(oldData.id);
                const dataDelete = [...rows];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1);
                setRows([...dataDelete]);
                resolve();
              }, 1000);
            }),
        }}
        options={{
          tableLayout: 'auto',
          filtering: "true",
          addRowPosition: 'first',
          padding: 'dense',
          pageSize: 10,
          doubleHorizontalScroll: false,
          headerStyle: {
            width: 26,
            whiteSpace: "nowrap",
            textAlign: "center",
            flexDirection: "row",
            overflow: "hidden",
            textOverflow: "ellipsis",
            paddingRight: 2,
            backgroundColor: "#DC3D24",
            color: "whitesmoke",
            fontWeight: "bold",
            fontSize: "18px",
            letterSpacing: "1px",
            paddingLeft: "25px"
          },
          cellStyle: {
            textAlign: "center",
            letterSpacing: "0.5px",
          },
          rowStyle: { backgroundColor: "#E5E5E5" },

        }}
      />
    </motion.div>
  );
};

export default Users;
