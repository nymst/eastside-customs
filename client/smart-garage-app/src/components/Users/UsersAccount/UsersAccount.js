/* eslint-disable react/prop-types */
import React, { useContext, useState, useEffect } from "react";
import "./UsersAccount.css";
import CustomerOrders from "../../Orders/CustomerOrders/CustomerOrders";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBCol,
  MDBBtn,
  MDBBtnGroup,
} from "mdbreact";
import ChangePassword from "../../ChangePassword/ChangePassword";
import { BASE_URL } from "../../../common/constants";
import { getToken } from "../../../providers/AuthContext";
import AuthContext from "../../../providers/AuthContext";
import { motion } from "framer-motion"

const UsersAccount = () => {
  const [clickOrder, setClickOrder] = useState(true);
  const [clickPassword, setClickPassword] = useState(false);
  const [activeOrders, setActiveOrders] = useState([]);

  const { user } = useContext(AuthContext);

  const clickedCMyOrders = () => {
    clickOrder ? setClickOrder(false) : setClickOrder(true);
    setClickPassword(false);
  };

  const clickedPassword = () => {
    clickPassword ? setClickPassword(false) : setClickPassword(true);
    setClickOrder(false);
  };

  useEffect(() => {
    fetch(`${BASE_URL}/user/activeOrders`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setActiveOrders(result);
      });
  }, []);

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ delay: 0.3, duration: 0.35 }}>

        <div className="user-account" style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/admin-background.jpg` }}>
          <div
            className="btn-toolbar-customer-account"
            role="toolbar"
            aria-label="Toolbar with button groups"
          >
            <MDBBtnGroup className="mr-2">
              <MDBBtn onClick={clickedCMyOrders}>Orders</MDBBtn>
              <MDBBtn onClick={clickedPassword}>Change Password</MDBBtn>
            </MDBBtnGroup>
          </div>
          <div className="customer-side-bar">
            <MDBCol md="4">
              <MDBCard
                className="customer-description-card"
                style={{ width: "22rem" }}
              >
                <MDBCardBody className="card-body-user-account">
                  <MDBCardTitle className="customer-name-card-title">
                    {user.name} {user.last_name}
                  </MDBCardTitle>
                  <MDBCardText>
                    <hr></hr>
                    <p>
                      <b>Email:</b> {user.email}
                    </p>
                    <hr></hr>
                    <p>
                      <b>Phone:</b> {user.phone}
                    </p>
                    <hr></hr>
                    <p>
                      <b>City:</b> {user.city}
                    </p>
                    <hr></hr>
                    <p>
                      <b>Address: </b>
                      {user.address}
                    </p>
                    <hr></hr>
                  </MDBCardText>
                  <MDBCardTitle className="customer-order-card-title">
                    Current Orders
            </MDBCardTitle>

                  {activeOrders.map((activeOrder) => {
                    return (
                      <MDBCardText>
                        <hr></hr>
                        <p className="customer-order-order-id">
                          <b>Order ID:</b>{activeOrder.order_id}
                        </p>
                        <hr></hr>
                        <p>
                          <b>Vehicle:</b> {activeOrder.brand_name}{" "}
                          {activeOrder.model_name}
                        </p>
                        <hr></hr>
                        <p>
                          <b>To be provided:</b> {activeOrder.service_name}
                        </p>
                        <hr></hr>
                        <p>
                          <b>Status:</b> {activeOrder.status}
                        </p>
                        <hr></hr>
                        <p>
                          <b>Due date: </b>
                          {activeOrder.due_date}
                        </p>
                        <hr></hr>
                        <p>
                          <b>Your mechanic: </b>
                          {activeOrder.employee_full_name}
                        </p>
                      </MDBCardText>)
                  })}
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
          </div>
          <div className="customer-options">
            {clickOrder ? <CustomerOrders /> : ""}
            {clickPassword ? <ChangePassword /> : ""}
          </div>
        </div>
      </motion.div>
    </>
  );
};

export default UsersAccount;
