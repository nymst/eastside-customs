import React, {useState } from 'react';
import Modal from 'react-bootstrap/Modal'
import { Button } from 'react-bootstrap';
import { MDBIcon } from 'mdbreact';
import { useContext } from 'react';
import AuthContext from '../../../providers/AuthContext';
import "./ModalLogout.css"

const ModalLogout = () => {

  const [show, setShow] = useState(false);
  const auth = useContext(AuthContext);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const logout = () => {
    localStorage.removeItem('token');
    auth.setAuthValue({
      user: null,
      isLogged: false,
    }) 
  }

  return (
    <>
     <MDBIcon onClick={handleShow} icon="sign-out-alt" />
  
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Log out</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure you want to log out from your profile?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button onClick={logout} variant="primary">Log out</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalLogout;
