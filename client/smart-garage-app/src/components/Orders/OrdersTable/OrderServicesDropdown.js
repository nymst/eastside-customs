/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { useEffect, useState } from "react";
import { BASE_URL } from "../../../common/constants";
import { getToken } from "../../../providers/AuthContext";
import makeAnimated from 'react-select/animated';
import Select from 'react-select'

const OrderServicesDropdown = ({ row }) => {
  const animatedComponents = makeAnimated();
  const [options, setOptions] = useState([]);
  const [selectedOptions, setSelectedOptions] = useState([]);
  useEffect(() => {
    fetch(`${BASE_URL}/services`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json"
      },
      method: "GET",
    })
      .then(response => response.json())
      .then(result => {
        if (row.rowData.hasOwnProperty("services_id")) {
          const existingServices = result
            .filter(element => row.rowData.services_id.includes(element.id));
          result = result
            .filter(element => element.id !== existingServices.id)

          setSelectedOptions(existingServices);
        }

        setOptions(result);
      });
  }, []);

  return (
    <Select
      value={selectedOptions}
      isMulti
      getOptionLabel={(option) => option.name}
      getOptionValue={(option) => option.id}
      onChange={(value) => {
        row.onChange(value.map(service => service.id))
        setSelectedOptions(value);
      }}
      placeholder="Select services"
      components={animatedComponents}
      name="services"
      options={options}
      className="basic-multi-select"
      classNamePrefix="select"
    />
  )
};

export default OrderServicesDropdown;
