/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { useEffect, useState } from "react";
import { BASE_URL } from "../../../common/constants";
import { getToken } from "../../../providers/AuthContext";
import makeAnimated from 'react-select/animated';
import Select from 'react-select'

const VehiclesDropdown = ({ row }) => {
  const animatedComponents = makeAnimated();
  const [options, setOptions] = useState([]);

  useEffect(() => {
    fetch(`${BASE_URL}/vehicles`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json"
      },
      method: "GET",
    })
      .then(response => response.json())
      .then(result => {
        setOptions(result)
      });
  }, []);

  return (
    <Select
      getOptionLabel={(option) => option.reg_plate}
      getOptionValue={(option) => option.id}
      onChange={(value) => row.onChange(value.id)}
      placeholder="Select vehicles"
      components={animatedComponents}
      name="vehicles"
      options={options}
      className="basic-single"
      classNamePrefix="select"
    />
  )
};

export default VehiclesDropdown;
