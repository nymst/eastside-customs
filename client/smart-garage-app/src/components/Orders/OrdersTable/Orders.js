import MaterialTable from "material-table";
import { React, useEffect, useState } from "react";
import {
  BASE_URL,
  DOTS,
  MAX_NOTES_LENGTH,
  MIN_NOTES_LENGTH,
} from "../../../common/constants.js";
import { getToken } from "../../../providers/AuthContext";
import { ToastContainer } from "react-toastify";
import { tableIcons } from "../../../common/material-icons";
import OrderServicesDropdown from "./OrderServicesDropdown";
import VehiclesDropdown from "./VehiclesDropdown";
import { errorToast, successToast } from "../../Toasters/Toasters.js";
import { calculateTotal } from "../../../common/utils.js";
import { motion } from "framer-motion"

const Orders = () => {
  const [allOrders, setAllOrders] = useState([]);
  const [isClicked, setIsClicked] = useState(false);
  const columns = [
    {
      title: "Order",
      field: "order_id",
      editable: false,
      align: "center",
      render: (rowData) => (
        <a href={"/orders/" + rowData?.order_id}>{rowData?.order_id}</a>
      ),
    },
    {
      title: "Status",
      field: "status",
      align: "center",
      editable: false,
      lookup: { New: "New", Progressing: "Progressing", Done: "Done" },
    },
    {
      title: "Customer",
      field: "fullname",
      editable: false,
      align: "center",
      headerStyle: { paddingLeft: "25px" },
    },
    {
      title: "Vehicle",
      field: "reg_plate",
      editable: "onAdd",
      align: "center",
      editComponent: (props) => <VehiclesDropdown row={props} />,
    },
    {
      title: "Performed services",
      field: "service_name",
      align: "center",
      editComponent: (props) => <OrderServicesDropdown row={props} />,
      headerStyle: {
        paddingLeft: "20px",
      },
      render: (rowData) => rowData?.service_name.length > 28 ? rowData?.service_name.slice(0, 25) + DOTS : rowData?.service_name,
    },
    {
      title: "Notes",
      field: "notes",
      validate: (rowData) =>
        rowData.notes?.length >= MIN_NOTES_LENGTH &&
        rowData.notes?.length <= MAX_NOTES_LENGTH,
      render: (rowData) => rowData?.notes.length > 15 ? rowData?.notes.slice(0, 20) + DOTS : rowData?.notes,
    },
    {
      title: "Due date",
      field: "due_date",
      type: "date",
      dateSetting: { locale: "en-AU" },
    },
    {
      title: "Issue date",
      field: "issue_date",
      editable: false,
      type: "date",
      dateSetting: { format: "YYYY-MM-DD" },
    },
    {
      title: "Total price",
      field: "sum",
      editable: false,
      render: (rowData) => calculateTotal(rowData?.class, rowData?.sum) + rowData?.currency
    },
    { title: "Employee", field: "employee_full_name", editable: false },
  ];

  const getAllOrders = () => {
    fetch(`${BASE_URL}/orders`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-Type": "application/json",
      },
      method: "GET",
    })
      .then((response) => response.json())
      .then((result) => {
        if (Array.isArray(result)) {
          setAllOrders(result);
        } else {
          return;
        }
      })
  };

  const deleteOrder = (id) => {
    fetch(`${BASE_URL}/orders/${id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json",
      },
      method: "DELETE",
    })
      .then((response) => response.json())
      .then(() => setIsClicked(true));
  };

  const updateOrder = (id, updateData) => {
    fetch(`${BASE_URL}/orders/${id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json",
      },
      method: "PUT",
      body: JSON.stringify(updateData),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.errors) {
          if (result.errors.services_id) {
            errorToast(
              "Cannot update the order. No services have been selected!",
              { ...null }
            );
          } else if (result.errors.notes) {
            errorToast(
              "Cannot update the order. The notes field should be 3-500 symbols long!",
              { ...null }
            );
          }
        }
        successToast(
          `Order №${result.order_id} has been successfully updated!`,
          { ...null }
        );
        setIsClicked(true);
      });
  };

  const createOrder = (createData) => {
    fetch(`${BASE_URL}/orders`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(createData),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.errors) {
          return errorToast(
            "Order not created! Fill out all of the required fields before submitting.",
            { ...null }
          );
        }
        if (result.message) {
          return errorToast(`Order not created! ${result.message}`, { ...null });
        }
        successToast("Order successfully created!", { ...null });
        setIsClicked(true);
      });
  };

  useEffect(() => {
    getAllOrders();
  }, [isClicked]);

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ delay: 0.3, duration: 0.35 }}>

        <ToastContainer />
        <MaterialTable
          icons={tableIcons}
          title="Orders"
          data={allOrders}
          columns={columns}
          options={{
            tableLayout: "auto",
            filtering: "true",
            addRowPosition: "first",
            padding: "dense",
            pageSize: 10,
            doubleHorizontalScroll: false,
            headerStyle: {
              width: 26,
              whiteSpace: "nowrap",
              textAlign: "center",
              flexDirection: "row",
              overflow: "hidden",
              textOverflow: "ellipsis",
              paddingRight: 2,
              backgroundColor: "#DC3D24",
              color: "whitesmoke",
              fontWeight: "bold",
              fontSize: "18px",
              letterSpacing: "1px",
              paddingLeft: "25px",
            },
            cellStyle: {
              textAlign: "center",
              letterSpacing: "0.5px",
            },
            rowStyle: { backgroundColor: "#E5E5E5" },
          }}
          editable={{
            onRowDelete: (selectedRow) =>
              new Promise((resolve, reject) => {
                const index = selectedRow.order_id;
                const updatedRows = [...allOrders];

                updatedRows.splice(index, 1);

                deleteOrder(index);
                setAllOrders(updatedRows);
                setIsClicked(false);

                resolve();
              }),

            onRowUpdate: (updatedRow, oldRow) =>
              new Promise((resolve, reject) => {
                const updateOrderData = {
                  status: updatedRow.status,
                  notes: updatedRow.notes,
                  due_date: updatedRow.due_date,
                  services_id:
                    typeof updatedRow.service_name === "string"
                      ? updatedRow.services_id
                      : updatedRow.service_name,
                };

                setTimeout(() => {
                  updateOrder(oldRow.order_id, updateOrderData);
                  setIsClicked(false);

                  resolve();
                }, 1000);
              }),

            onRowAdd: (newRow) =>
              new Promise((resolve, reject) => {
                const createOrderData = {
                  due_date: newRow.due_date,
                  notes: newRow.notes,
                  vehicle_id: newRow.reg_plate,
                  services_id: newRow.service_name,
                };

                setTimeout(() => {
                  createOrder(createOrderData);
                  setIsClicked(false);

                  resolve();
                }, 1000);
              }),
          }}
        />
      </motion.div>
    </>
  );
};
export default Orders;
