/* eslint-disable react/prop-types */
import {
  MDBBtn,
  MDBDropdown,
  MDBDropdownItem,
  MDBDropdownMenu,
  MDBDropdownToggle,
} from "mdbreact";
import { React, useContext, useEffect, useState } from "react";
import { ToastContainer } from "react-toastify";
import { BASE_URL, BGN, EUR, USD } from "../../../common/constants";
import { calculateTotal } from "../../../common/utils";
import { getToken } from "../../../providers/AuthContext";
import { errorToast, successToast } from "../../Toasters/Toasters";
import "./Order.css";
import AuthContext from "../../../providers/AuthContext";

const Order = ({ match }) => {
  const [order, setOrder] = useState([]);
  const [services, setServices] = useState([]);
  const auth = useContext(AuthContext);
  const id = +match.params.id;
  const [total, setTotal] = useState(null);
  
  useEffect(() => {
    fetch(`${BASE_URL}/orders/${id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json",
      },
      method: "GET",
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.message) {
          errorToast(result.message, { ...null });
        }
        setOrder(result);
        setTotal(calculateTotal(result.class, result.sum));
      });
  }, [id]);

  useEffect(() => {
    fetch(`${BASE_URL}/orders/${id}/services`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json",
      },
      method: "GET",
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.message) {
          errorToast(result.message, { ...null });
        }
        setServices(result);
      });
  }, [order, id]);

  const generateInvoice = (values) => {
    fetch(`${BASE_URL}/invoices`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(values),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.message) {
          errorToast(result.message, { ...null });
        }

        successToast(`An invoice has been sent to the customer's email.`, {
          ...null,
        });
      });
  };

  const generateReport = () => {
    fetch(`${BASE_URL}/orders/${order.order_id}/report`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({ total: total + order.currency }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.message) {
          errorToast(result.message, { ...null });
        }

        successToast(
          `A detailed order report has been sent to the customer's email.`,
          { ...null }
        );
      });
  };

  const resendInvoice = () => {
    fetch(`${BASE_URL}/invoices/${order.invoice_id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json",
      },
      method: "GET",
      body: JSON.stringify(),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.message) {
          errorToast(result.message, { ...null });
        }

        successToast(`An invoice has been sent to the customer's email.`, {
          ...null,
        });
      });
  };

  const editOrder = (values) => {
    fetch(`${BASE_URL}/orders/${order.order_id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json",
      },
      method: "PUT",
      body: JSON.stringify(values),
    })
      .then((response) => response.json())
      .then((result) => {
        setTotal(calculateTotal(result.class, result.sum));
        setOrder(result);
      });
  };

  return (
    <>
      <ToastContainer />
      <div className="invoice-box">
        <table cellPadding="0" cellSpacing="0">
          <tr className="top">
            <td colSpan="2">
              <table>
                <tr>
                  <td>
                    Order #: {order.order_id}
                    <br />
                    Issue date: {order.issue_date}
                    <br />
                    Due date: {order.due_date}
                  </td>
                  <td>
                    Client: {order.fullname} <br />
                    {order.street_address}
                    <br />
                    {order.postal_code} {order.city} <br />
                    {order.country}
                    <br />
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr className="information">
            <td colSpan="2">
              <table>
                <tr>
                  <td>
                    Employee: {order.employee_full_name}
                    <br />
                    Status: {order.status}
                    <br />
                  </td>
                </tr>
              </table>
            </td>
          </tr>

          <tr className="heading">
            <td>Brand: </td>

            <td>Car details: </td>
          </tr>

          <tr className="details">
            <td>
              {" "}
              {order.brand_name} {order.model_name} {order.year}
            </td>

            <td>
              Class: {order.class}
              <br />
              Engine: {order.engine}
              <br />
              Transmission: {order.transmissions}
              <br />
              VIN: {order.vin}
              <br />
              Registration plate: {order.reg_plate}
              <br />
            </td>
          </tr>

          <tr className="heading">
            <td>Item</td>

            <td>Price</td>
          </tr>
          {services.map((service) => (
            <tr className="item" key={service.id}>
              <td className="order-services-items">{service.service} </td>

              <td className="order-services-prices">{service.price}</td>
            </tr>
          ))}

          <tr className="total">
            <td>Total:</td>
            <td>{calculateTotal(order?.class, order?.sum) + order.currency}</td>
          </tr>
        </table>
        <div className="add-info">
          <p className="multiplier">
            *The total price varies depending on the class of your vehicle
          </p>
          <p className="multipliers-class">
            <b>Class Multipliers:</b> <b>A:</b>1.05; <b>B:</b> 1.10; <b>C:</b>{" "}
            1.30; <b>D:</b> 1.50; <b>E:</b> 1.65;
            <b>F:</b> 1.80; <b>S:</b> 2.00;{" "}
          </p>
        </div>

        <div className="order-generate-invoice-button text-center">
          <div>
            <MDBDropdown>
              <MDBDropdownToggle caret color="default">
                Currency
              </MDBDropdownToggle>
              <MDBDropdownMenu basic>
                <MDBDropdownItem
                  style={
                    order.currency === BGN ? { backgroundColor: "grey" } : {}
                  }
                  onClick={() => editOrder({ currency: BGN })}
                >
                  BGN
                </MDBDropdownItem>

                <MDBDropdownItem
                  style={
                    order.currency === EUR ? { backgroundColor: "grey" } : {}
                  }
                  onClick={() => editOrder({ currency: EUR })}
                >
                  EUR
                </MDBDropdownItem>

                <MDBDropdownItem
                  style={
                    order.currency === USD ? { backgroundColor: "grey" } : {}
                  }
                  onClick={() => editOrder({ currency: USD })}
                >
                  USD
                </MDBDropdownItem>
              </MDBDropdownMenu>
            </MDBDropdown>
          </div>

          {auth.user.role === "employee" ? (
            <MDBBtn
              disabled={order.invoice_id}
              onClick={() => {
                generateInvoice({
                  orders_id: order.order_id,
                  currency: order.currency,
                  total,
                });
                editOrder({ status: "Progressing" });
              }}
            >
              {order?.invoice_id ? "Order confirmed" : "Confirm order"}
            </MDBBtn>
          ) : (
            ""
          )}

          {auth.user.role === "employee" ? (
            <MDBBtn
              outline
              color="secondary"
              disabled={order.status === "Done"}
              onClick={() =>
                order.status === "New"
                  ? editOrder({ status: "Progressing" })
                  : order.status === "Progressing"
                  ? editOrder({ status: "Done" })
                  : ""
              }
            >
              Advance order
            </MDBBtn>
          ) : (
            ""
          )}
          <MDBBtn outline color="secondary" onClick={() => generateReport()}>
            Send full order report
          </MDBBtn>

          <MDBBtn outline color="secondary" onClick={() => resendInvoice()}>
            Resend order invoice
          </MDBBtn>
        </div>
      </div>
    </>
  );
};
export default Order;
