import MaterialTable from "material-table";
import React, { useEffect, useState } from "react";
import { BASE_URL, DOTS } from "../../../common/constants";
import { getToken } from "../../../providers/AuthContext";
import { tableIcons } from "../../../common/material-icons";
import { calculateTotal } from "../../../common/utils";

const CustomerOrders = () => {
  const [orders, setOrders] = useState([]);

  const columns = [
    {
      title: "Order",
      field: "order_id",
      editable: false,
      align: "center",
      render: (rowData) => (
        <a href={"/orders/" + rowData?.order_id}>{rowData?.order_id}</a>
      ),
    },
    { title: "Brand", field: "brand_name" },
    { title: "Model", field: "model_name" },
    { title: "Plate", field: "reg_plate" },
    { title: "VIN", field: "vin" },
    {
      title: "Issue Date",
      field: "issue_date",
      type: "date",
      dateSetting: { locale: "en-AU" },
      filtering: true,
    },
    {
      title: "Due Date",
      field: "due_date",
      type: "date",
      dateSetting: { locale: "en-AU" },
      filtering: true,
    },
    {
      title: "Service",
      field: "service_name",
      render: (rowData) =>
        rowData?.service_name.length > 28
          ? rowData?.service_name.slice(0, 25) + DOTS
          : rowData?.service_name,
    },
    {
      title: "Sum",
      field: "sum",
      render: (rowData) =>
        calculateTotal(rowData?.class, rowData?.sum) + rowData?.currency,
    },
    { title: "Status", field: "status" },
    { title: "Employee", field: "employee_full_name" },
  ];

  useEffect(() => {
    fetch(`${BASE_URL}/user/orders`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setOrders(result);
      });
  }, []);

  return (
    <MaterialTable
      style={{
        width: "100%",

        display: "flex",
        flexDirection: "column",
      }}
      icons={tableIcons}
      title="My orders"
      columns={columns}
      data={orders}
      options={{
        tableLayout: "auto",
        filtering: "true",
        addRowPosition: "first",
        padding: "dense",
        pageSize: 10,
        doubleHorizontalScroll: false,
        headerStyle: {
          width: 20,
          whiteSpace: "nowrap",
          textAlign: "center",
          flexDirection: "row",
          overflow: "hidden",
          textOverflow: "ellipsis",
          paddingRight: 2,
          backgroundColor: "#DC3D24",
          color: "whitesmoke",
          fontWeight: "bold",
          fontSize: "14px",
          letterSpacing: "1px",
          paddingLeft: "25px",
        },
        cellStyle: {
          textAlign: "center",
          letterSpacing: "0.2px",
        },
        rowStyle: { backgroundColor: "#E5E5E5" },
      }}
    />
  );
};

export default CustomerOrders;
