import { useContext, useEffect, useState } from "react";
import { MDBBtn } from "mdbreact";
import "./Services.css";
import React from "react";
import { errorToast } from "../../Toasters/Toasters";
import { Link } from "react-router-dom";
import EditIcon from "@material-ui/icons/Edit";
import { getAllServicesNoAuth } from "../../../common/utils";
import AuthContext from "../../../providers/AuthContext";
import Loader from "../../Loader/Loader";
import { motion } from "framer-motion"

const Services = () => {
  const [services, setServices] = useState([]);
  const [itemsCount, setItemsCount] = useState(6);
  const [loading, setLoading] = useState(false);
  const { user } = useContext(AuthContext)

  const showMoreItems = () => {
    setItemsCount((prevValue) => prevValue + 6);
  };
  useEffect(() => {
    setLoading(true)
    getAllServicesNoAuth()
      .then((r) => r.json())
      .then((result) => {
        if (Array.isArray(result)) {
          setServices(result);
        } else {
          throw new Error(result.message);
        }
      })
      .catch((error) => {
        errorToast(error.message, { ...null });
      }).finally(() => setLoading(false));
  }, []);

  return (
    loading ? <Loader /> :
      <>
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ delay: 0.3, duration: 0.35 }}>
          <div className="services-heading">
            <h1 className="h1-responsive font-weight-bold my-5">
              All services we offer
        </h1>
          </div>
          <div className="price-legend">
            <div className="price-legend-text">
              <h5>Price Legend</h5>
              <p className="price-multiplayer">*The price of the service depends on the class of your vehicle</p>
            </div>
            <div className="price-legend-table">
              <table striped bordered hover size="sm" className="class-table">
                <thead>
                  <tr>
                    <th>Class</th>
                    <th > *Price Multiplier</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>A</td>
                    <td>1.05</td>
                  </tr>
                  <tr>
                    <td>B</td>
                    <td>1.15</td>
                  </tr>
                  <tr>
                    <td>C</td>
                    <td>1.30</td>
                  </tr>
                  <tr>
                    <td>D</td>
                    <td>1.50</td>
                  </tr>
                  <tr>
                    <td>E</td>
                    <td>1.65</td>
                  </tr>
                  <tr>
                    <td>F</td>
                    <td>1.80</td>
                  </tr>
                  <tr>
                    <td>S</td>
                    <td>2.00</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          {user !== null && user.role === "employee" ?
            <div className="service-add-button">
              <Link to="/services/add">
                <MDBBtn color="primary" size="md">
                  Add New Service
          </MDBBtn>
              </Link>
            </div> : ""}

          <div className="all-services">
            {services.slice(0, itemsCount).map((s) => {
              return (
                <div className="card" id="service-card" key={s.id}>
                  <div className="service-card-body" id="service-body">
                    {user && user.role === "employee" ?
                      <Link to={`/services/${s.id}/edit`} style={{ color: "black" }}>
                        <EditIcon />
                      </Link> : ""}
                    <h2 className="service-card-text" id="service-text">
                      {" "}
                      {s.name}
                    </h2>
                    <p className="service-price">{s.price} BGN</p>
                    <p className="service-description">{s.description}</p>
                  </div>
                </div>
              );
            })}
            <MDBBtn
              className="show-more-btn mb-5"
              onClick={showMoreItems}
              style={itemsCount >= services.length ? { display: "none" } : {}}
            >
              Load more
        </MDBBtn>
          </div>
        </motion.div>
      </>
  );
};

export default Services;
