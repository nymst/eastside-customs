/* eslint-disable react/prop-types */
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { MDBBtn, MDBCol, MDBContainer, MDBInput, MDBRow } from 'mdbreact';
import React, { useEffect, useState } from 'react';
import { BASE_URL } from '../../../../common/constants';

import { serviceSchema } from '../../../../common/validator-schemas';
import { getToken } from '../../../../providers/AuthContext';
import { useHistory } from "react-router-dom";
import '../Services.css';

const AddUpdateService = ({ match }) => {
  const id = +match.params.id;
  const [services, setServices] = useState([]);
  const history = useHistory();
  const service = services
    .filter((s) => s.id === id)
    .map((el) => ({
      name: el.name,
      price: el.price,
      description: el.description
    }))[0];

  const updateServiceValues = {
    name: service?.name,
    price: service?.price,
    description: service?.description
  }
  const createServiceValues = {
    name: "",
    price: 0,
    description: "",
  }

  useEffect(() => {

    fetch(`${BASE_URL}/services`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json"
      },
      method: "GET",
    })
      .then(response => response.json())
      .then(result => setServices(result))
  }, []);

  const createService = (values, { setSubmitting }) => {
    fetch(`${BASE_URL}/services`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(values)
    })
      .then(response => response.json())
      .then(result => {
        setSubmitting(false);
        history.push('/services')
      })
  }

  const updateService = (values, { setSubmitting }) => {
    fetch(`${BASE_URL}/services/${id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
        "Content-type": "application/json"
      },
      method: "PUT",
      body: JSON.stringify(values)
    })
      .then(response => response.json())
      .then(result => {
        console.log(result);
        setSubmitting(false);
        history.push('/services')
      })
  }

  return (
    <div className="edit-add-service" >
      <MDBContainer className="service-submit-form" >
        <MDBRow>
          <MDBCol md="6">
            <Formik
              initialValues={id ? updateServiceValues : createServiceValues}
              validationSchema={serviceSchema}
              onSubmit={id ? updateService : createService}
              enableReinitialize={true}>
              {props => (
                <Form>
                  <p className="h5 text-center mb-4">{id ? `Currently updating '${service?.name}' service.` : 'Create a service'}</p>

                  <div className="grey-text">
                    <Field
                      as={MDBInput}
                      label="Service name"
                      id="name"
                      name="name"
                      type="text"
                      disabled={props.isSubmitting}>

                      <div className="validator-error">
                        <ErrorMessage name="name" style={{ color: "red" }} />
                      </div>

                    </Field>

                    <Field
                      as={MDBInput}
                      label="Price"
                      name="price"
                      id="price"
                      type="number"
                      disabled={props.isSubmitting}>

                      <div className="validator-error">
                        <ErrorMessage name="price" style={{ color: "red" }} />
                      </div>


                    </Field>

                    <Field
                      as={MDBInput}
                      label="Description"
                      name="description"
                      type="text"
                      id="description"
                      disabled={props.isSubmitting}>

                      <div className="validator-error">
                        <ErrorMessage name="description" style={{ color: "red" }} />
                      </div>

                    </Field>
                  </div>

                  <div className="service-buttons text-center">
                    <MDBBtn type="submit" disabled={props.isSubmitting}>
                      Submit
                  </MDBBtn>
                    <MDBBtn outline color="secondary" onClick={() => history.push('/services')}>
                      Cancel
                  </MDBBtn>
                  </div>

                </Form>
              )}
            </Formik>
          </MDBCol>
        </MDBRow>
      </MDBContainer >
    </div>
  )
};

export default AddUpdateService;
