import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Users from "../../Users/UsersTable/Users";
import { useState } from "react";
import Orders from "../../Orders/OrdersTable/Orders";
import Vehicles from "../../Vehicles/Vehicles";
import { motion } from "framer-motion"

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ScrollableTabsButtonAuto() {
  const [clickRegister, setClickRegister] = useState(true);
  const [clickOrder, setClickOrder] = useState(false);
  const [clickVehicle, setClickVehicle] = useState(false);
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const clickedRegister = () => {
    clickRegister ? setClickRegister(false) : setClickRegister(true);
    setClickOrder(false);
    setClickVehicle(false);
  };

  const clickedOrder = () => {
    clickOrder ? setClickOrder(false) : setClickOrder(true);
    setClickRegister(false);
    setClickVehicle(false);
  };

  const clickedVehicle = () => {
    clickVehicle ? setClickVehicle(false) : setClickVehicle(true);
    setClickRegister(false);
    setClickOrder(false);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <motion.div
      className={classes.root}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ delay: 0.3, duration: 0.2 }}
    >
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          <Tab onClick={clickedRegister} label="Users" {...a11yProps(0)} />
          <Tab onClick={clickedOrder} label="Orders" {...a11yProps(1)} />
          <Tab onClick={clickedVehicle} label="Vehicles" {...a11yProps(2)} />


        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        {clickRegister ? <Users /> : ""}
      </TabPanel>
      <TabPanel value={value} index={1}>
        {clickOrder ? <Orders /> : ""}
      </TabPanel>
      <TabPanel value={value} index={2}>
        {clickVehicle ? <Vehicles /> : ""}
      </TabPanel>
    </motion.div>
  );
}
