/* eslint-disable react/react-in-jsx-scope */
import { useEffect, useState } from 'react';
import { MDBBtn } from 'mdbreact';
import './Brands.css';
import Loader from '../../Loader/Loader.js';
import { motion } from "framer-motion"

const Brands = () => {
  const [brands, setBrands] = useState([]);
  const [itemsCount, setItemsCount] = useState(6);
  const [loading, setLoading] = useState(false);

  const showMoreItems = () => {
    setItemsCount(prevValue => prevValue + 6);
  }

  useEffect(() => {
    setLoading(true)
    fetch('http://localhost:5555/brands', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(r => r.json())
      .then(result => {
        if (Array.isArray(result)) {
          setBrands(result)
        } else {
          throw new Error(result.message)
        }
      })
      .catch(error => {
        console.warn(error.message)
      })
      .finally(() => setLoading(false));
  }, []);

  return (
    loading ? <Loader/> :
    <>
    <motion.div
    initial={{ opacity: 0 }}
    animate={{ opacity: 1 }}
    transition={{ delay: 0.3, duration: 0.35 }}>

      <h1 className="h1-responsive font-weight-bold my-5">All brands we provide maintenance for</h1>
      <div className="all-brands">
        {brands.slice(0, itemsCount).map((b) => {
          return (
            <div className="card" id="brand-card" key={b.id}>

              <div className="card-body" id="brand-body">
                <img className="card-img" id="brand-img" src={b.logo} alt="" />
                <h1 className="card-text" id="brand-text"> {b.name}</h1>
              </div>

            </div>
          )
        })}
        <MDBBtn className="show-more-btn mb-5"
          onClick={showMoreItems}
          style={itemsCount >= brands.length ? { display: "none" } : {}}>
          Load more
      </MDBBtn>
      </div>
    </motion.div>
    </>
  )
}


export default Brands;
