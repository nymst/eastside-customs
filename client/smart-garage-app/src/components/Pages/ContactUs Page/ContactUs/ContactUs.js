import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBIcon, MDBBtn, MDBInput } from "mdbreact";
import "./ContactUs.css";
import AboutUs from "../AboutUs/AboutUs";
import { motion } from "framer-motion"


const ContactUs = () => {
  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ delay: 0.3, duration: 0.35 }}>
      <AboutUs />
      <MDBContainer className="contact-form-container">
        <h2 className="h1-responsive font-weight-bold text-center my-5">
          Contact us
      </h2>
        <p className="text-center w-responsive mx-auto pb-5">
          Always humans, never bots. The sharpest and friendliest support team in
        Internet. Get in touch with us and we’ll get back to you{" "}
          <span>in about 14 minutes.</span>
        </p>
        <MDBRow>
          <MDBCol md="9" className="md-0 mb-5">
            <form>
              <MDBRow>
                <MDBCol md="6">
                  <div className="md-form mb-0">
                    <MDBInput type="text" id="contact-name" label="Your name" />
                  </div>
                </MDBCol>
                <MDBCol md="6">
                  <div className="md-form mb-0">
                    <MDBInput
                      type="text"
                      id="contact-email"
                      label="Your email"
                    />
                  </div>
                </MDBCol>
              </MDBRow>
              <MDBRow>
                <MDBCol md="12">
                  <div className="md-form mb-0">
                    <MDBInput type="text" id="contact-subject" label="Subject" />
                  </div>
                </MDBCol>
              </MDBRow>
              <MDBRow>
                <MDBCol md="12">
                  <div className="md-form mb-0">
                    <MDBInput
                      type="textarea"
                      id="contact-message"
                      label="Your message"
                    />
                  </div>
                </MDBCol>
              </MDBRow>
            </form>
            <div className="text-center text-md-left">
              <MDBBtn color="primary" size="md">
                Send
            </MDBBtn>
            </div>
          </MDBCol>
          <MDBCol md="3" className="text-center">
            <ul className="list-unstyled mb-0">
              <li className="contact-form-info">
                <MDBIcon icon="map-marker-alt" size="2x" id="icons-contact-form" className="blue-text" />
                <p>Varna, 9000, Bulgaria</p>
              </li>
              <li className="contact-form-info">
                <MDBIcon icon="phone" size="2x" id="icons-contact-form" className="blue-text mt-4" />
                <p>+ 359 887866 092</p>
              </li>
              <li className="contact-form-info">
                <MDBIcon icon="envelope" size="2x" id="icons-contact-form" className="blue-text mt-4" />
                <p>eastside.customs.garage@gmail.com</p>
              </li>
            </ul>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
      </motion.div>
    </>
  );
}

export default ContactUs;
