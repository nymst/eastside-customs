import React from "react";
import {
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBIcon,
  MDBBtn,
} from "mdbreact";
import "./AboutUs.css";


const AboutUs = () => {

  return (

    <MDBCard className="my-5 px-1 pb-5 text-center">
      <MDBCardBody>
        <h2 className="h1-responsive font-weight-bold my-5">
          Our amazing team
        </h2>
        <p className="grey-text w-responsive mx-auto mb-5">
          Eastside Customs Garage is changing the way auto repair is done. When you
          bring your vehicle to us, we will lessen, if not eliminate, your pain,
          frustration and inconvenience. We’ll help you put together a realistic
          budget for your yearly car maintenance. You’ll know all your needed
          services, and their costs, well in advance.
        </p>
        <MDBRow>
          <MDBCol md="4" className="mb-md-0 mb-5">
            <img
              tag="img"
              src="Emi.jpg"
              className="rounded z-depth-1-half img-fluid"
              alt="Sample avatar"
            />
            <h4 className="font-weight-bold dark-grey-text my-4">Emiliya Kamenova</h4>
            <MDBBtn tag="a" floating size="sm" className="mx-1 mb-0 btn-fb">
              <MDBIcon fab icon="facebook-f" />
            </MDBBtn>
            <MDBBtn
              tag="a"
              floating
              size="sm"
              className="mx-1 mb-0 btn-dribbble"
            >
              <MDBIcon fab icon="dribbble" />
            </MDBBtn>
            <MDBBtn tag="a" floating size="sm" className="mx-1 mb-0 btn-tw">
              <MDBIcon fab icon="twitter" />
            </MDBBtn>
          </MDBCol>

          <MDBCol md="4" className="mb-md-0 mb-5">
            <img
              tag="img"
              src="Ivo.jpg"
              className="rounded z-depth-1-half img-fluid Ivo"
              alt="Sample avatar"
            />
            <h4 className="font-weight-bold dark-grey-text my-4">Ivo Kovachev</h4>
            <MDBBtn tag="a" floating size="sm" className="mx-1 mb-0 btn-email">
              <MDBIcon icon="envelope" />
            </MDBBtn>
            <MDBBtn tag="a" floating size="sm" className="mx-1 mb-0 btn-fb">
              <MDBIcon fab icon="facebook-f" />
            </MDBBtn>
            <MDBBtn tag="a" floating size="sm" className="mx-1 mb-0 btn-git">
              <MDBIcon fab icon="github" />
            </MDBBtn>
          </MDBCol>
        </MDBRow>
      </MDBCardBody>
    </MDBCard>
  );
};

export default AboutUs;
