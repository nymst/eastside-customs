import React from "react";
import { Link } from "react-router-dom";
import "./Home.css";
import { motion } from "framer-motion"

const Home = () => {
  const handleScrollToStats = () => {
    window.scrollTo({
      top: 0,
    });
  };

  return (
    <>
      <motion.div
      initial={{ opacity: 0}}
      animate={{ opacity: 1}}
      transition={{delay: 0.3, duration: 1}}
      >
        <div
          className="home-page-picture"
          style={{
            backgroundImage: `url(${process.env.PUBLIC_URL}/landing-page-background.jpg`,
          }}
        >
          <h3 className="home-page-picture-title">
            Expert technicians, competitive prices
        </h3>
          <p className="home-page-picture-text">
            We are commited to earning your trust by providing the expertise and
            value you expect!
        </p>

          <Link to='/services'>
            <button className="home-page-picture-button">Our services</button>
          </Link>
        </div>
        <div className="home-page-middle-content">
          <h3 className="why-us">WHY CHOOSE US?</h3>
          <p>
            East Side Customs offers the finest auto repair in Varna. They co-op
            the cost to market together and bring you value & service available
            only at their select auto care center. We can help you with everything
            from an oil change to an engine change. We can handle any problem on
            both foreign and domestic vehicles.
        </p>
          <div className="why-us-icons">
            <div className="why-us-first-icon">
              <img className="why-us-first-icon-img" alt="1" src="1.png"></img>
              <h3>EVERY JOB IS PERSONAL</h3>
              <p>
                {" "}
              If you want the quality you would expect from the dealership, but
              with a more personal and friendly atmosphere, you have found it.
            </p>
            </div>
            <div className="why-us-second-icon">
              <img className="why-us-second-icon-img" alt="2" src="2.png"></img>
              <h3>BEST MATERIALS</h3>
              <p>
                {" "}
              We have invested in all the latest specialist tools and diagnostic
              software that is specifically tailored for the software in your
              vehicle.
            </p>
            </div>
            <div className="why-us-third-icon">
              <img className="why-us-third-icon-img" alt="3" src="3.png"></img>
              <h3>PROFESSIONAL STANDARDS</h3>
              <p>
                {" "}
              East Side Customs is capable of servicing a variety of
              models. We only do the work that is needed to fix your problem.
            </p>
            </div>
          </div>
          <div className="blue-cars-container">
            <div className="blue-cars">
              <img className="blue-cars-img" alt="cars" src="cars.jpg"></img>
            </div>
            <div className="blue-cars-content">
              <h3> BRANDS WE SERVICE</h3>
              <p>
                We provide top notch maintenance service for all types of
                vehicles. We are certified and ready to service and repair your
                vehicle:
              </p>
              <div className="blue-cars-content-models">
                <div className="blue-cars-content-models-first">
                  <p>Seat</p>
                  <p>Renault</p>
                  <p>Alfa Romeo</p>
                </div>
                <div className="blue-cars-content-models-second">
                  <p>Dacia</p>
                  <p>Honda</p>
                  <p>Mitsubishi</p>
                </div>
                <div className="blue-cars-content-models-third">
                  <p>Lexus</p>
                  <p>BMW</p>
                  <p>Toyota</p>
                </div>
              </div>
              <Link to="/brands">
                <button className="blue-cars-content-models-button" onClick={handleScrollToStats}>
                  See more
                  </button>
              </Link>
            </div>
          </div>
        </div>
      </motion.div>

    </>
  );
};

export default Home;
