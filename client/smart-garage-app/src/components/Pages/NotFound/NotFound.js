import React from "react";
import "./NotFound.css";
import { useHistory} from "react-router-dom";
const NotFound = () => {

    const history = useHistory();

    const routeChange = () =>{ 
        history.push("/Home");
      }

  return (
    <div className="container-fluid text-center">
      <div className="px-5 py-5">
        <h1 className="not-found">404</h1>{" "}
        <span className="oops-text">Oops! page not found</span>
        <h4 className="not-found-text mt-4">
          The page you were looking for does not exist! You may have mistyped{" "}
          <br></br>the address or the page may have been moved!
        </h4>
        <div className="text-center mt-4 mb-5">
          {" "}
          <button onClick={routeChange}  className="btn btn-success send px-3">
            <i className="fa fa-long-arrow-left mr-1"></i> Return to main page
          </button>{" "}
        </div>
      </div>
    </div>
  );
};

export default NotFound;
