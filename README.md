# ***EastSide-Customs Garage***
<br>

## Table of contents
   - [1. Description](#1-description)
   - [2. Project information](#2-project-information)
   - [3. Setup](#3-setup)
   - [4. Project structure](#4-project-structure)
   - [5. Working with Postman and postman collections](#5-working-with-postman-and-postman-collections)
   - [6. Documentation - Backend ](#6-documentation---backend)
   - [7. Documentation - Frontend ](#7-documentation---frontend)


### **1. Description**
<br>


А web application that enables the owners of an auto repair shop to manage their day-to-day job. 
It has a list with cars (brand, model) and a list of available services (oil change, filters change etc.) and their price. The system has a list with all customers and their contact information (name, phone, email, etc.). The shop’s employees are able to link specific car to a customer. Each specific car has identification details (registration plate, vehicle identification number, etc.). 
SmartGarage keeps a history of all services done on customer’s car/cars. The application is able to visualize a detailed report for a given visit to the shop. It includes all the performed services for the visit, as well as the total price (in the default currency, chosen by the employee/customer). 
For new customers, а profile could be generated. A profile is generated by the administrator using the system and the login information is sent to customer’s email. The generated profile information is email (the customer’s email address) and password (randomly generated string). Once the login information is generated, it must be sent to the customer via email. All customers are able to access their personal information via the web UI.

<br>

### **2. Project information**

<br>

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **Express**, **ESLint**, **React**
- Libraries: **Material-UI**, **Material-table**, **mdbreact**, **formik**,**framer-motion**

<br>

### **3. Setup**
<br>

You will be working in the `src` folder. This will be designated as the **root** folder, where `package.json` should be placed.

You need to install all the packages in the root folder: `npm i`.

The project can be run in two ways:

- `npm start` - will run the current version of the code and will **not** reflect any changes done to the code until this command is executed again
- `npm run start:dev` - will run the code in *development* mode, meaning every change done to the code will trigger the program to restart and reflect the changes made
- `npm run eslint` - will run the **eslint** file 
- `npm start` - root client starts the frontend part in folder client **root**, where `package.json` should be placed

**Create Environment Variable** - .env file in root **server** folder
```

PORT=5555
HOST=triton.rdb.superhosting.bg
DBPORT=3306
USER=fwraptea_eastside_customs
PASSWORD=Parola12345678
DATABASE=fwraptea_eastside_customs
PRIVATE_KEY=sekreten

```
**Setup DB**
1. Make sure MariaDB database service is running
2. Start your  SQL Client and create new database scheme "fwraptea_eastside_customs", using the **eastsidecustoms.sql** file in **/dbdump/eastsidecustoms.sql**
3. Connect to the database "fwraptea_eastside_customs" .
4. Use the "fwraptea_eastside_customs"  database for the project.

<br>

### 4. Project structure

Project structure consists of two main parts - back-end (folder `server`) and front-end (folder `client`)

**Back-end - folder (server)**

- `src/index.js` - the entry point of the backend project
- `src/auth` - contain settings and logic for authentication 
- `src/common` - contains helper files `constants`, `error-strings`, `roles`
- `src/controllers` - contains all controller logic `auth`, `orders`, `services`, `users`, `vehicles`, `brands`, `invoices`, `user-order`
- `src/data` - contains the connection settings and functions for CRUD over users, services, orders, vehicles, invoices, models in the database
- `src/middleware` -  contains all custom middlewares
- `src/services` - contains all service logic
- `src/validators` - contains objects (called validator) for validating body/query objects


**Front-end - folder (client)**

- `src/App.js` - the entry point of the frontend project
- `src/common` - contains `brands`, `constants`, `material-table-icons`, `utils`, `validator-schema`
- `src/components` - contains `Base` and `Pages` components of resources
- `src/providers` - contains used context objects in the project about authentication

<br>

### 5. Working with Postman and postman collections
1. Download and Install Postman
2. Open the **Postman** tool
3. Click the **Import** button and choose files in the **postman** folder

<br>


### 6. Documentation - Backend

**The following CRUD operations and endpoints are implemented**

<br>

**Public endpoints**

 The public part of DEVA smart garage is accessible without authentication.

  User properties
   - `email` - A valid email address in the range `[3-100]` by which the user is registered in the garage system
   - `password` - string with length in the range `[8-20]`, which user has received after registration in the garage system via email
  
 Endpoints
   - `POST: /auth/ login` - login user
     - body: `{ email: String, password: String}`, validation criteria provided above; 
   - `POST: /auth/reset` - reset password, which check if user exist in the system and created a token for resetting forgotten password. The token is valid for one hour.
     - body: `{ email: String }`, validation criteria provided above; 
     - body: `{ password: String }`, validation criteria provided above; 
   - `GET/brands` - display all brands we service
   - `GET/services` - display all services we offer
<br>

**Private endpoints**


  User/Customer properties


   - `first_name` - string with length in the range `[2-20]` 
   - `last_name` - string with length in the range `[2-20]`
   - `email` - a valid email format, validated via regex
   - `phone_number` - Expected a valid phone format `[(359)-882-974963, 541-753-6010, 541753-6010,541753-6010, 359882974963, 359-882-974963, +359882974963, +359-882-974963]`
   - `street_address` - Expected string in the range `[3-45]`
   - `city` - Expected string in the range `[3-45]`
   - `country` - Expected string in the range `[3-45]`
   - `postal_code` - Expected a valid format `[XXXX (1000-9999)]`
  
  <br>

  Vehicle properties

   - `reg_plate` - Expected string a valid license plate format: `[A-Z]X|XX)([0-9]XXXX)([A-Z]XX`
   - `year` - Expected year format `XXXX - 1900 - 2099 `
   - `class` - Expected string `A`, `B`, `C`, `D`, `E`, `F`, `S`!
   - `vin` - Expected a valid exactly 17 long string BG vehicle identification number
   - `transmissions` - Expected string `manual`, `auto`
   - `engine` - Expected string `petrol`, `diesel`, `hybrid`
   - `model` - Expected string in the range `[1-20]`
   - `brand` - Expected a valid brand from our garage -> see our brand list `(39)`
  
  <br>

  Service properties

   - `name` - Expected string in the range `[3-45]`
   - `price` - Expected number > 0
   - `description`- Expected string in the range [3-500]

  <br>

  Order properties

   - `due_date` - Expected a Valid Date
   - `issue_date` - Expected a Valid Date
   - `status` - Expected string: `New`, ` Progress` or `Done`
   - `notes` - Expected string in the range `[3-50]`
   - `currency` - Expected BGN, USD or EUR
   - `currency_multiplier` 
  <br>  

 Endpoints User/Customer
   - `DELETE: /logout` - logout the current user and deleting token from localStorage
   - `GET: /user/order ` - get an order by a user
   - `GET: /services` - view all services of DEVA garage system
   - `GET: /orders` - view all existing orders, old and current
   - `GET: /activeOrders` - view all current orders
   - `PUT: /users/:id/change_password` - updates current password
   
<br>

 Endpoints User/Employee

   - `GET: /users` - retrieve all users
   - `PUT: /users/:id` - updates the current user profile information 
     - body: `{ first_name: String, last_name:String, email:String, phone_number: String, street_address: String, city: String, country: String, postal_code:String, avatar: file   }`, validation criteria provided above 
   - `POST: /users` - creates a new user and send an email login email and password
     - body: `{ first_name: String, last_name:String, email:String, phone_number: String, street_address: String, city: String, country: String, postal_code:String, avatar: file   }`, validation criteria provided above  
   - `DELETE: /users/:id` - deletes existing user by id
   - `GET: /vehicles` - retrieve all vehicles
   - `PUT: /vehicles/:id` - updates an existing vehicle
     - body: `{ reg_plate: String, year:String, class:String, transmissions: String, engine: String, model: String, brand: String  }`, validation criteria provided above;
   - `POST: /vehicles` - creates a new vehicle
     - body: `{ reg_plate: String, year:String, class:String, vin:String, transmissions: String, engine: String, model: String, brand: String, customer_id: Number  }`, validation criteria provided above;
   - `DELETE: /vehicles/:id` - deletes existing vehicle by id
   - `POST: /services` - creates a new service
     - body: `{ name: String, price: Number  }`, validation criteria provided above;
   - `PUT: /services/:id` - updates price by service id
     - body: `{ price: Number  }`, validation criteria provided above; 
   - `DELETE: /services/:id` - deletes existing service by id
   - `GET: /orders` - retrieve all orders
   - `PUT: /user/order/:ordId` - adding services to order 
     - body: `{ service_id: Number  }`, validation criteria provided above; 
   - `PUT: /order/:ordId/status` - updating status of order by orderId 
     - body: `{ status: String  }`, validation criteria provided above;  
   - `DELETE: orders/:ordId` - deletes existing order by id
   - `POST: /orders` - create order for already created user and vehicle 
     - body: `{ due_date: String, issue_date: String, notes: String, status: String }`, validation criteria provided above
   - `GET: /orders/:id/services`- get all services in existing order
   - `POST: /orders/:id/report`- generate detailed report for existing order
   - `POST: /invoices` - generate invoice for existing order
   - `GET: invoices/customer/:customerId`- get existing invoice by customer id
<br>

### 7. Documentation - Frontend

**Public routes**

   - `Route - /` - Redirecting to `/home` route
   - `Route - /home` - directs to `Home` component 
   - `Route - /brands` - directs to `Brands` component
   - `Route - /services` - directs to `Services` component
   - `Route - /contact_us` - directs to `ContactUs` component
   - `Route - /login` - directs to `Login` component
   - `Route - /reset` - directs ti `ResetPassword` component 
   - `Route - /*` - directs to component `NotFound` in case no resource is found

  
**Private routes**

   - `Route - /orders/:id` - directs to `Order` component, only if the employee is logged in
   - `Route - /user` - directs to `UsersAccount` component and render detailed information about current customer profile
   - `Route - /operations` - directs to `Operations` component and render the dashboard for the employee to perform the required operations
 - `Route - /services/add` - directs to `AddUpdateService` component and render the form for creating and editing existing service.

<br>
<br>

**Homepage View**

![Homepage](./client/smart-garage-app/public/HomePage.png)

![Homepage](./client/smart-garage-app/public/HomePage2.png)

![Homepage](./client/smart-garage-app/public/HomePage3.png)


<br>
<br>


**Services View**

![Services](./client/smart-garage-app/public/Services.png)
![EditService](./client/smart-garage-app/public/EditService.png)
![CreateService](./client/smart-garage-app/public/CreateService.png)
<br>
<br>

**Brands View**

![Brands](./client/smart-garage-app/public/Brands.png)


<br>
<br>

**ContactUS View**

![AboutUs](./client/smart-garage-app/public/AboutUs.png)
![ContactUs](./client/smart-garage-app/public/ContactUs.png)

<br>
<br>

**Login View**

![Login](./client/smart-garage-app/public/LogIn.png)

<br>
<br>



**Send email to reset password View**

![EmailResetPassword](./client/smart-garage-app/public/resetPassEmail.png)

<br>
<br>

**Reset password View**

![Reset](./client/smart-garage-app/public/resetPassword.png)

<br>
<br>


**User account View**

![UserAccount](./client/smart-garage-app/public/userAccount.png)
![OrderView](./client/smart-garage-app/public/OrderView.png)
<br>
<br>



**Operations View**

![UserDasboard](./client/smart-garage-app/public/UserDashboard.png)
![OrdersDashboard](./client/smart-garage-app/public/OrdersDashboard.png)
![VehiclesDashboard](./client/smart-garage-app/public/VehiclesDashboard.png)
<br>
<br>


