import jwt from 'jsonwebtoken';
import { TOKEN_LIFETIME } from '../common/constants.js';
import { config } from '../config.js';

const PRIVATE_KEY = config.PRIVATE_KEY;

const createToken = (payload) => {
  const token = jwt.sign(
    payload,
    PRIVATE_KEY,
    { expiresIn: TOKEN_LIFETIME },
  );

  return token;
};

export default createToken;
                  