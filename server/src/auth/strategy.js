import passportJwt from 'passport-jwt';
import { config } from '../config.js';

const PRIVATE_KEY = config.PRIVATE_KEY;

const options = {
  secretOrKey: PRIVATE_KEY,
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
  const userData = {
    id: payload.id,
    email: payload.email,
    password: payload.password,
    role: payload.role,
    name:payload.first_name,
    last_name:payload.last_name,
    city:payload.city,
    address:payload.street_address,
    phone:payload.phone_number,
  };

  done(null, userData);
});

export default jwtStrategy;
