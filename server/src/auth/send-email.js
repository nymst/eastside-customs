/* eslint-disable no-unused-vars */
import nodemailer from 'nodemailer';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASSWORD,
  },
});

let mailOptions = {
  from: 'eastside.customs.garage@gmail.com',
  to: null,
  subject: null,
  text: null,
};

const transportMailer = () => {
  transporter.sendMail(mailOptions, (err) => {
    if (err) {
      console.log('err occured', err);
    } else {
      console.log('mail sent');
    }

  });
};

export const sendForgottenPassEmail = (receiverEmail, token, first_name, last_name) => {

  const link = `http://localhost:3000/reset?id=${token}`;
  mailOptions.to = receiverEmail;
  mailOptions.subject = 'Eastside Customs - Reset password request';
  mailOptions.text = `
  Hello, ${first_name} ${last_name}! \n
  Someone has requested a link to change your password and you can do this through the link below. \n
  Click on the following link in order to reset your password: \n
  ${link}
  If you didn't request this, please ignore this email.
  Your password won't change until you access the link above and create a new one.

  Best regards, Eastside Customs
`;

  transportMailer();
};

export const sendCredentialsEmail = (receiverEmail, first_name, last_name, password) => {
  mailOptions.to = receiverEmail;
  mailOptions.subject = 'Eastside Customs - Registration successful!';
  mailOptions.text = `
  Hello, ${first_name} ${last_name}! \n
  Thank you for choosing to work with Eastside Customs.
  Below are attached your credentials, required to log in our system and track your orders.
  
  Username: ${receiverEmail}
  Password: ${password}

  If you wish to change your password, log in our system and visit the account settings tab.

  Best regards, Eastside Customs
`;

  transportMailer();
};

export const sendInvoiceEmail = (receiverEmail, first_name, last_name, order_id, invoice) => {
  mailOptions.to = receiverEmail;
  mailOptions.subject = `Eastside Customs - Invoice for order #${order_id}!`;
  mailOptions.attachments = [
    {
      filename: `order${order_id}_invoice.pdf`,
      content: invoice,
    },
  ];
  mailOptions.text = `
  Hello, ${first_name} ${last_name}! \n
  Thank you for choosing to work with Eastside Customs.
  Below is attached your invoice for order #${order_id}.
  Please make sure the payment is processed within the specified deadline.

  Best regards, Eastside Customs
`;

  transportMailer();
};

export const sendReportEmail = (receiverEmail, fullname, order_id, report) => {
  mailOptions.to = receiverEmail;
  mailOptions.subject = `Eastside Customs - Report for order #${order_id}!`;
  mailOptions.attachments = [
    {
      filename: `order${order_id}_report.pdf`,
      content: report,
    },
  ];
  mailOptions.text = `
  Hello, ${fullname}! \n
  Thank you for choosing to work with Eastside Customs.
  Below is attached a detailed report for order #${order_id}.

  Best regards, Eastside Customs
`;

  transportMailer();
};