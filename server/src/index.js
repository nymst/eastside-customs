/* eslint-disable no-unused-vars */
import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import authController from './controllers/auth-controller.js';
import usersController from './controllers/users-controller.js';
import servicesController from './controllers/services-controller.js';
import vehiclesController from './controllers/vehicles-controller.js';
import invoicesController from './controllers/invoices-controller.js';
import { config } from './config.js';
import ordersController from './controllers/orders-controller.js';
import userOrderController from './controllers/user-order-controller.js';
import brandsController from './controllers/brands-controller.js';
import modelsController from './controllers/models-controller.js';

process.env.TZ = 'Europe/Sofia';

// const config = dotenv.config().parsed;
const PORT = config.PORT;

const app = express();
passport.use(jwtStrategy);
app.use(passport.initialize());
app.use(cors());
app.use(helmet());
app.use(express.json());
app.use('/users', usersController);
app.use('/auth', authController);
app.use('/services', servicesController);
app.use('/vehicles', vehiclesController);
app.use('/orders', ordersController);
app.use('/user', userOrderController);
app.use('/invoices', invoicesController);
app.use('/brands', brandsController);
app.use('/models', modelsController);


app.listen(PORT, () => console.log(`Listening on ${PORT}...`));