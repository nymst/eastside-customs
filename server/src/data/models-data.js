import db from './pool.js';

const getModels = async(brand_id) => {
  const sql = `
  SELECT m.name, m.brand_id, m.id AS id
  FROM models m WHERE m.brand_id = ?
  `;

  const result = await db.query(sql,[brand_id]);
  return result;
};

export default {
  getModels,
};
