import db from './pool.js';

const getAll = async (sort, page, limit) => {
  const direction = sort || 'ASC';
  const resultsPerPage = limit || 30;
  const startIndex = page ? (page - 1) * limit : 0;

  const sql = `
    SELECT v.id, m.name AS model, b.name AS brand, b.id AS brand_id, v.year, v.reg_plate, 
    v.class, v.vin, v.transmissions, v.engine,
    concat(u.first_name, ' ' , u.last_name) AS full_name,
    u.email, a.street_address, a.city, a.country, a.postal_code, 
    v.is_deleted, v.customer_id, v.models_id
    
    FROM vehicles AS v
    JOIN models AS m ON  m.id=v.models_id
    JOIN brands AS b ON  b.id=m.brand_id
    JOIN users AS u ON u.id=v.customer_id
    JOIN addresses AS a ON a.id=u.addresses_id
    AND v.is_deleted = 0
    ORDER BY u.first_name ${direction}
    LIMIT ${startIndex}, ${resultsPerPage}
  `;

  return await db.query(sql);
};

const searchBy = async (queries) => {
  const sql1 = `
    SELECT v.id, m.name AS model, b.name AS brand, v.year, v.reg_plate, v.class,
    v.vin, v.transmissions, v.engine, u.first_name, u.last_name, u.email,
    u.phone_number, a.street_address, a.city, a.country, a.postal_code,
    v.is_deleted, v.customer_id, v.models_id
    
    FROM vehicles AS v
    JOIN models AS m ON  m.id = v.models_id
    JOIN brands AS b ON  b.id= m.brand_id
    JOIN users AS u ON u.id = v.customer_id
    JOIN addresses AS a ON a.id = u.addresses_id
    WHERE 
  `;

  const sql2 = queries
    .map(([key, value]) => {
      return `${key} LIKE '%${db.escape(value).replace(/'/g, '')}%'`;
    })
    .join(' AND ');

  return await db.query(sql1 + sql2);
};

const getBy = async (column, value) => {
  const sql = `
  SELECT v.id, m.name AS model, b.name AS brand, v.year, v.reg_plate, 
  v.class, v.vin, v.transmissions, v.engine,
  concat(u.first_name, ' ' , u.last_name) AS full_name,
  u.email, a.street_address, a.city, a.country, a.postal_code, v.is_deleted

  FROM vehicles AS v
  JOIN models AS m ON  m.id=v.models_id
  JOIN brands AS b ON  b.id=m.brand_id
  JOIN users AS u ON u.id=v.customer_id
  JOIN addresses AS a ON a.id=u.addresses_id
  WHERE ${column}=? AND v.is_deleted = 0
  `;

  return await db.query(sql, [value]);
};

const getById = async (id) => {
  const sql = `
    SELECT v.id, m.name AS model, b.name AS brand, b.id AS brand_id, v.year, v.reg_plate, 
    v.class, v.vin, v.transmissions, v.engine,
    concat(u.first_name, ' ' , u.last_name) AS full_name,
    u.email, a.street_address, a.city, a.country, a.postal_code,
    v.is_deleted, v.customer_id, v.models_id

    FROM vehicles v
    JOIN models AS m ON  m.id=v.models_id
    JOIN brands AS b ON  b.id=m.brand_id
    JOIN users AS u ON u.id=v.customer_id
    JOIN addresses AS a ON a.id=u.addresses_id
    WHERE v.id=? AND v.is_deleted=0
  `;

  const result = await db.query(sql, [id]);
  return result[0];
};

const update = async (id, data, modelId) => {
  const sql = `
  UPDATE vehicles v 
  LEFT JOIN models AS m ON m.id=v.models_id
  LEFT JOIN brands AS b ON b.id=m.brand_id
  SET v.reg_plate=?, v.year=?, v.class=?, 
  v.transmissions=?, v.engine=?, m.name=?, v.models_id=?
  WHERE v.id = ? 
  `;

  return await db.query(sql, [
    data.reg_plate,
    data.year,
    data.class,
    data.transmissions,
    data.engine,
    data.model,
    modelId,
    +id,
  ]);
};

const deleted = async (id) => {

  const sql = `
  UPDATE vehicles  
  SET is_deleted=1
  WHERE id=?
  `;

  await db.query(sql, [+id]);

  const sql1 = `SELECT v.id, m.name AS model, b.name AS brand, v.year, v.reg_plate, 
 v.class, v.vin, v.transmissions, v.engine,
 concat(u.first_name, ' ' , u.last_name) AS full_name,
 u.email, a.street_address, a.city, a.country, a.postal_code,
 v.is_deleted, v.customer_id, v.models_id

 FROM vehicles v
 JOIN models AS m ON  m.id=v.models_id
 JOIN brands AS b ON  b.id=m.brand_id
 JOIN users AS u ON u.id=v.customer_id
 JOIN addresses AS a ON a.id=u.addresses_id
 WHERE v.id=? 
 `;

  return await db.query(sql1, [+id]);
};

const searchByModelAndBrand = async (model, brand) => {
  const sql = `
  SELECT m.id , m.name AS model, b.name AS brand
  FROM models AS m
  JOIN brands AS b ON  b.id=m.brand_id
  WHERE m.name=? AND b.name=?
  `;

  return await db.query(sql, [model, brand]);
};

const createModel = async (data) => {
  const sql1 = `
  SELECT b.id
  FROM brands AS b
  WHERE b.name=?
  `;
  const brandId = await db.query(sql1, [data.brand]);

  const sql2 = `
  INSERT INTO models (name, brand_id)
  VALUES (?, ?)
  `;
  return db.query(sql2, [data.model, brandId[0].id]);
};

const create = async (data, modelId) => {
  const sql = `
  INSERT INTO vehicles (reg_plate, year, class, vin, transmissions, engine, models_id, customer_id)
  VALUES (?, ?, ?, ?, ?, ?, ?, ?)
  `;
  return await db.query(sql, [
    data.reg_plate,
    data.year,
    data.class,
    data.vin,
    data.transmissions,
    data.engine,
    modelId,
    data.customer_id,
  ]);
};

const searchModelBy = async (column, value) => {
  const sql = `
    SELECT b.id, b.name AS brand, m.name AS model
    FROM models AS m
    JOIN brands AS b ON  b.id=m.brand_id
    WHERE ${column}=? 
  `;

  return await db.query(sql, [value]);
};


export default {
  searchByModelAndBrand,
  searchModelBy,
  createModel,
  deleted,
  searchBy,
  getById,
  update,
  create,
  getAll,
  getBy,
};
