import pool from './pool.js';

const save = async (token, userId) => {
  const sql = `
  INSERT INTO reset_tokens (token, user_id) 
  VALUES (?, ?)`;

  return await pool.query(sql, [token, userId]);
};

const getToken = async(token) => {
  const sql = `
  SELECT user_id, expiry_time
  FROM reset_tokens
  WHERE token=?
  `;

  const result = await pool.query(sql, [token]);
  return result[0];
};

export default {
  save,
  getToken,
};
