import db from './pool.js';

const getAll = async (sort, date, page, limit) => {
  // const direction = sort || 'ASC';
  // const dueDate = date || 'ASC';
  const resultsPerPage = limit || 15;
  const startIndex = page ? (page - 1) * limit : 0;

  const sql = `
  SELECT o.id AS order_id, u.id AS customer_id, 
  CONCAT(u.first_name, " ", u.last_name) AS fullname, u.email, u.phone_number,
  u.role, u.is_deleted AS user_is_deleted, a.id AS addresses_id, a.street_address,
  a.city, a.country, a.postal_code , v.id AS vehicle_id, m.name AS model_name,
  b.name AS brand_name, v.reg_plate, v.year, v.class, v.vin, v.transmissions, 
  GROUP_CONCAT(ord.services_id) as services_id, o.currency_multiplier,
  v.engine, v.is_deleted AS vehicles_is_deleted, o.issue_date, o.due_date, o.notes,
  o.status, o.is_deleted AS order_is_deleted, GROUP_CONCAT(" ",s.name) AS service_name,
  SUM(s.price * o.currency_multiplier) AS sum, o.currency,
  CONCAT(empl.first_name, " ", empl.last_name) AS employee_full_name

  FROM orders_have_services AS ord
  JOIN orders AS o ON o.id=ord.orders_id
  JOIN services AS s ON s.id=ord.services_id
  JOIN vehicles AS v ON v.id=o.vehicle_id
  JOIN users AS u ON u.id=v.customer_id
  JOIN users AS empl ON empl.id=o.users_id
  JOIN addresses AS a ON a.id=u.addresses_id
  JOIN models AS m ON m.id=v.models_id
  JOIN brands AS b ON b.id=m.brand_id

  WHERE o.is_deleted=0
  GROUP BY order_id
  ORDER BY order_id DESC
  LIMIT ${startIndex}, ${resultsPerPage}
    `;

  const result = await db.query(sql);

  for (const order of result) {
    order.services_id = order.services_id.split(',').map(Number);
  }

  return result;
};

const getByOrder = async (id) => {
  const sql = `
  SELECT o.id AS order_id, u.id AS customer_id, 
  CONCAT(u.first_name, " ", u.last_name) AS fullname, u.email, u.phone_number,
  u.role, u.is_deleted AS user_is_deleted, a.id AS addresses_id, a.street_address,
  a.city, a.country, a.postal_code, v.id AS vehicle_id, m.name AS model_name,
  b.name AS brand_name, v.reg_plate, v.year, v.class, v.vin, v.transmissions, 
  GROUP_CONCAT(ord.services_id) as services_id, o.currency_multiplier,
  GROUP_CONCAT(s.price * o.currency_multiplier) as service_price, i.id AS invoice_id,
  v.engine, v.is_deleted AS vehicles_is_deleted, o.issue_date, o.due_date, o.notes,
  o.status, o.is_deleted AS order_is_deleted, GROUP_CONCAT(s.name) AS service_name,
  SUM(s.price * o.currency_multiplier) AS sum, o.currency,
  CONCAT(empl.first_name, " ", empl.last_name) AS employee_full_name

  FROM orders_have_services AS ord
  JOIN orders AS o ON o.id=ord.orders_id
  JOIN services AS s ON s.id=ord.services_id
  JOIN vehicles AS v ON v.id=o.vehicle_id
  JOIN users AS u ON u.id=v.customer_id
  JOIN users AS empl ON empl.id=o.users_id
  JOIN addresses AS a ON a.id=u.addresses_id
  JOIN models AS m ON m.id=v.models_id
  JOIN brands AS b ON b.id=m.brand_id
  LEFT JOIN invoices AS i ON i.orders_id=o.id
  WHERE o.id=? 
  `;

  const result = await db.query(sql, [id]);
  if (!result[0].order_id) return null;
  result[0].services_id = result[0].services_id.split(',').map(Number);

  return result[0];
};

const getServicesByOrderId = async (order_id) => {
  const sql = `
  SELECT ord.services_id, s.name as service, CONCAT(s.price * o.currency_multiplier, '', o.currency)  as price
  FROM orders_have_services as ord
  JOIN services s on s.id = ord.services_id
  JOIN orders AS o ON ord.orders_id=o.id 
  where ord.orders_id = ?

  `;
  return await db.query(sql, [order_id]);
};

const getCreated = async (id) => {
  const sql = `
  SELECT issue_date, due_date, notes, status, id
  FROM orders
  WHERE id=?;
  `;

  return await db.query(sql, [id]);
};

const getOrderByVehicleId = async (vehicle_id) => {
  const sql = `
  SELECT o.status, o.notes, v.id, v.reg_plate, v.customer_id
  FROM orders o 
  JOIN vehicles v on v.id = o.vehicle_id
  WHERE v.id =? AND o.status !="done"
  `;
  const result = await db.query(sql, [vehicle_id]);
  return result[0];
};

const getBy = async (column, value) => {
  const sql = `
  SELECT o.id AS order_id, u.id AS customer_id, o.currency, o.currency_multiplier,
  CONCAT(u.first_name, " ", u.last_name) AS fullname, u.email, u.phone_number,
  u.role, u.is_deleted AS user_is_deleted, a.id AS addresses_id, a.street_address,
  a.city, a.country, a.postal_code, v.id AS vehicle_id, m.name AS model_name,
  b.name AS brand_name, v.reg_plate, v.year, v.class, v.vin, v.transmissions, ord.services_id,
  GROUP_CONCAT(s.price * o.currency_multiplier) as service_price,
  v.engine, v.is_deleted AS vehicles_is_deleted, o.issue_date, o.due_date, o.notes,
  o.status, o.is_deleted AS order_is_deleted, GROUP_CONCAT(s.name) AS service_name,
  SUM(s.price * o.currency_multiplier) AS sum, 
  CONCAT(empl.first_name, " ", empl.last_name) AS employee_full_name
  
  FROM orders_have_services AS ord
  JOIN orders AS o ON o.id=ord.orders_id
  JOIN services AS s ON s.id=ord.services_id
  JOIN vehicles AS v ON v.id=o.vehicle_id
  JOIN users AS u ON u.id=v.customer_id
  JOIN users AS empl ON empl.id=o.users_id
  JOIN addresses AS a ON a.id=u.addresses_id
  JOIN models AS m ON m.id=v.models_id
  JOIN brands AS b ON b.id=m.brand_id
  WHERE ${column}=? 
  GROUP BY order_id
  `;

  return await db.query(sql, [value]);
};

const getActiveOrders = async (user_id) => {
  const sql = `
  SELECT o.id AS order_id, u.id AS customer_id,
  CONCAT(u.first_name, " ", u.last_name) AS fullname, u.email, u.phone_number,
  u.role, u.is_deleted AS user_is_deleted, a.id AS addresses_id, a.street_address,
  a.city, a.country, a.postal_code, v.id AS vehicle_id, m.name AS model_name,
  b.name AS brand_name, v.reg_plate, v.year, v.class, v.vin, v.transmissions, ord.services_id,
  v.engine, v.is_deleted AS vehicles_is_deleted, o.issue_date, o.due_date, o.notes,
  o.status, o.is_deleted AS order_is_deleted, GROUP_CONCAT(s.name) AS service_name,
  SUM(s.price) AS sum, CONCAT(empl.first_name, " ", empl.last_name) AS employee_full_name
  
  FROM orders_have_services AS ord
  JOIN orders AS o ON o.id=ord.orders_id
  JOIN services AS s ON s.id=ord.services_id
  JOIN vehicles AS v ON v.id=o.vehicle_id
  JOIN users AS u ON u.id=v.customer_id
  JOIN users AS empl ON empl.id=o.users_id
  JOIN addresses AS a ON a.id=u.addresses_id
  JOIN models AS m ON m.id=v.models_id
  JOIN brands AS b ON b.id=m.brand_id
  WHERE u.id=? and o.status !="Done"
  GROUP BY order_id
  
  `;

  return await db.query(sql, [user_id]);
};


const getById = async (id) => {
  const sql = `
  SELECT ord.orders_id, ord.services_id, s.name, i.id AS invoice_id
  FROM orders_have_services AS ord
  JOIN services AS s ON s.id=ord.services_id
  LEFT JOIN invoices AS i ON ord.id=i.orders_id
  WHERE ord.orders_id=?
`;

  const order = await db.query(sql, [id]);

  return order[0];
};

const getService = async (id, service_id) => {
  const sql = `
  SELECT ord.orders_id, ord.services_id, s.name
  FROM orders_have_services AS ord
  JOIN services AS s ON s.id=ord.services_id
  WHERE orders_id=?
  AND s.id=?
  GROUP BY s.id
  `;

  const service = await db.query(sql, [id, service_id]);

  return service[0];
};

const deleteOrder = async (id) => {
  const sql = `
  UPDATE orders
  SET is_deleted=1, status="Done"
  WHERE id=?;
  `;

  return await db.query(sql, [id]);
};

const update = async (order, id) => {
  const toUpdate = Object.keys(order).map((key) => {
    if (order[key]) {
      return `${key} = ?`;
    }
  });
  const values = Object.values(order).filter((value) => !!value);

  const sql = `
  UPDATE orders 
  SET ${toUpdate.join(', ')}
  WHERE orders.id=?
  `;

  return await db.query(sql, [...values, id]);
};

const create = async (order) => {
  const { due_date, notes, vehicle_id, users_id } = order;
  const sql = `
  INSERT INTO orders (due_date, notes, vehicle_id, users_id)
  VALUES (?, ?, ?, ?)
  `;

  const createdOrder = await db.query(sql, [
    due_date,
    notes,
    vehicle_id,
    users_id,
  ]);

  return {
    id: createdOrder.insertId,
    due_date: due_date,
    notes: notes,
    vehicle_id: vehicle_id,
    users_id: users_id,
  };
};

const addService = async (order_id, service_id) => {
  const sql = `
  INSERT INTO orders_have_services (orders_id, services_id)
  VALUES (?, ?)
  `;

  const createServiceInOrder = await db.query(sql, [order_id, service_id]);

  return {
    id: createServiceInOrder.insertId,
    order_id: order_id,
    service_id: service_id,
  };
};

const deleteOrderServices = async (id) => {
  const sql = `
  DELETE
  FROM orders_have_services
  WHERE orders_id=?
  `;

  await db.query(sql, [id]);
};

export default {
  getAll,
  getBy,
  getById,
  getServicesByOrderId,
  getByOrder,
  getActiveOrders,
  deleteOrder,
  update,
  addService,
  getService,
  create,
  getCreated,
  getOrderByVehicleId,
  deleteOrderServices,
};
