import db from './pool.js';

const getAll = async (sort, page, limit) => {
  const direction = sort || 'ASC';
  const resultsPerPage = limit || 10;
  const startIndex = page ? (page - 1) * limit : 0;

  const sql = `
  SELECT i.id, i.issue_date, i.due_date, i.number, i. orders_id, i.total,
  o.notes, o.is_deleted, o.status,o.vehicle_id, o.currency,
 u.first_name, u.last_name, GROUP_CONCAT(s.name) as services
 FROM invoices i
 JOIN orders o on o.id = i.orders_id
 JOIN orders_have_services ohs on ohs.orders_id=o.id
 JOIN services s on s.id = ohs.services_id
 JOIN vehicles v on v.id = o.vehicle_id
 JOIN users u on u.id = v.customer_id
 GROUP BY i. orders_id
  ORDER BY first_name ${direction}
  LIMIT ${startIndex}, ${resultsPerPage};
        `;

  return await db.query(sql);
};

const searchBy = async (queries) => {
  const sql1 = `  
  SELECT i.id, i.issue_date, i.due_date,i. orders_id, i.total,
   o.notes, o.is_deleted, o.status,o.vehicle_id,
  u.first_name, u.last_name, GROUP_CONCAT(s.name) as services
  FROM invoices i
  JOIN orders o on o.id = i.orders_id
  JOIN orders_have_services ohs on ohs.orders_id=o.id
  JOIN services s on s.id = ohs.services_id
  JOIN vehicles v on v.id = o.vehicle_id
  JOIN users u on u.id = v.customer_id
  GROUP BY i.orders_id
  HAVING
`;
  const sql2 = queries
    .map(([key, value]) => {
      return `${key} LIKE '%${db.escape(value).replace(/'/g, '')}%'`;
    })
    .join(' AND ');

  return await db.query(sql1 + sql2);
};

const getBy = async (column, value) => {
  const sql = `
  SELECT i.id, i.issue_date, i.due_date,i.orders_id, i.total,
  o.is_deleted, o.status,o.vehicle_id, v.customer_id, o.currency,
  u.first_name, u.last_name, u.email, a.street_address, a.city, a.postal_code, 
  a.country, s.name as services, v.reg_plate

  FROM invoices i
  JOIN orders o on o.id = i.orders_id
  JOIN orders_have_services ohs on ohs.orders_id=o.id
  JOIN services s on s.id = ohs.services_id
  JOIN vehicles v on v.id = o.vehicle_id
  JOIN users u on u.id = v.customer_id
  JOIN addresses a on u.addresses_id = a.id
WHERE ${column}=?
    `;

  const result = await db.query(sql, [value]);
  return result[0];
};

const create = async (orders_id, total) => {
  const sql = `
    INSERT INTO invoices (orders_id, total)
    VALUES (?, ?)
  `;

  const createdInvoice = await db.query(sql, [orders_id, total]);

  const invoiceId = createdInvoice.insertId;

  return {
    id: invoiceId,
    orders_id: orders_id,
  };
};

const getAllInvoicesByUser = async (customer_id) => {
  const sql = `
  SELECT o.notes, o.is_deleted, o.status,o.vehicle_id,
  u.first_name, u.last_name, u.email, a.street_address, a.city, a.postal_code, 
  a.country, s.name as services, v.customer_id
  FROM invoices i
  JOIN orders o on o.id = i.orders_id
  JOIN orders_have_services ohs on ohs.orders_id=o.id
  JOIN services s on s.id = ohs.services_id
  JOIN vehicles v on v.id = o.vehicle_id
  JOIN users u on u.id = v.customer_id
  JOIN addresses a on u.addresses_id = a.id
  WHERE v.customer_id=?
  `;
  return await db.query(sql, [customer_id]);
};

export default {
  getAll,
  searchBy,
  getBy,
  create,
  getAllInvoicesByUser,
};
