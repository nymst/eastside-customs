import db from './pool.js';

const getAll = async (user, sort, page, limit) => {
  const direction = sort || 'ASC';
  const resultsPerPage = limit || 30;
  const startIndex = page ? page * limit : 0;

  const sql = `
  SELECT u.id, u.first_name, u.last_name, CONCAT(u.first_name, " ", u.last_name) AS fullname, u.phone_number,
  u.email, u.password, u.role,
  a.postal_code, a.country, a.city, a.street_address
  FROM users AS u
  JOIN addresses AS a ON u.addresses_id=a.id
  WHERE is_deleted=0
  ORDER BY first_name ${direction}
  LIMIT ${startIndex}, ${resultsPerPage};
  `;

  return await db.query(sql);
};

const getBy = async (column, value) => {
  const sql = `
  SELECT u.id, u.first_name, u.last_name, u.email, u.phone_number,
  u.password, u.role,
  a.postal_code, a.country, a.city, a.street_address
  FROM users AS u
  JOIN addresses AS a ON u.addresses_id=a.id
  WHERE ${column}=?
    `;

  const result = await db.query(sql, [value]);
  return result[0];
};

const searchBy = async (column, value) => {
  const sql = `
  SELECT u.first_name, u.last_name, u.email, u.phone_number,
  u.password, u.role,
  a.postal_code, a.country, a.city, a.street_address
  FROM users AS u
  JOIN addresses AS a ON u.addresses_id=a.id
  WHERE ${column} LIKE '%${db.escape(value).replace(/'/g, '')}%' 
    `;

  return await db.query(sql);
};

const create = async (user) => {
  const { first_name, last_name, email, phone_number, password, role, addresses_id } = user;
  const sql = `
    INSERT INTO users (first_name, last_name, email, 
      phone_number, password, role, addresses_id)
    VALUES (?, ?, ?, ?, ?, ?, ?)
  `;

  const createdUser = await db.query(sql, [
    first_name,
    last_name,
    email,
    phone_number,
    password,
    role,
    addresses_id,
  ]);

  const userId = createdUser.insertId;

  return {
    id: userId,
    first_name: first_name,
    last_name: last_name,
    email: email,
    phone_number: phone_number,
    role: role,
    addresses_id: addresses_id,
  };
};

const createAddress = async (postal_code, country, city, street_address) => {
  const sql = `
  INSERT INTO addresses (postal_code, country, city, 
    street_address)
  VALUES (?, ?, ?, ?)
`;

  const address = await db.query(sql, [postal_code, country, city, street_address]);

  return {
    id: address.insertId,
    postal_code: postal_code,
    country: country,
    city: city,
    street_address: street_address,
  };
};

const update = async (user, id) => {
  const toUpdate = Object.keys(user).map((key) => {
    if (user[key]) {

      return `${key} = ?`;
    }
  });

  const values = Object.values(user).filter((value) => {
    if (value) {

      return true;
    }

    return false;
  });

  const sql = ` 
  UPDATE users u
  JOIN addresses a ON a.id=u.addresses_id
  SET ${toUpdate.join(', ')}
  WHERE u.id=?
   `;

  return await db.query(sql, [...values, id]);
};


const remove = async (user) => {
  if (!user) {
    return null;
  }

  const sql = `
  UPDATE users
  SET is_deleted=1
  WHERE id=?
  `;
  return await db.query(sql, [user.id]);
};

const updateUserPassword = async (id, password) => {
  const sql = `
  UPDATE users
  SET password=?
  WHERE id=?
  `;

  return await db.query(sql, [password, id]);
};

export const logout = async (token) => {
  const sql = `
    INSERT INTO tokens (token) 
    VALUES (?)
  `;

  return await db.query(sql, [token]);
};

export default {
  getAll,
  getBy,
  searchBy,
  create,
  createAddress,
  update,
  remove,
  updateUserPassword,
  logout,
};
