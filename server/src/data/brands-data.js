import db from './pool.js';

const getAll = async() => {
  const sql = `
  SELECT id, name, logo
  FROM brands
  `;

  return await db.query(sql);
};

export default {
  getAll,
};
