import db from './pool.js';

const getAll = async () => {
  const sql = `
  SELECT s.name, s.price, s.id, s.is_deleted, s.description
  FROM services AS s
  `;

  return await db.query(sql);
};

const getBy = async (column, value) => {
  const sql = `
  SELECT s.name, s.price, s.id, s.is_deleted, s.description
  FROM services AS s
  WHERE ${column}=? AND s.is_deleted = 0
  `;

  const result = await db.query(sql, [value]);
  return result[0];
};

const create = async (name, price, description) => {
  const sql = `
  INSERT INTO services (name, price, description)
  VALUES (?, ?, ?)
  `;

  return await db.query(sql, [name, price, description]);
};

const update = async (id, name, price, description) => {
  const sql = `
  UPDATE services AS s
  SET name=?, price=?, description=?
  WHERE s.id=?
  `;

  return await db.query(sql, [name, price, description, id]);
};

const remove = async (id) => {
  const sql = `
  UPDATE services
  SET is_deleted=1
  WHERE id=?
  `;

  return await db.query(sql, [id]);
};
export default {
  getAll,
  getBy,
  create,
  update,
  remove,
};
