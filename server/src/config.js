import dotenv from 'dotenv';

export const config = dotenv.config().parsed;

export const DB_CONFIG = {
  port: config.DBPORT,
  database: config.DATABASE,
  user: config.USER,
  password: config.PASSWORD,
  host: config.HOST,
  connectionLimit: 1,
  dateStrings: 'date',
};
