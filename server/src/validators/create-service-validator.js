import {
  SERVICE_NAME_MAX_LENGTH,
  SERVICE_NAME_MIN_LENGTH,
  MIN_SERVICE_PRICE,
  MIN_DESCRIPTION_LENGTH,
  MAX_DESCRIPTION_LENGTH,
} from '../common/constants.js';

export default {
  name: (value) => typeof value === 'string' &&
    value.length > SERVICE_NAME_MIN_LENGTH &&
    value.length <= SERVICE_NAME_MAX_LENGTH,
  price: (value) => typeof value === 'number' &&
    value > MIN_SERVICE_PRICE,
  description: (value) => typeof value === 'string' &&
    value.length >= MIN_DESCRIPTION_LENGTH &&
    value.length <= MAX_DESCRIPTION_LENGTH,
};