import {
  CITY_MAX_LENGTH,
  CITY_MIN_LENGTH,
  COUNTRY_MAX_LENGTH,
  COUNTRY_MIN_LENGTH,
  emailRegex,
  FIRST_NAME_MAX_LENGTH,
  FIRST_NAME_MIN_LENGTH,
  LAST_NAME_MAX_LENGTH,
  LAST_NAME_MIN_LENGTH,
  phoneRegex,
  PHONE_NUMBER_LENGTH,
  POSTAL_CODE_MAX_LENGTH,
  POSTAL_CODE_MIN_LENGTH,
} from '../common/constants.js';

export default {
  first_name: (value) =>
    typeof value === 'undefined' ||
    (typeof value === 'string' &&
      value.length >= FIRST_NAME_MIN_LENGTH &&
      value.length <= FIRST_NAME_MAX_LENGTH),
  last_name: (value) =>
    typeof value === 'undefined' ||
    (typeof value === 'string' &&
      value.length >= LAST_NAME_MIN_LENGTH &&
      value.length <= LAST_NAME_MAX_LENGTH),
  email: (value) =>
    typeof value === 'undefined' ||
    (typeof value === 'string' && value.match(emailRegex)),
  phone_number: (value) =>
    typeof value === 'undefined' ||
    (typeof value === 'string' &&
      value.length === PHONE_NUMBER_LENGTH &&
      value.match(phoneRegex)),
  postal_code: (value) =>
    typeof value === 'undefined' ||
    (typeof value === 'string' &&
      value.length >= POSTAL_CODE_MIN_LENGTH &&
      value.length <= POSTAL_CODE_MAX_LENGTH),
  country: (value) =>
    typeof value === 'undefined' ||
    (typeof value === 'string' &&
      value.length >= COUNTRY_MIN_LENGTH &&
      value.length <= COUNTRY_MAX_LENGTH),
  city: (value) =>
    typeof value === 'undefined' ||
    (typeof value === 'string' &&
      value.length >= CITY_MIN_LENGTH &&
      value.length <= CITY_MAX_LENGTH),
  street_address: (value) =>
    typeof value === 'undefined' || typeof value === 'string',
};
