import { MAX_NOTES_LENGTH, MIN_NOTES_LENGTH, STATUS_DONE, STATUS_PROGRESSING, STATUS_NEW } from '../common/constants.js';

export default {
  status: (value) =>
    typeof value === 'undefined' ||
    (typeof value === 'string' &&
      value === STATUS_NEW || value === STATUS_PROGRESSING || value === STATUS_DONE),
  due_date: (value) =>
    typeof value === 'undefined' ||
    !new Date(value).toString().includes('Invalid'),
  notes: (value) =>
    typeof value === 'undefined' ||
    (typeof value === 'string' &&
      value.length >= MIN_NOTES_LENGTH &&
      value.length <= MAX_NOTES_LENGTH),
  services_id: (value) =>
    typeof value === 'undefined' ||
    Array.isArray(value) &&
    value.length > 0,
};