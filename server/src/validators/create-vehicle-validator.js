import {
  classRegex,
  vinRegex,
  yearRegex,
  regPlateRegex,
  MODEL_MIN_LENGTH,
  MODEL_MAX_LENGTH,
  MIN_REG_PLATE_LENGTH,
  MAX_REG_PLATE_LENGTH,
} from '../common/constants.js';

export default {
  reg_plate: (value) => typeof value === 'string' && 
    value.length >= MIN_REG_PLATE_LENGTH && 
    value.length <= MAX_REG_PLATE_LENGTH,
  year: (value) => typeof value === 'string' && value.match(yearRegex),
  class: (value) => typeof value === 'string' && value.match(classRegex),
  vin: (value) => typeof value === 'string' && value.match(vinRegex),
  transmissions: (value) =>
    typeof value === 'string' && ['manual', 'auto'].includes(value),
  engine: (value) =>
    typeof value === 'string' && ['petrol', 'diesel', 'hybrid'].includes(value),
  model: (value) =>
    typeof value === 'string' &&
    value.length >= MODEL_MIN_LENGTH &&
    value.length <= MODEL_MAX_LENGTH,
  brand: (value) =>
    typeof value === 'string' &&
    [
      'Seat',
      'Renault',
      'Peugeot',
      'Dacia',
      'Citroën',
      'Opel',
      'Alfa Romeo',
      'Škoda',
      'Chevrolet',
      'Porsche',
      'Honda',
      'Subaru',
      'Mazda',
      'Mitsubishi',
      'Lexus',
      'Toyota',
      'BMW',
      'Volkswagen',
      'Suzuki',
      'Mercedes-Benz',
      'Saab',
      'Audi',
      'Kia',
      'Land Rover',
      'Dodge',
      'Chrysler',
      'Ford',
      'Hummer',
      'Hyundai',
      'Infiniti',
      'Jaguar',
      'Jeep',
      'Nissan',
      'Volvo',
      'Tewoo',
      'Fiat',
      'MINI',
      'Rover',
      'Smart',
    ].includes(value),
};
