import { PASSWORD_MIN_LENGTH } from '../common/constants.js';

export default {
  password: (value) =>
    typeof value === 'string' && value.length >= PASSWORD_MIN_LENGTH,
};
