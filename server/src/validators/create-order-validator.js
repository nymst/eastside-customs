import { MAX_NOTES_LENGTH, MIN_NOTES_LENGTH } from '../common/constants.js';

export default {
  vehicle_id: (value) =>
    typeof value !== 'undefined',
  due_date: (value) =>
    !new Date(value).toString().includes('Invalid'),
  notes: (value) =>
    typeof value === 'string' &&
    value.length >= MIN_NOTES_LENGTH &&
    value.length <= MAX_NOTES_LENGTH,
  services_id: (value) =>
    Array.isArray(value) &&
    value.length > 0,
};