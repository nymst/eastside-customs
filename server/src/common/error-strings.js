/* eslint-disable quotes */
import { CITY_MAX_LENGTH, CITY_MIN_LENGTH, COUNTRY_MAX_LENGTH, COUNTRY_MIN_LENGTH, emailRegex, FIRST_NAME_MAX_LENGTH, FIRST_NAME_MIN_LENGTH, invoiceNumRegex, LAST_NAME_MAX_LENGTH, LAST_NAME_MIN_LENGTH, MAX_DESCRIPTION_LENGTH, MAX_NOTES_LENGTH, MIN_DESCRIPTION_LENGTH, MIN_NOTES_LENGTH, MIN_SERVICE_PRICE, MODEL_MAX_LENGTH, MODEL_MIN_LENGTH, PASSWORD_MIN_LENGTH, phoneRegex, POSTAL_CODE_MAX_LENGTH, POSTAL_CODE_MIN_LENGTH, SERVICE_NAME_MAX_LENGTH, SERVICE_NAME_MIN_LENGTH, STATUS_DONE, STATUS_PROGRESSING, STATUS_NEW } from './constants.js';

export default {

  user: {
    first_name: `Expected string with length [${FIRST_NAME_MIN_LENGTH}-${FIRST_NAME_MAX_LENGTH}]`,
    last_name: `Expected string with length [${LAST_NAME_MIN_LENGTH}-${LAST_NAME_MAX_LENGTH}]`,
    email: `Expected string that match ${emailRegex}`,
    phone_number: `Expected string that match ${phoneRegex}`,
    password: `Expected string with min length: ${PASSWORD_MIN_LENGTH}`,
    postal_code: `Expected string with length [${POSTAL_CODE_MIN_LENGTH}-${POSTAL_CODE_MAX_LENGTH}]`,
    country: `Expected string with length [${COUNTRY_MIN_LENGTH}-${COUNTRY_MAX_LENGTH}]`,
    city: `Expected string with length [${CITY_MIN_LENGTH}-${CITY_MAX_LENGTH}]`,
    street_address: 'Expected string',
  },

  password: {
    password: `Expected string with length at least ${PASSWORD_MIN_LENGTH}`,
  },

  vehicle: {
    reg_plate: 'Expected a valid Bulgarian license plate format.',
    year: 'Expected year format XXXX - 1900 - 2099!',
    class: 'Expected string \'A\', \'B\', \'C\', \'D\', \'E\', \'F\', \'S\'!',
    vin: 'Expected a valid exactly 17 long string BG vehicle identification number!',
    transmissions: 'Expected string \'manual\', \'auto\'!',
    engine: 'Expected string \'petrol\', \'diesel\', \'hybrid\'!',
    model: `Expected string ${MODEL_MIN_LENGTH}-${MODEL_MAX_LENGTH}length!`,
    brand: 'Expected a valid brand from our garage ->see our brand list (39)!',
  },
  order: {
    vehicle_id: "No vehicle chosen",
    status: `Expected one of the following statuses: ${STATUS_NEW}, ${STATUS_PROGRESSING}, ${STATUS_DONE}`,
    notes: `Expected a string with length [${MIN_NOTES_LENGTH} - ${MAX_NOTES_LENGTH}]!`,
    due_date: 'Expected a valid date',
    services_id: 'Expected an array with existing service',
  },
  service: {
    name: `Expected string with length ${SERVICE_NAME_MIN_LENGTH} - ${SERVICE_NAME_MAX_LENGTH}`,
    price: `Expected a price higher than ${MIN_SERVICE_PRICE}`,
    description: `Expected a string with length ${MIN_DESCRIPTION_LENGTH} - ${MAX_DESCRIPTION_LENGTH}`
  },

  invoice: {
    due_date: 'Expected to be instance of new Date()',
    number: `Expected invoice number format ${invoiceNumRegex}`,
  },
};