/* eslint-disable quotes */
import { readFileSync } from 'fs';
import { dirname } from 'path';
import { fileURLToPath } from 'url';
import pkg from "pdf-creator-node";
const { create } = pkg;

const __dirname = dirname(fileURLToPath(import.meta.url));

const options = {
  format: "A4",
  orientation: "portrait",
  border: "10mm",
};

export const generatePdf = (templateName, pdfData) => {
  const template = readFileSync(`${__dirname}/${templateName}.html`, "utf8");
  const document = {
    html: template,
    data: {
      data: pdfData,
      services: pdfData.services,
      total: pdfData.total,
    },
    path: "./output.pdf",
    type: "buffer",
  };

  return create(document, options);
};