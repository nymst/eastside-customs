export const roleMiddleware = role => {
  return async(req, res, next) => {
  
    if (req.user.role !== role) {
      return res.status(403).json({ error: 'You\'re not authorized!'});
    }
    
    await next();
  };
};
    
  
    