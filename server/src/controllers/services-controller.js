import express from 'express';
import { userRole } from '../common/roles.js';
import servicesData from '../data/services-data.js';
import { authMiddleware } from '../middlewares/auth-middleware.js';
import loggedUserMiddleware from '../middlewares/logged-user-middleware.js';
import { roleMiddleware } from '../middlewares/role-middleware.js';
import validateBody from '../middlewares/validate-body.js';
import serviceErrors from '../services/service-errors.js';
import servicesService from '../services/service-service.js';
import createServiceValidator from '../validators/create-service-validator.js';
import updateServiceValidator from '../validators/update-service-validator.js';


const servicesController = express.Router();

servicesController

  // get all services
  .get('/', async (req, res) => {
    const result = await servicesService.getAll(servicesData)();
    res.status(200).json(result);
  })

  // get service by id
  .get('/:id', authMiddleware, loggedUserMiddleware, roleMiddleware(userRole.Employee), async (req, res) => {
    const id = +req.params.id;
    const { error, service } = await servicesService.getById(servicesData)(id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).json({
        message: 'Service not found',
      });
    } else {
      res.status(200).json({ service });
    }
  })

  // create a service
  .post('/',
    authMiddleware,
    loggedUserMiddleware,
    roleMiddleware(userRole.Employee),
    validateBody('services', createServiceValidator),
    async (req, res) => {
      const { name, price, description } = req.body;
      const { error, service } = await servicesService.create(servicesData)(name, price, description);

      if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(409).json({ message: 'This service already exists' });
      } else {
        res.status(200).json({ service });
      }
    })

  // update a service
  .put('/:id',
    authMiddleware,
    loggedUserMiddleware,
    roleMiddleware(userRole.Employee),
    validateBody('service', updateServiceValidator),
    async (req, res) => {
      const id = +req.params.id;
      const { name, price, description } = req.body;
      const { error, service } = await servicesService.update(servicesData)(id, name, +price, description);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).json({
          message: 'Service not found',
        });
      } else if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(404).json({
          message: 'This service already exists',
        });
      }else {
        res.status(200).json({ service });
      }
    })

  // delete a service
  .delete('/:id', authMiddleware, loggedUserMiddleware, roleMiddleware(userRole.Employee), async (req, res) => {
    const id = +req.params.id;
    const { error, message } = await servicesService.remove(servicesData)(id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).json({
        message: 'Service not found',
      });
    } else {
      res.status(200).json({ message });
    }
  });

export default servicesController;