import modelsData from '../data/models-data.js';
import modelService from '../services/model-service.js';
import express from 'express';
import { authMiddleware } from '../middlewares/auth-middleware.js';
import loggedUserMiddleware from '../middlewares/logged-user-middleware.js';


const modelsController = express.Router();

modelsController

  // get all models by brand_id
  .get(
    '/brands/:brandId',
    authMiddleware,
    loggedUserMiddleware,
    async (req, res) => {
      const {brandId} = req.params;
      const result = await modelService.getAllModelsByBrand(modelsData)(+brandId);
      
      res.status(200).json(result);
    },
  );

export default modelsController;