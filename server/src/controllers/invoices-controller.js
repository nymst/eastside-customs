import express from 'express';
import { sendInvoiceEmail } from '../auth/send-email.js';
import { INVOICE_TEMPLATE } from '../common/constants.js';
import { userRole } from '../common/roles.js';
import invoicesData from '../data/invoices-data.js';
import ordersData from '../data/orders-data.js';
import { authMiddleware } from '../middlewares/auth-middleware.js';
import loggedUserMiddleware from '../middlewares/logged-user-middleware.js';
import { roleMiddleware } from '../middlewares/role-middleware.js';
import { generatePdf } from '../pdf/generate-pdf.js';
import invoiceService from '../services/invoice-service.js';
import orderService from '../services/order-service.js';
import serviceErrors from '../services/service-errors.js';


const invoicesController = express.Router();

invoicesController
  // get all invoices
  .get(
    '/',
    authMiddleware,
    loggedUserMiddleware,
    roleMiddleware(userRole.Employee),
    async (req, res) => {
      const queries = req.query;

      const invoices = await invoiceService.getAllInvoices(invoicesData)(
        queries,
      );

      res.status(200).send(invoices);
    },
  )

  // get invoices by customer_id
  .get('/customer/:customerId', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const { id: customerId } = req.user;

    const invoices = await invoiceService.getAllInvoicesByCustomerID(invoicesData)(+customerId);
    res.status(200).json(invoices);

  })

  // get invoice by id and receive it on email
  .get('/:id', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const { id } = req.params;
    const { id: user_id } = req.user;
    const { error, invoice } = await invoiceService.getInvoiceById(
      invoicesData,
    )(id, user_id);
    const orderServices = await orderService.getAllServicesByOrder(ordersData)(invoice.orders_id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'Invoice not found!' });
    } else {
      console.log(invoice);
      const generatedPdf = await generatePdf(INVOICE_TEMPLATE, { ...invoice, ...orderServices });

      sendInvoiceEmail(invoice.email, invoice.first_name,
        invoice.last_name, invoice.orders_id, generatedPdf);
      res.status(200).send(invoice);
    }
  })


  // create invoice
  .post(
    '/',
    authMiddleware,
    loggedUserMiddleware,
    roleMiddleware(userRole.Employee),
    async (req, res) => {
      const { orders_id, total, currency } = req.body;
      const { error, invoice } = await invoiceService.createInvoice(
        invoicesData,
        ordersData,
      )(orders_id, total);
      const orderServices = await orderService.getAllServicesByOrder(ordersData)(orders_id);

      if (error === serviceErrors.DUPLICATE_RECORD) {
        return res.status(409).json({ message: 'An invoice for this order already exists' });
      } else if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'No such order!' });
      } else {

        const createdInvoice = await invoiceService.getInvoiceById(invoicesData)(invoice.id);
        const invoiceForPdf = createdInvoice.invoice;
        const generatedPdf = await generatePdf(INVOICE_TEMPLATE, { ...invoiceForPdf, ...orderServices, currency });

        sendInvoiceEmail(invoiceForPdf.email, invoiceForPdf.first_name,
          invoiceForPdf.last_name, invoiceForPdf.orders_id, generatedPdf);
        res.status(201).json(invoice);
      }
    },
  );

export default invoicesController;
