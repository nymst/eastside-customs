/* eslint-disable no-unused-vars */
import express from 'express';
import createToken from '../auth/create-token.js';
import userService from '../services/user-service.js';
import usersData from '../data/users-data.js';
import serviceErrors from '../services/service-errors.js';
import resetTokenService from '../services/reset-token-service.js';
import { sendForgottenPassEmail } from '../auth/send-email.js';
import validateBody from '../middlewares/validate-body.js';
import resetPasswordValidator from '../validators/reset-password-validator.js';

const authController = express.Router();

authController
  // sign in
  .post('/login', async (req, res) => {
    const { email, password } = req.body;
    const { error, user } = await userService.signInUser(usersData)(
      email,
      password,
    );

    if (error === serviceErrors.INVALID_SIGNIN) {
      res.status(401).send({
        message: 'Invalid email/password',
      });
    } else {
      const payload = {
        id: user.id,
        email: user.email,
        password: user.password,
        role: user.role,
        name:user.first_name,
        last_name:user.last_name,
        city:user.city,
        address:user.street_address,
        phone:user.phone_number,
      };
      const token = createToken(payload);
  
      res.status(200).send({
        token: token,
      });
    }
  })

  // logout
  .delete('/logout', async (req, res) => {
    await usersData.logout(req.headers.authorization.replace('Bearer ', ''));
    res.json({ message: 'Successfully logged out!' });
  })

  //Forgotten password
  .post('/forgot', async (req, res) => {
    const { error, user } = await userService.getByEmail(req.body.email);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({
        message: 'User not found',
      });
    } else {
      const token = await resetTokenService.create(user.id);

      sendForgottenPassEmail(user.email, token, user.first_name, user.last_name);
      res.status(200).json({ token });
    }
  })

  //Reset password
  .post(
    '/reset',
    validateBody('password', resetPasswordValidator),
    async (req, res) => {
      const password = req.body.password;
      const token = req.query.id;
      const tokenResult = await resetTokenService.getToken(token);
      const tokenError = tokenResult.error;
      const foundToken = tokenResult.token;
      if (tokenError === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).json({
          message: 'Token not found',
        });
      }

      if (tokenError === serviceErrors.OPERATION_NOT_ALLOWED) {
        res.status(405).send({
          message: 'The link has expired. Please request a new one.',
        });
      }

      const { error, message } = await userService.resetPassword(
        foundToken.user_id,
        password,
      );

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({
          message: 'User not found',
        });
      } else {
        res.status(200).json({ message });
      }
    },
  );
export default authController;
