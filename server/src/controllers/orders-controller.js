import express from 'express';
import orderService from '../services/order-service.js';
import serviceErrors from '../services/service-errors.js';
import ordersData from '../data/orders-data.js';
import vehiclesData from '../data/vehicles-data.js';
import { authMiddleware } from '../middlewares/auth-middleware.js';
import loggedUserMiddleware from '../middlewares/logged-user-middleware.js';
import { roleMiddleware } from '../middlewares/role-middleware.js';
import { userRole } from '../common/roles.js';
import validateBody from '../middlewares/validate-body.js';
import updateOrderValidator from '../validators/update-order-validator.js';
import createOrderValidator from '../validators/create-order-validator.js';
import { generatePdf } from '../pdf/generate-pdf.js';
import { CURRENCY_API_Key, CURRENCY_URL, REPORT_TEMPLATE } from '../common/constants.js';
import { sendReportEmail } from '../auth/send-email.js';
import fetch from 'node-fetch';

const ordersController = express.Router();

ordersController

  // for employee all orders
  .get('/', authMiddleware, loggedUserMiddleware, roleMiddleware(userRole.Employee), async (req, res) => {
    const { error, order } = await orderService.getAllOrders(ordersData)();

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'No orders found!' });
    } else {
      res.status(200).send(order);
    }
  })
  // get order by Id
  .get('/:id', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const id = +req.params.id;
    const { error, order } = await orderService.getOrderById(ordersData)(id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'No such order found!' });
    } else {
      res.status(200).send(order);
    }
  })
  
  // get all services by order Id
  .get('/:id/services', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const id = +req.params.id;
    const { error, services } = await orderService.getAllServicesByOrder(ordersData)(id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'No services found!' });
    } else {
      res.status(200).send(services);
    }
  })

  // for employee to delete order by order id
  .delete('/:id', authMiddleware, loggedUserMiddleware, roleMiddleware(userRole.Employee), async (req, res) => {
    const id = +req.params.id;
    const { error, order } = await orderService.deleteOrder(ordersData)(id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'Order is not found!' });
    } else {
      res.status(200).send(order);
    }
  })

  // for employee to update order by order id
  .put('/:id',
    authMiddleware,
    loggedUserMiddleware,
    validateBody('order', updateOrderValidator),
    async (req, res) => {
      const id = +req.params.id;
      const updateData = req.body;

      if (updateData.currency) {
        const query = "BGN_" + updateData.currency; // BGN_USD
        const response = await fetch(`${CURRENCY_URL}/api/v7/convert?q=${query}&compact=ultra&apiKey=${CURRENCY_API_Key}`)
          .then(res => res.json());

        updateData.currency_multiplier = response[query];
      }
      const { error, order } = await orderService.updateOrder(ordersData)(updateData, id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Order is not found!' });
      } else if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(409).send({ message: 'Service already added!' });
      } else {
        res.status(200).send(order);
      }
    })

  // for employee to create order for already created user and vehicle
  .post('/',
    authMiddleware,
    loggedUserMiddleware,
    roleMiddleware(userRole.Employee),
    validateBody('order', createOrderValidator),
    async (req, res) => {
      const users_id = +req.user.id;
      const { error, order } = await orderService.createOrder(ordersData, vehiclesData)(req.body, users_id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Vehicle is not found!' });
      } else if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(409).send({ message: 'An order for this vehicle already exists and is in progress!' });
      }
      else {
        res.status(200).send(order);
      }
    })

  // generate report
  .post('/:id/report', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const id = +req.params.id;
    const total = req.body;
    const { error, order } = await orderService.getOrderById(ordersData)(id);
    const orderServices = await orderService.getAllServicesByOrder(ordersData)(order.order_id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'No such order found!' });
    } else {
      const orderPdf = await generatePdf(REPORT_TEMPLATE, { ...order, ...orderServices, ...total });
      sendReportEmail(order.email, order.fullname, order.order_id, orderPdf);
      res.status(200).send(order);
    }
  });
export default ordersController;
