import express from 'express';
import { sendCredentialsEmail } from '../auth/send-email.js';
import { userRole } from '../common/roles.js';
import usersData from '../data/users-data.js';
import { authMiddleware } from '../middlewares/auth-middleware.js';
import loggedUserMiddleware from '../middlewares/logged-user-middleware.js';
import { roleMiddleware } from '../middlewares/role-middleware.js';
import validateBody from '../middlewares/validate-body.js';
import serviceErrors from '../services/service-errors.js';
import userService from '../services/user-service.js';
import createUserValidator from '../validators/create-user-validator.js';
import updateUserValidator from '../validators/update-user-validator.js';
import { v4 as uuidv4 } from 'uuid';
import resetPasswordValidator from '../validators/reset-password-validator.js';

const usersController = express.Router();

usersController

  // get all users
  .get(
    '/',
    authMiddleware,
    loggedUserMiddleware,
    roleMiddleware(userRole.Employee),
    async (req, res) => {
      const { search, sort, page, limit } = req.query;
      const result = await userService.getAllUsers(usersData)(
        search,
        sort,
        page,
        limit,
        req.user,
      );
      res.status(200).json(result);
    },
  )

  // get user by id
  .get('/:id', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const { id } = req.params;
    const { error, user } = await userService.getUserById(usersData)(+id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res
        .status(404)
        .json({ msg: `User with id ${req.params.id} was not found!` });
    } else {
      res.status(200).json(user);
    }
  })

  // create user
  .post(
    '/',
    authMiddleware,
    loggedUserMiddleware,
    roleMiddleware(userRole.Employee),
    validateBody('user', createUserValidator),
    async (req, res) => {
      const password = uuidv4().slice(0, 8);
      const { error, user } = await userService.createUser(usersData)(
        req.body,
        password,
      );

      if (error === serviceErrors.DUPLICATE_RECORD) {
        return res.status(409).json({ msg: 'This user already exists' });
      }

      sendCredentialsEmail(
        user.email,
        user.first_name,
        user.last_name,
        password,
      );
      res.status(201).json(user);
    },
  )

  // update user by id
  .put(
    '/:id',
    authMiddleware,
    loggedUserMiddleware,
    validateBody('user', updateUserValidator),
    async (req, res) => {
      const { id } = req.params;
      const updateData = req.body;
      const { error, user } = await userService.updateUser(usersData)(
        +id,
        updateData,
        req.user.id,
        req.user.role,
      );

      if (error === serviceErrors.OPERATION_NOT_ALLOWED) {
        return res.status(405).json({ msg: 'This operation is not allowed' });
      } else if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res
          .status(404)
          .json({ msg: `User with id ${req.params.id} was not found!` });
      } else if (error === serviceErrors.DUPLICATE_RECORD) {
        return res
          .status(409)
          .json({ msg: 'User with the same email already exists' });
      } else {
        res.status(200).json(user);
      }
    },
  )

  // update user's password
  .put(
    '/:id/change_password',
    authMiddleware,
    loggedUserMiddleware,
    validateBody('user', resetPasswordValidator),
    async (req, res) => {
      const { id } = req.params;
      const { password, oldPassword } = req.body;
      const { error, message } = await userService.updatePassword(
        +id,
        password,
        oldPassword,
      );

      if (error === serviceErrors.OPERATION_NOT_ALLOWED) {
        return res.status(405).json({ msg: 'Password not correct' });
      } else if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res
          .status(404)
          .json({ msg: `User with id ${req.params.id} was not found!` });
      } else {
        res.status(200).json(message);
      }
    },
  )

  // remove user
  .delete(
    '/:id',
    authMiddleware,
    loggedUserMiddleware,
    roleMiddleware(userRole.Employee),
    async (req, res) => {
      const { id } = req.params;
      const { error, user } = await userService.removeUser(usersData)(+id);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res
          .status(404)
          .json({ msg: `User with id ${req.params.id} was not found!` });
      } else {
        res.status(200).json(user);
      }
    },
  );

export default usersController;
