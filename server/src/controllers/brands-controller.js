import express from 'express';
import brandService from '../services/brand-service.js';

const brandsController = express.Router();

brandsController

  .get('/', async (req, res) => {
    const result = await brandService.getAll();

    res.status(200).json(result);
  });

export default brandsController;