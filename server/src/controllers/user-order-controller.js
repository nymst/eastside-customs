import express from 'express';
import { userRole } from '../common/roles.js';
import ordersData from '../data/orders-data.js';
import { authMiddleware } from '../middlewares/auth-middleware.js';
import loggedUserMiddleware from '../middlewares/logged-user-middleware.js';
import { roleMiddleware } from '../middlewares/role-middleware.js';
import orderService from '../services/order-service.js';
import serviceErrors from '../services/service-errors.js';


const userOrderController = express.Router();

userOrderController

  // for logged user to get his orders
  .get('/orders', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const id = +req.user.id;
    const { error, order } = await orderService.getByUser(ordersData)(id);
   
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'No orders found!' });
    } else {
      res.status(200).send(order);
    }
  })
  // for logged user to get his active order
  .get('/activeOrders', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const user_id = +req.user.id;
    const { error, order } = await orderService.getActiveOrdersByUser(ordersData)(user_id);
   
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'No orders found!' });
    } else {
      res.status(200).send(order);

    }
  })

// for employee to get orders for user by  id
  .get('/:id/order', authMiddleware, loggedUserMiddleware, roleMiddleware(userRole.Employee), async (req, res) => {
    const id = +req.params.id;
    const { error, order } = await orderService.getByUser(ordersData)(id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'No orders found!' });
    } else {
      res.status(200).send(order);
    }
  });



export default userOrderController;
