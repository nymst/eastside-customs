import express from 'express';
import vehiclesData from '../data/vehicles-data.js';
import validateBody from '../middlewares/validate-body.js';
import serviceErrors from '../services/service-errors.js';
import createVehicleValidator from '../validators/create-vehicle-validator.js';
import updateVehicleValidator from '../validators/update-vehicle-validator.js';
import vehicleService from '../services/vehicle-service.js';
import { authMiddleware } from '../middlewares/auth-middleware.js';
import loggedUserMiddleware from '../middlewares/logged-user-middleware.js';
import { roleMiddleware } from '../middlewares/role-middleware.js';
import { userRole } from '../common/roles.js';

const vehiclesController = express.Router();

vehiclesController

  // get all vehicles
  .get(
    '/',
    authMiddleware,
    loggedUserMiddleware,
    roleMiddleware(userRole.Employee),
    async (req, res) => {

      const queries = req.query;
     
      const { error, vehicles } = await vehicleService.getAllVehicles(
        vehiclesData,
      )(queries);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Vehicles not found!'});
      } else {
        res.status(200).send(vehicles);
      }
    },
  )
  
  // get vehicle by id
  .get('/:id', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const { id } = req.params;

    const { error, vehicle } = await vehicleService.getVehicleById(
      vehiclesData,
    )(id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'vehicle not found!'});
    } else {
      res.status(200).send(vehicle);
    }
  })

  //  update vehicle data
  .put(
    '/:id',
    authMiddleware,
    loggedUserMiddleware,
    roleMiddleware(userRole.Employee),
    validateBody('vehicle', updateVehicleValidator),
    async (req, res) => {
      const { id } = req.params;
      const data = req.body;
      const { error, vehicle } = await vehicleService.updateVehicle(
        vehiclesData,
      )(id, data);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'vehicle not found!' });
      } else {
        res.status(200).send(vehicle);
      }
    },
  )

  // create vehicle
  .post(
    '/',
    authMiddleware,
    loggedUserMiddleware,
    roleMiddleware(userRole.Employee),
    validateBody('vehicle', createVehicleValidator),
    async (req, res) => {
      const data = req.body;
      const { error, vehicle } = await vehicleService.createVehicle(
        vehiclesData,
      )(data);

      if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(404).send({
          message: 'Vehicle with such a vin already exist!',
        });
      } else {
        res.status(200).send(vehicle);
      }
    },
  )
// delete vehicle by id
  .delete(
    '/:id',
    authMiddleware,
    loggedUserMiddleware,
    roleMiddleware(userRole.Employee),
    async (req, res) => {
      const { id } = req.params;
      const { error, vehicle } = await vehicleService.deleteVehicle(
        vehiclesData,
      )(id);
  
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({
          message: 'vehicle not found or already is deleted!',
        });
      } else {
        res.status(200).send(vehicle);
      }
    },
  );

export default vehiclesController;
