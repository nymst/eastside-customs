import { v4 as uuidv4 } from 'uuid';
import serviceErrors from './service-errors.js';
import resetTokensData from '../data/reset-tokens-data.js';

const create = async (userId) => {
  const token = uuidv4();
  
  await resetTokensData.save(token, userId);

  return token;
};

const getToken = async (token) => {
  const foundToken = await resetTokensData.getToken(token);

  if (!foundToken) {

    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      message: 'Token not found',
    };
  }
 
  if (foundToken.expiry_time.valueOf() < new Date().valueOf()) {
    
    return {
      error: serviceErrors.OPERATION_NOT_ALLOWED,
      message: 'The link has expired',
    };
  }

  return {
    error: null,
    token: foundToken,
  };
};
export default {
  create,
  getToken,
};
