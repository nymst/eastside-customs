import serviceErrors from './service-errors.js';

const getAll = (servicesData) => {

  return async () => {
    return await servicesData.getAll();
  };
};

const getById = (servicesData) => {

  return async (id) => {
    const service = await servicesData.getBy('s.id', id);

    if (!service) {

      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        service: null,
      };
    }

    return {
      error: null,
      service: service,
    };
  };
};

const create = (servicesData) => {

  return async (name, price, description) => {
    const existingService = await servicesData.getBy('s.name', name);

    if (existingService) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        service: null,
      };
    }

    await servicesData.create(name, price, description);

    const createdService = await servicesData.getBy('s.name', name);

    return {
      error: null,
      service: createdService,
    };
  };
};

const remove = (servicesData) => {

  return async (id) => {
    const existingService = await servicesData.getBy('s.id', id);

    if (!existingService) {

      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        message: null,
      };
    }

    await servicesData.remove(id);

    return {
      error: null,
      message: 'Service deleted successfully',
    };
  };
};

const update = (servicesData) => {

  return async (id, name, price, description) => {
    const existingService = await servicesData.getBy('s.id', id);

    if (!existingService) {

      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        service: null,
      };
    }

    if (
      name === existingService.name &&
      price === existingService.price && 
      description === existingService.description
    ) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        service: null,
      };
    }

    await servicesData.update(id, name, price, description);

    const updatedService = await servicesData.getBy('s.id', id);

    return {
      error: null,
      service: updatedService,
    };
  };
};

export default {
  getAll,
  getById,
  create,
  remove,
  update,
};
