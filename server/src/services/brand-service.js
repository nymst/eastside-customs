import brandsData from '../data/brands-data.js';

const getAll = async() => {
  return await brandsData.getAll();
};

export default {
  getAll,
};
