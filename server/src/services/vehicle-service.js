import serviceErrors from './service-errors.js';

const getAllVehicles = (vehicleData) => {
  return async (queries) => {

    if (
      Object.keys(queries).includes('first_name') ||
      Object.keys(queries).includes('last_name') ||
      Object.keys(queries).includes('email')||
      Object.keys(queries).includes('phone_number')||
      Object.keys(queries).includes('m.name')
    ) {
      const filteredQueries = Object.entries(queries).filter((el) => {
        return (
          el[0] === 'first_name' || el[0] === 'last_name' || el[0] === 'email'
          || el[0] === 'phone_number'|| el[0] === 'm.name'
        );
      });

      const result = await vehicleData.searchBy(filteredQueries);

      return { error: null, vehicles: result };
    }
    const vehicles = await vehicleData.getAll(
      queries.sort,
      queries.page,
      queries.limit,
    );

    return { error: null, vehicles: vehicles };
  };
};

const getVehicleById = (vehicleData) => {
  return async (id, user) => {
    const vehicle = await vehicleData.getById(id, user);
    if (!vehicle) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicle: null,
      };
    }

    return { error: null, vehicle: vehicle };
  };
};

const updateVehicle = (vehicleData) => {
  return async (id, data) => {
    const vehicle = await vehicleData.getById(id);
    if (!vehicle || vehicle.is_deleted === 1) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicle: null,
      };
    }

    const model = await vehicleData.searchByModelAndBrand(
      data.model,
      data.brand,
    );

    if (!model[0]) {
      const newModel = await vehicleData.createModel(data);
      await vehicleData.update(id, data, newModel.insertId);
      const updated = await vehicleData.getById(id);
      return { error: null, vehicle: updated };
    }
    await vehicleData.update(id, data, model[0].id);
    const updated = await vehicleData.getById(id);

    return { error: null, vehicle: updated };
  };
};

const deleteVehicle = (vehicleData) => {
  return async (id) => {
    const vehicle = await vehicleData.getById(id);
   
    if (!vehicle) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicle: null,
      };
    }
    const deleted = await vehicleData.deleted(id);


    return { error: null, vehicle: deleted };
  };
};

const createVehicle = (vehicleData) => {
  return async (data, user) => {
    const vehicleVin = await vehicleData.getBy('v.vin', data.vin, user);

    if (vehicleVin[0]) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        vehicle: null,
      };
    }

    const model = await vehicleData.searchByModelAndBrand(
      data.model,
      data.brand,
    );

    if (!model[0]) {
      const newModel = await vehicleData.createModel(data, user);
      const newVehicle = await vehicleData.create(
        data,
        newModel.insertId,
        user,
      );
      const created = await vehicleData.getById(newVehicle.insertId, user);
      return { error: null, vehicle: created };
    }
    const newVehicle = await vehicleData.create(data, model[0].id, user);

    const created = await vehicleData.getById(newVehicle.insertId, user);
    return { error: null, vehicle: created };
  };
};




export default {
  getVehicleById,
  getAllVehicles,
  updateVehicle,
  deleteVehicle,
  createVehicle,
};
