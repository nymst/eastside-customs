/* eslint-disable no-unused-vars */
import serviceErrors from './service-errors.js';

const getAllOrders = (ordersData) => {
  return async () => {
    const order = await ordersData.getAll();

    if (!order[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }

    return { error: null, order: order };
  };
};

const getOrderById = (ordersData) => {
  return async (id) => {
    const order = await ordersData.getByOrder(id);

    if(!order) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }

    return { error: null, order: order};
  };
};

const getAllServicesByOrder = (ordersData) => {
  return async (order_id) => {
    const order = await ordersData.getByOrder(order_id);
    const services = await ordersData.getServicesByOrderId(order_id);
    if(!order || !services) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        services: null,
      };
    }

    delete services.meta;
    return { error: null, services: services};
  };

};

const getByUser = (ordersData) => {
  return async (id) => {
    const order = await ordersData.getBy('v.customer_id', id);

    if (!order[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }

    return { error: null, order: order };
  };
};

const getActiveOrdersByUser = (ordersData) => {
  return async (user_id) => {
    const orders = await ordersData.getActiveOrders(user_id);

    if (!orders) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }
    
    return { error: null, order: orders };
  };
};

const deleteOrder = (ordersData) => {
  return async (id) => {
    const order = await ordersData.getByOrder(id);

    if (!order) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }

    await ordersData.deleteOrder(id);

    const deletedOrder = await ordersData.getByOrder(id);
    return { error: null, order: deletedOrder };
  };
};

const updateOrder = (ordersData) => {
  return async (updateData, order_id) => {
    const order = await ordersData.getByOrder(order_id);
    
    if (!order) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }

    if (updateData.services_id && updateData.services_id.length > 0) {
      await ordersData.deleteOrderServices(order_id);
      await updateData.services_id.map(async (s_id) => {
        await ordersData.addService(order_id, s_id);
      });
      delete updateData.services_id;
    }

    if (updateData.status || updateData.due_date || updateData.notes || updateData.currency) {
      await ordersData.update(updateData, order_id);
    }

    const result = await ordersData.getByOrder(order_id);
    return { error: null, order: result };
  };
};

const createOrder = (ordersData, vehiclesData) => {
  return async (createOrderData, users_id) => {
    const { due_date, notes,services_id, vehicle_id } =
      createOrderData;

    const order = {
      due_date,
      notes,
      
    };

    const vehicle = await vehiclesData.getById(vehicle_id);

    if (!vehicle) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }

    const existingOrder = await ordersData.getOrderByVehicleId(vehicle_id);

    if (existingOrder) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        order: null,
      };
    }

    const createOrder = await ordersData.create({
      ...order,
      vehicle_id,
      users_id,
    });
    await services_id.map(async (s_id) => {
      await ordersData.addService(createOrder.id, s_id);
    });

    const created = await ordersData.getCreated(createOrder.id);

    return { error: null, order: created };
  };
};

export default {
  getAllOrders,
  getByUser,
  getOrderById,
  getAllServicesByOrder,
  getActiveOrdersByUser,
  deleteOrder,
  updateOrder,
  createOrder,
};
