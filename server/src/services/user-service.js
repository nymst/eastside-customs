/* eslint-disable no-unused-vars */
import serviceErrors from './service-errors.js';
import bcrypt from 'bcrypt';
import { userRole } from '../common/roles.js';
import usersData from '../data/users-data.js';

const signInUser = (usersData) => {
  return async (email, password) => {
    const user = await usersData.getBy('email', email);

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return {
        error: serviceErrors.INVALID_SIGNIN,
        user: null,
      };
    }

    return {
      error: null,
      user: user,
    };
  };
};

const getAllUsers = (usersData) => {
  return async (search, sort, page, limit, user) => {
    return search
      ? await usersData.searchBy('email', search)
      : await usersData.getAll(user, sort, page, limit, user);
  };
};

const getUserById = (usersData) => {
  return async (id) => {
    const user = await usersData.getBy('u.id', id);

    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }
    return { error: null, user: user };
  };
};

const createUser = (usersData) => {
  return async (createUserData, password) => {
    const {
      first_name,
      last_name,
      email,
      phone_number,
      role,
      postal_code,
      country,
      city,
      street_address,
    } = createUserData;

    const user = {
      first_name,
      last_name,
      email,
      password,
      phone_number,
      role,
    };

    const existingUser = await usersData.getBy('u.email', email);

    if (existingUser) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }

    const passwordHash = await bcrypt.hash(password, 10);
    user.password = passwordHash;

    const address = await usersData.createAddress(
      postal_code,
      country,
      city,
      street_address,
    );

    user.addresses_id = address.id;

    return {
      error: null,
      user: await usersData.create({ ...user, passwordHash }),
    };
  };
};

const updateUser = (usersData) => {
  return async (id, updateData, logguedUserId, role) => {
    const user = await usersData.getBy('u.id', id);

    if (user && user.id !== logguedUserId && role !== userRole.Employee) {
      return {
        error: serviceErrors.OPERATION_NOT_ALLOWED,
        user: null,
      };
    }

    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    if (
      user.email !== updateData.email &&
      !!(await usersData.getBy('email', updateData.email))
    ) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }

    const updatedUser = { ...user, ...updateData };

    const _ = await usersData.update(updateData, id);

    return {
      error: null,
      user: updatedUser,
    };
  };
};

const removeUser = (usersData) => {
  return async (id) => {
    const userToDelete = await usersData.getBy('u.id', id);
    if (!userToDelete) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    const _ = await usersData.remove(userToDelete);
    return { error: null, user: userToDelete };
  };
};

const getByEmail = async (email) => {
  const user = await usersData.getBy('u.email', email);

  if (!user) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      user: null,
    };
  }

  return {
    error: null,
    user: user,
  };
};

const resetPassword = async (id, password) => {
  const user = await usersData.getBy('u.id', id);
  const hashedPassword = await bcrypt.hash(password, 10);

  if (!user) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      message: null,
    };
  }

  await usersData.updateUserPassword(id, hashedPassword);

  return {
    error: null,
    message: 'Password updated successfully',
  };
};

const updatePassword = async (id, password, oldPassword) => {
  const user = await usersData.getBy('u.id', id);

  if (!user) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      message: null,
    };
  }
  if (!user || !(await bcrypt.compare(oldPassword, user.password))) {
    return {
      error: serviceErrors.OPERATION_NOT_ALLOWED,
      user: null,
    };
  }
  const hashedPassword = await bcrypt.hash(password, 10);
  await usersData.updateUserPassword(id, hashedPassword);

  return {
    error: null,
    message: 'Password updated successfully',
  };
};


export default {
  signInUser,
  getAllUsers,
  getUserById,
  createUser,
  updateUser,
  removeUser,
  getByEmail,
  resetPassword,
  updatePassword,
};
