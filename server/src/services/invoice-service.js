import serviceErrors from './service-errors.js';

const getAllInvoices = (invoicesData) => {
  return async (queries) => {
    if (
      Object.keys(queries).includes('u.first_name') ||
      Object.keys(queries).includes('u.last_name') ||
      Object.keys(queries).includes('id')
    ) {
      const filteredQueries = Object.entries(queries).filter((el) => {
        return (
          el[0] === 'u.first_name' ||
          el[0] === 'u.last_name' ||
          el[0] === 'id'
        );
      });

      const result = await invoicesData.searchBy(filteredQueries);

      return { invoices: result };
    }
    const invoices = await invoicesData.getAll(
      queries.sort,
      queries.page,
      queries.limit,
    );

    return { invoices: invoices };
  };
};

const getInvoiceById = (invoicesData) => {
  return async (id) => {
    const invoice = await invoicesData.getBy('i.id', id);
   
    if (!invoice) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }
    return { error: null, invoice: invoice };
  };
};

const createInvoice = (invoicesData, ordersData) => {
  return async (orders_id, total) => {

    const existingInvoice = await invoicesData.getBy('i.orders_id', orders_id);
    const existingOrder = await ordersData.getById(orders_id);

    if (existingInvoice) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        invoice: null,
      };
    }
    
    if (!existingOrder || (existingOrder && existingOrder.order_is_deleted===1)) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        invoice: null,
      };
    }
    const invoice = await invoicesData.create(
      orders_id,
      total,
    );

    return { error: null, invoice: invoice };
  };

};

const getAllInvoicesByCustomerID = (invoicesData) => {
  return async (customer_id) => { 
    return await invoicesData.getAllInvoicesByUser(customer_id);
  
  };
};

export default {
  getAllInvoices,
  getInvoiceById,
  createInvoice,
  getAllInvoicesByCustomerID,
};
