const getAllModelsByBrand = (modelsData) => {
  return async (brand_id) => {
    return await modelsData.getModels(brand_id);
  };
};

export default {
  getAllModelsByBrand,
};
