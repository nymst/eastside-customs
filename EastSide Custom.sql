-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for Win64 (AMD64)
--
-- Host: triton.rdb.superhosting.bg    Database: fwraptea_eastside_customs
-- ------------------------------------------------------
-- Server version	10.3.29-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postal_code` varchar(45) NOT NULL,
  `country` varchar(255) NOT NULL DEFAULT 'Bulgaria',
  `city` varchar(255) NOT NULL,
  `street_address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` VALUES (1,'9000','Bulgaria','Varna','Akchelar 3'),(2,'1000','Bulgaria','Sofia','Kneja 14'),(3,'3000','Bulgaria','Vratsa','Nikola Voinov 6'),(4,'8000','Bulgaria','Burgas','Svoboda 1'),(5,'44111','Bulgaria','Pazardzhik','Slunce 89'),(6,'7000','Bulgaria','Ruse','Tsarski pat 4');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `logo` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'Seat','https://listcarbrands.com/wp-content/uploads/2016/12/seat-logo.png'),(2,'Renault','https://logo-logos.com/wp-content/uploads/2016/11/Renault_logo.png'),(3,'Peugeot','https://cache1.24chasa.bg/Images/Cache/669/Image_9015669_128.jpg'),(4,'Dacia','https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Dacia_Logo_new.jpg/553px-Dacia_Logo_new.jpg'),(5,'Citroën','https://www.nastarta.com/wp-content/uploads/2018/04/30020-citroen-logo3.jpg'),(6,'Opel','https://purepng.com/public/uploads/large/purepng.com-opel-logoopelopel-automobilemanufacturerfrench-automobileopel-logo-1701527529528p9geq.png'),(7,'Alfa Romeo','https://lezebre.lu/images/detailed/17/30527-Alfa-Romeo.png'),(8,'Škoda','https://i.pinimg.com/originals/0a/8e/38/0a8e3852954dd0a8b0d2602e9e5de08b.jpg'),(9,'Chevrolet','https://www.gmignitionupdate.com/dld/content/dam/Media/images/INTL/chevrolet/logos/chevrolet-logo/Chevrolet-Logo-286513.jpg'),(10,'Porsche','https://www.nastarta.com/wp-content/uploads/2018/04/Porsche-Logo-download.jpg'),(11,'Honda','https://i.pinimg.com/originals/90/88/a9/9088a9cdcf4bf9daaa774f4643ba4a4a.jpg'),(12,'Subaru','https://autobild.bg/wp-content/uploads/2016/05/Subaru_logo_and_wordmark.svg_.png'),(13,'Mazda','https://phoenix-archery.com/wp-content/uploads/2017/01/Mazda-logo-6.jpg'),(14,'Mitsubishi','https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Mitsubishi_motors_new_logo.svg/561px-Mitsubishi_motors_new_logo.svg.png'),(15,'Lexus','https://www.kalocom.com/storage/LOGA/Lexus_logo.jpg'),(16,'Toyota','https://www.futurelab.net/sites/default/files/toyota-logo.jpg'),(17,'BMW','https://logosvector.net/wp-content/uploads/2014/07/BMW-logo-vector.png'),(18,'Volkswagen','https://park.shifting-gears.com/wp-content/uploads/2015/11/VW_Logo.png'),(19,'Suzuki','https://cdn-0.motorcycle-logos.com/wp-content/uploads/2016/10/Suzuki-logo.png'),(20,'Mercedes-Benz','https://cdn.worldvectorlogo.com/logos/mercedes-benz-6.svg'),(21,'Saab','http://lofrev.net/wp-content/photos/2014/07/Saab-Logo.jpg'),(22,'Audi','https://i.pinimg.com/originals/75/36/85/75368501cb5135f0605443b0b752c719.jpg'),(23,'Kia','https://logowiki.net/uploads/logo/k/kia-motors.svg'),(24,'Land Rover','https://logodownload.org/wp-content/uploads/2019/08/land-rover-logo.png'),(25,'Dodge','https://logos-world.net/wp-content/uploads/2021/03/Dodge-Logo.png'),(26,'Chrysler','https://avtotachki.com/wp-content/uploads/2020/11/chrysler.jpg'),(27,'Ford','https://www.whiteoakfordlincoln.ca/Whiteoak%20Ford%20Lincoln_files/ford-logo.png'),(28,'Hummer','https://cdn.carbuzz.com/gallery-images/840x560/697000/100/697178.jpg'),(29,'Hyundai','https://i.pinimg.com/originals/aa/08/65/aa08656f292342a47afebd5b0a896706.jpg'),(30,'Infiniti','https://cdn.worldvectorlogo.com/logos/infiniti-1.svg'),(31,'Jaguar','https://i.pinimg.com/originals/24/1b/43/241b43a86cd70a6789612291510ab3dc.jpg'),(32,'Jeep','https://www.artandstick.be/getsupercustomizedimage.php5?objid=4944&colorid1=43&colorid2=4&colorid3=4&colorid4=4&colorid5=4&way=NORMAL&transparent=Y'),(33,'Nissan','https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Nissan-logo.svg/697px-Nissan-logo.svg.png'),(34,'Volvo','https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Volvo_Trucks_%26_Bus_logo.jpg/600px-Volvo_Trucks_%26_Bus_logo.jpg'),(35,'Daewoo','https://2.bp.blogspot.com/-46IE4VYDfQw/W83ZD6lkqmI/AAAAAAAACKA/njeT1xbH4M8_se2-vBuwexDpDElF0G8TQCLcBGAs/s1600/Daewoo-auto-logo-1.png'),(36,'Fiat','https://i.pinimg.com/originals/60/67/d0/6067d0d6c2e5577f4a718b8fbe019d40.jpg'),(37,'MINI','https://lezebre.lu/images/detailed/83/48318-logo-mini-2018.jpg'),(38,'Rover','https://cdn.worldvectorlogo.com/logos/rover-1.svg'),(39,'Smart','https://www.pangearent.com/wp-content/uploads/2017/06/Smart-logo-1994.jpg'),(40,'Acura','https://kalocom.com/storage/LOGA/Acura_logo.png');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_date` date NOT NULL DEFAULT current_timestamp(),
  `due_date` date NOT NULL DEFAULT (current_timestamp() + interval 15 day),
  `orders_id` int(11) NOT NULL,
  `total` decimal(6,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orders_id_UNIQUE` (`orders_id`),
  KEY `fk_invoices_orders1_idx` (`orders_id`),
  CONSTRAINT `fk_invoices_orders1` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21006 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
INSERT INTO `invoices` VALUES (21000,'2021-06-10','2021-06-25',1,32.24),(21001,'2021-06-10','2021-06-25',4,627.00),(21002,'2021-06-10','2021-06-25',6,46.41),(21003,'2021-06-10','2021-06-25',7,13.26),(21004,'2021-06-10','2021-06-25',5,49.50),(21005,'2021-06-10','2021-06-25',8,49.50);
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `models`
--

DROP TABLE IF EXISTS `models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `brand_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_models_manufacturers_idx` (`brand_id`) USING BTREE,
  CONSTRAINT `FK_models_brands` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=899 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `models`
--

LOCK TABLES `models` WRITE;
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` VALUES (1,'Lodgy',1),(2,'Altea',1),(3,'106',1),(4,'Arosa',1),(5,'Cordoba',1),(6,'Cordoba Vario',1),(7,'Exeo',1),(8,'Ibiza',1),(9,'Ibiza ST',1),(10,'Exeo ST',1),(11,'Leon',1),(12,'Leon ST',1),(13,'Inca',1),(14,'Mii',1),(15,'Toledo',1),(16,'Alhambra',2),(17,'Clio',2),(18,'Clio Grandtour',2),(19,'Altea XL',2),(20,'Express',2),(21,'Fluence',2),(22,'Grand Espace',2),(23,'Grand Modus',2),(24,'Grand Scenic',2),(25,'Kadjar',2),(26,'Kangoo',2),(27,'Kangoo Express',2),(28,'Koleos',2),(29,'Laguna',2),(30,'Laguna Grandtour',2),(31,'Latitude',2),(32,'Mascott',2),(33,'Mégane',2),(34,'Mégane CC',2),(35,'Mégane Combi',2),(36,'Mégane Grandtour',2),(37,'Mégane Coupé',2),(38,'Mégane Scénic',2),(39,'Scénic',2),(40,'Talisman',2),(41,'Talisman Grandtour',2),(42,'Thalia',2),(43,'Twingo',2),(44,'Wind',2),(45,'Zoé',2),(46,'1007',3),(47,'107',3),(48,'106',3),(49,'108',3),(50,'2008',3),(51,'205',3),(52,'205 Cabrio',3),(53,'206',3),(54,'206 CC',3),(55,'206 SW',3),(56,'207',3),(57,'207 CC',3),(58,'207 SW',3),(59,'306',3),(60,'307',3),(61,'307 CC',3),(62,'307 SW',3),(63,'308',3),(64,'308 CC',3),(65,'308 SW',3),(66,'309',3),(67,'4007',3),(68,'4008',3),(69,'405',3),(70,'406',3),(71,'407',3),(72,'407 SW',3),(73,'5008',3),(74,'508',3),(75,'508 SW',3),(76,'605',3),(77,'806',3),(78,'607',3),(79,'807',3),(80,'Bipper',3),(81,'RCZ',3),(82,'Dokker',4),(83,'Duster',4),(84,'Lodgy',4),(85,'Logan',4),(86,'Logan MCV',4),(87,'Logan Van',4),(88,'Sandero',4),(89,'Solenza',4),(90,'Berlingo',5),(91,'C-Crosser',5),(92,'C-Elissée',5),(93,'C-Zero',5),(94,'C1',5),(95,'C2',5),(96,'C3',5),(97,'C3 Picasso',5),(98,'C4',5),(99,'C4 Aircross',5),(100,'C4 Cactus',5),(101,'C4 Coupé',5),(102,'C4 Grand Picasso',5),(103,'C4 Sedan',5),(104,'C5',5),(105,'C5 Break',5),(106,'C5 Tourer',5),(107,'C6',5),(108,'C8',5),(109,'DS3',5),(110,'DS4',5),(111,'DS5',5),(112,'Evasion',5),(113,'Jumper',5),(114,'Jumpy',5),(115,'Saxo',5),(116,'Nemo',5),(117,'Xantia',5),(118,'Xsara',5),(119,'Agila',6),(120,'Ampera',6),(121,'Antara',6),(122,'Clio Grandtour',6),(123,'Astra cabrio',6),(124,'Astra caravan',6),(125,'Astra coupé',6),(126,'Calibra',6),(127,'Campo',6),(128,'Cascada',6),(129,'Corsa',6),(130,'Frontera',6),(131,'Insignia',6),(132,'Insignia kombi',6),(133,'Kadett',6),(134,'Meriva',6),(135,'Mokka',6),(136,'Movano',6),(137,'Omega',6),(138,'Signum',6),(139,'Vectra',6),(140,'Vectra Caravan',6),(141,'Vivaro',6),(142,'Vivaro Kombi',6),(143,'Zafira',6),(144,'145',7),(145,'146',7),(146,'147',7),(147,'155',7),(148,'156',7),(149,'156 Sportwagon',7),(150,'159',7),(151,'159 Sportwagon',7),(152,'164',7),(153,'166',7),(154,'4C',7),(155,'Brera',7),(156,'GTV',7),(157,'MiTo',7),(158,'Crosswagon',7),(159,'Spider',7),(160,'GT',7),(161,'Giulietta',7),(162,'Giulia',7),(163,'Favorit',8),(164,'Felicia',8),(165,'Citigo',8),(166,'Fabia',8),(167,'Fabia Combi',8),(168,'Fabia Sedan',8),(169,'Felicia Combi',8),(170,'Octavia',8),(171,'Octavia Combi',8),(172,'Roomster',8),(173,'Yeti',8),(174,'Rapid',8),(175,'Rapid Spaceback',8),(176,'Superb',8),(177,'Superb Combi',8),(178,'Alero',9),(179,'Aveo',9),(180,'Camaro',9),(181,'Captiva',9),(182,'Corvette',9),(183,'Cruze',9),(184,'Cruze SW',9),(185,'Epica',9),(186,'Equinox',9),(187,'Evanda',9),(188,'HHR',9),(189,'Kalos',9),(190,'Lacetti',9),(191,'Lacetti SW',9),(192,'Lumina',9),(193,'Malibu',9),(194,'Matiz',9),(195,'Monte Carlo',9),(196,'Nubira',9),(197,'Orlando',9),(198,'Spark',9),(199,'Suburban',9),(200,'Tacuma',9),(201,'Tahoe',9),(202,'Trax',9),(203,'911 Carrera',10),(204,'911 Carrera Cabrio',10),(205,'911 Targa',10),(206,'911 Turbo',10),(207,'924',10),(208,'944',10),(209,'997',10),(210,'Boxster',10),(211,'Cayenne',10),(212,'Cayman',10),(213,'Macan',10),(214,'Panamera',10),(215,'Accord',11),(216,'Accord Coupé',11),(217,'Accord Tourer',11),(218,'City',11),(219,'Civic',11),(220,'Civic Aerodeck',11),(221,'Civic Coupé',11),(222,'Civic Tourer',11),(223,'Civic Type R',11),(224,'CR-V',11),(225,'CR-X',11),(226,'CR-Z',11),(227,'FR-V',11),(228,'HR-V',11),(229,'Insight',11),(230,'Integra',11),(231,'Jazz',11),(232,'Legend',11),(233,'Prelude',11),(234,'BRZ',12),(235,'Forester',12),(236,'Impreza',12),(237,'Impreza Wagon',12),(238,'Justy',12),(239,'Legacy',12),(240,'Legacy Wagon',12),(241,'Legacy Outback',12),(242,'Levorg',12),(243,'Outback',12),(244,'SVX',12),(245,'Tribeca',12),(246,'Tribeca B9',12),(247,'XV',12),(248,'121',13),(249,'2',13),(250,'3',13),(251,'323',13),(252,'323 Combi',13),(253,'323 Coupé',13),(254,'323 F',13),(255,'5',13),(256,'6',13),(257,'6 Combi',13),(258,'626',13),(259,'626 Combi',13),(260,'B-Fighter',13),(261,'B2500',13),(262,'BT',13),(263,'CX-3',13),(264,'CX-5',13),(265,'CX-7',13),(266,'CX-9',13),(267,'Demio',13),(268,'MPV',13),(269,'MX-3',13),(270,'MX-5',13),(271,'MX-6',13),(272,'Premacy',13),(273,'RX-7',13),(274,'RX-8',13),(275,'Xedox 6',13),(276,'3000 GT',14),(277,'ASX',14),(278,'Carisma',14),(279,'Colt',14),(280,'Colt CC',14),(281,'Eclipse',14),(282,'Fuso canter',14),(283,'Galant',14),(284,'Galant Combi',14),(285,'Grandis',14),(286,'L200',14),(287,'L200 Pick up',14),(288,'L200 Pick up Allrad',14),(289,'L300',14),(290,'Lancer',14),(291,'Lancer Combi',14),(292,'Lancer Evo',14),(293,'Lancer Sportback',14),(294,'Outlander',14),(295,'Pajero',14),(296,'Pajeto Pinin',14),(297,'Pajero Pinin Wagon',14),(298,'Pajero Sport',14),(299,'Pajero Wagon',14),(300,'Space Star',14),(301,'CT',15),(302,'GS',15),(303,'GS 300',15),(304,'GX',15),(305,'IS',15),(306,'IS 200',15),(307,'IS 250 C',15),(308,'IS-F',15),(309,'LS',15),(310,'LX',15),(311,'NX',15),(312,'RC F',15),(313,'RX',15),(314,'RX 300',15),(315,'RX 400h',15),(316,'RX 450h',15),(317,'SC 430',15),(318,'4-Runner',16),(319,'Auris',16),(320,'Avensis',16),(321,'Avensis Combi',16),(322,'Avensis Van Verso',16),(323,'Aygo',16),(324,'Camry',16),(325,'Carina',16),(326,'Celica',16),(327,'Corolla',16),(328,'Corolla Combi',16),(329,'Corolla sedan',16),(330,'Corolla Verso',16),(331,'FJ Cruiser',16),(332,'GT86',16),(333,'Hiace',16),(334,'Hiace Van',16),(335,'Highlander',16),(336,'Hilux',16),(337,'Land Cruiser',16),(338,'MR2',16),(339,'Paseo',16),(340,'Picnic',16),(341,'Prius',16),(342,'RAV4',16),(343,'Sequoia',16),(344,'Starlet',16),(345,'Supra',16),(346,'Tundra',16),(347,'Urban Cruiser',16),(348,'Verso',16),(349,'Yaris',16),(350,'Yaris Verso',16),(351,'i3',17),(352,'i8',17),(353,'M3',17),(354,'M4',17),(355,'M5',17),(356,'M6',17),(357,'Rad 1',17),(358,'Rad 1 Cabrio',17),(359,'Rad 1 Coupé',17),(360,'Rad 2',17),(361,'Rad 2 Active Tourer',17),(362,'Rad 2 Coupé',17),(363,'Rad 2 Gran Tourer',17),(364,'Rad 3',17),(365,'Rad 3 Cabrio',17),(366,'Rad 3 Compact',17),(367,'Rad 3 Coupé',17),(368,'Rad 3 GT',17),(369,'Rad 3 Touring',17),(370,'Rad 4',17),(371,'Rad 4 Cabrio',17),(372,'Rad 4 Gran Coupé',17),(373,'Rad 5',17),(374,'Rad 5 GT',17),(375,'Rad 5 Touring',17),(376,'Rad 6',17),(377,'Rad 6 Cabrio',17),(378,'Rad 6 Coupé',17),(379,'Rad 6 Gran Coupé',17),(380,'Rad 7',17),(381,'Rad 8 Coupé',17),(382,'X1',17),(383,'X3',17),(384,'X4',17),(385,'X5',17),(386,'X6',17),(387,'Z3',17),(388,'Z3 Coupé',17),(389,'Z3 Roadster',17),(390,'Z4',17),(391,'Z4 Roadster',17),(392,'Amarok',18),(393,'Beetle',18),(394,'Bora',18),(395,'Bora Variant',18),(396,'Caddy',18),(397,'Caddy Van',18),(398,'Life',18),(399,'California',18),(400,'Caravelle',18),(401,'CC',18),(402,'Crafter',18),(403,'Crafter Van',18),(404,'Crafter Kombi',18),(405,'CrossTouran',18),(406,'Eos',18),(407,'Fox',18),(408,'3000 GT',18),(409,'Golf Cabrio',18),(410,'Golf Plus',18),(411,'Golf Sportvan',18),(412,'Golf Variant',18),(413,'Jetta',18),(414,'LT',18),(415,'Lupo',18),(416,'Multivan',18),(417,'New Beetle',18),(418,'New Beetle Cabrio',18),(419,'Passat',18),(420,'Passat Alltrack',18),(421,'Passat CC',18),(422,'Passat Variant',18),(423,'Passat Variant Van',18),(424,'Phaeton',18),(425,'Polo',18),(426,'Polo Van',18),(427,'Polo Variant',18),(428,'Scirocco',18),(429,'Sharan',18),(430,'T4',18),(431,'T4 Caravelle',18),(432,'T4 Multivan',18),(433,'T5',18),(434,'T5 Caravelle',18),(435,'T5 Multivan',18),(436,'T5 Transporter Shuttle',18),(437,'Tiguan',18),(438,'Touareg',18),(439,'Touran',18),(440,'Alto',19),(441,'Baleno',19),(442,'Baleno kombi',19),(443,'Grand Vitara',19),(444,'Grand Vitara XL-7',19),(445,'Ignis',19),(446,'Jimny',19),(447,'Kizashi',19),(448,'Liana',19),(449,'Samurai',19),(450,'Splash',19),(451,'Swift',19),(452,'SX4',19),(453,'SX4 Sedan',19),(454,'Vitara',19),(455,'Wagon R+',19),(456,'100 D',20),(457,'115',20),(458,'124',20),(459,'126',20),(460,'190',20),(461,'190 D',20),(462,'190 E',20),(463,'200 - 300',20),(464,'200 D',20),(465,'200 E',20),(466,'210 Van',20),(467,'210 kombi',20),(468,'310 Van',20),(469,'310 kombi',20),(470,'230 - 300 CE Coupé',20),(471,'260 - 560 SE',20),(472,'260 - 560 SEL',20),(473,'500 - 600 SEC Coupé',20),(474,'Trieda A',20),(475,'A',20),(476,'A L',20),(477,'AMG GT',20),(478,'Trieda B',20),(479,'Trieda C',20),(480,'C',20),(481,'C Sportcoupé',20),(482,'C T',20),(483,'Citan',20),(484,'CL',20),(485,'CL',20),(486,'CLA',20),(487,'CLC',20),(488,'CLK Cabrio',20),(489,'CLK Coupé',20),(490,'CLS',20),(491,'Trieda E',20),(492,'E',20),(493,'E Cabrio',20),(494,'E Coupé',20),(495,'E T',20),(496,'Trieda G',20),(497,'G Cabrio',20),(498,'GL',20),(499,'GLA',20),(500,'GLC',20),(501,'GLE',20),(502,'GLK',20),(503,'Trieda M',20),(504,'MB 100',20),(505,'Trieda R',20),(506,'Trieda S',20),(507,'S',20),(508,'S Coupé',20),(509,'SL',20),(510,'SLC',20),(511,'SLK',20),(512,'SLR',20),(513,'Sprinter',20),(514,'9-3',21),(515,'9-3 Cabriolet',21),(516,'9-3 Coupé',21),(517,'9-3 SportCombi',21),(518,'9-5',21),(519,'9-5 SportCombi',21),(520,'900',21),(521,'900 C',21),(522,'900 C Turbo',21),(523,'9000',21),(524,'100',22),(525,'100 Avant',22),(526,'80',22),(527,'80 Avant',22),(528,'80 Cabrio',22),(529,'90',22),(530,'A1',22),(531,'A2',22),(532,'A3',22),(533,'A3 Cabriolet',22),(534,'A3 Limuzina',22),(535,'A3 Sportback',22),(536,'A4',22),(537,'A4 Allroad',22),(538,'A4 Avant',22),(539,'A4 Cabriolet',22),(540,'A5',22),(541,'A5 Cabriolet',22),(542,'A5 Sportback',22),(543,'A6',22),(544,'A6 Allroad',22),(545,'A6 Avant',22),(546,'A7',22),(547,'A8',22),(548,'A8 Long',22),(549,'Q3',22),(550,'Q5',22),(551,'Q7',22),(552,'R8',22),(553,'RS4 Cabriolet',22),(554,'RS4/RS4 Avant',22),(555,'RS5',22),(556,'RS6 Avant',22),(557,'RS7',22),(558,'S3/S3 Sportback',22),(559,'S4 Cabriolet',22),(560,'S4/S4 Avant',22),(561,'S5/S5 Cabriolet',22),(562,'S6/RS6',22),(563,'S7',22),(564,'S8',22),(565,'SQ5',22),(566,'TT Coupé',22),(567,'TT Roadster',22),(568,'TTS',22),(569,'Avella',23),(570,'Besta',23),(571,'Carens',23),(572,'Carnival',23),(573,'Cee`d',23),(574,'Cee`d SW',23),(575,'Cerato',23),(576,'K 2500',23),(577,'Magentis',23),(578,'Opirus',23),(579,'Optima',23),(580,'Picanto',23),(581,'Pregio',23),(582,'Pride',23),(583,'Pro Cee`d',23),(584,'Rio',23),(585,'Rio Combi',23),(586,'Rio sedan',23),(587,'Sephia',23),(588,'Shuma',23),(589,'Sorento',23),(590,'Soul',23),(591,'Sportage',23),(592,'Venga',23),(593,'109',24),(594,'Defender',24),(595,'Discovery',24),(596,'Discovery Sport',24),(597,'Freelander',24),(598,'Range Rover',24),(599,'Range Rover Evoque',24),(600,'Range Rover Sport',24),(601,'Avenger',25),(602,'Caliber',25),(603,'Challenger',25),(604,'Charger',25),(605,'Grand Caravan',25),(606,'Journey',25),(607,'Magnum',25),(608,'Nitro',25),(609,'RAM',25),(610,'Stealth',25),(611,'Viper',25),(612,'300 C',26),(613,'300 C Touring',26),(614,'300 M',26),(615,'Crossfire',26),(616,'Grand Voyager',26),(617,'LHS',26),(618,'Neon',26),(619,'Pacifica',26),(620,'Plymouth',26),(621,'PT Cruiser',26),(622,'Sebring',26),(623,'Sebring Convertible',26),(624,'Stratus',26),(625,'Stratus Cabrio',26),(626,'Town & Country',26),(627,'Voyager',26),(628,'Aerostar',27),(629,'B-Max',27),(630,'C-Max',27),(631,'Cortina',27),(632,'Cougar',27),(633,'Edge',27),(634,'Escort',27),(635,'Escort Cabrio',27),(636,'Escort kombi',27),(637,'Explorer',27),(638,'F-150',27),(639,'F-250',27),(640,'Fiesta',27),(641,'Focus',27),(642,'Focus C-Max',27),(643,'Focus CC',27),(644,'Focus kombi',27),(645,'Fusion',27),(646,'Galaxy',27),(647,'Grand C-Max',27),(648,'Ka',27),(649,'Kuga',27),(650,'Maverick',27),(651,'Mondeo',27),(652,'Mondeo Combi',27),(653,'Mustang',27),(654,'Orion',27),(655,'Puma',27),(656,'Ranger',27),(657,'S-Max',27),(658,'Sierra',27),(659,'Street Ka',27),(660,'Tourneo Connect',27),(661,'Tourneo Custom',27),(662,'Transit',27),(663,'Transit',27),(664,'Transit Bus',27),(665,'Transit Connect LWB',27),(666,'Transit Courier',27),(667,'Transit Custom',27),(668,'Transit kombi',27),(669,'Transit Tourneo',27),(670,'Transit Valnik',27),(671,'Transit Van',27),(672,'Transit Van 350',27),(673,'Windstar',27),(674,'H2',28),(675,'H3',28),(676,'Accent',29),(677,'Atos',29),(678,'Atos Prime',29),(679,'Coupé',29),(680,'Elantra',29),(681,'Galloper',29),(682,'Genesis',29),(683,'Getz',29),(684,'Grandeur',29),(685,'H 350',29),(686,'H1',29),(687,'H1 Bus',29),(688,'H1 Van',29),(689,'H200',29),(690,'i10',29),(691,'i20',29),(692,'i30',29),(693,'i30 CW',29),(694,'i40',29),(695,'i40 CW',29),(696,'ix20',29),(697,'ix35',29),(698,'ix55',29),(699,'Lantra',29),(700,'Matrix',29),(701,'Santa Fe',29),(702,'Sonata',29),(703,'Terracan',29),(704,'Trajet',29),(705,'Tucson',29),(706,'Veloster',29),(707,'EX',30),(708,'FX',30),(709,'G',30),(710,'G Coupé',30),(711,'M',30),(712,'Q',30),(713,'QX',30),(714,'Daimler',31),(715,'F-Pace',31),(716,'F-Type',31),(717,'S-Type',31),(718,'Sovereign',31),(719,'X-Type',31),(720,'X-type Estate',31),(721,'XE',31),(722,'XF',31),(723,'XJ',31),(724,'XJ12',31),(725,'XJ6',31),(726,'XJ8',31),(727,'XJ8',31),(728,'XJR',31),(729,'XK',31),(730,'XK8 Convertible',31),(731,'XKR',31),(732,'XKR Convertible',31),(733,'Cherokee',32),(734,'Commander',32),(735,'Compass',32),(736,'Grand Cherokee',32),(737,'Patriot',32),(738,'Renegade',32),(739,'Wrangler',32),(740,'100 NX',33),(741,'200 SX',33),(742,'350 Z',33),(743,'350 Z Roadster',33),(744,'370 Z',33),(745,'Almera',33),(746,'Almera Tino',33),(747,'Cabstar E - T',33),(748,'Cabstar TL2 Valnik',33),(749,'e-NV200',33),(750,'GT-R',33),(751,'Insterstar',33),(752,'Juke',33),(753,'King Cab',33),(754,'Leaf',33),(755,'Maxima',33),(756,'Maxima QX',33),(757,'Micra',33),(758,'Murano',33),(759,'Navara',33),(760,'Note',33),(761,'NP300 Pickup',33),(762,'NV200',33),(763,'NV400',33),(764,'Pathfinder',33),(765,'Patrol',33),(766,'Patrol GR',33),(767,'Pickup',33),(768,'Pixo',33),(769,'Primastar',33),(770,'Primastar Combi',33),(771,'Primera',33),(772,'Primera Combi',33),(773,'Pulsar',33),(774,'Qashqai',33),(775,'Serena',33),(776,'Sunny',33),(777,'Terrano',33),(778,'Tiida',33),(779,'Trade',33),(780,'Vanette Cargo',33),(781,'X-Trail',33),(782,'240',34),(783,'340',34),(784,'360',34),(785,'460',34),(786,'850',34),(787,'850 kombi',34),(788,'C30',34),(789,'C70',34),(790,'C70 Cabrio',34),(791,'C70 Coupé',34),(792,'S40',34),(793,'S60',34),(794,'S70',34),(795,'S80',34),(796,'S90',34),(797,'V40',34),(798,'V50',34),(799,'V60',34),(800,'V70',34),(801,'V90',34),(802,'XC60',34),(803,'XC70',34),(804,'XC90',34),(805,'Espero',35),(806,'Kalos',35),(807,'Lacetti',35),(808,'Lanos',35),(809,'Leganza',35),(810,'Lublin',35),(811,'Matiz',35),(812,'Nexia',35),(813,'Nubira',35),(814,'Nubira kombi',35),(815,'Racer',35),(816,'Tacuma',35),(817,'Tico',35),(818,'1100',36),(819,'126',36),(820,'500',36),(821,'500L',36),(822,'500X',36),(823,'850',36),(824,'Barchetta',36),(825,'Brava',36),(826,'Cinquecento',36),(827,'Coupé',36),(828,'Croma',36),(829,'Doblo',36),(830,'Doblo Cargo',36),(831,'Doblo Cargo Combi',36),(832,'Ducato',36),(833,'Ducato Van',36),(834,'Ducato Kombi',36),(835,'Ducato Podvozok',36),(836,'Florino',36),(837,'Florino Combi',36),(838,'Freemont',36),(839,'Grande Punto',36),(840,'Idea',36),(841,'Linea',36),(842,'Marea',36),(843,'Marea Weekend',36),(844,'Multipla',36),(845,'Palio Weekend',36),(846,'Panda',36),(847,'Panda Van',36),(848,'Punto',36),(849,'Punto Cabriolet',36),(850,'Punto Evo',36),(851,'Punto Van',36),(852,'Qubo',36),(853,'Scudo',36),(854,'Scudo Van',36),(855,'Scudo Kombi',36),(856,'Sedici',36),(857,'Seicento',36),(858,'Stilo',36),(859,'Stilo Multiwagon',36),(860,'Strada',36),(861,'Talento',36),(862,'Tipo',36),(863,'Ulysse',36),(864,'Uno',36),(865,'X1/9',36),(866,'Cooper',37),(867,'Cooper Cabrio',37),(868,'Cooper Clubman',37),(869,'Cooper D',37),(870,'Cooper D Clubman',37),(871,'Cooper S',37),(872,'Cooper S Cabrio',37),(873,'Cooper S Clubman',37),(874,'Countryman',37),(875,'Mini One',37),(876,'One D',37),(877,'200',38),(878,'214',38),(879,'218',38),(880,'25',38),(881,'400',38),(882,'414',38),(883,'416',38),(884,'620',38),(885,'75',38),(886,'Cabrio',39),(887,'City-Coupé',39),(888,'Compact Pulse',39),(889,'Forfour',39),(890,'Fortwo cabrio',39),(891,'Fortwo coupé',39),(892,'Roadster',39),(893,'Clio',2),(894,'Cleo',2),(896,'Cleos',2),(897,'Cleos',2),(898,'Megan',2);
/*!40000 ALTER TABLE `models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_date` date NOT NULL DEFAULT current_timestamp(),
  `due_date` date NOT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `status` enum('New','Progressing','Done') NOT NULL DEFAULT 'New',
  `vehicle_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `currency` enum('BGN','USD','EUR') NOT NULL DEFAULT 'BGN',
  `currency_multiplier` decimal(3,2) NOT NULL DEFAULT 1.00,
  PRIMARY KEY (`id`),
  KEY `fk_orders_vehicle1_idx` (`vehicle_id`),
  KEY `fk_orders_users1_idx` (`users_id`),
  CONSTRAINT `fk_orders_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_vehicle1` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'2021-06-10','2021-06-11','Urgent ',0,'Done',1,1,'USD',0.62),(2,'2021-06-10','2021-06-11','Check the coolant leak as well',0,'New',2,1,'BGN',1.00),(3,'2021-06-10','2021-06-11','High priority',0,'New',3,1,'BGN',1.00),(4,'2021-06-10','2021-06-11','Old car, check brake lines too',0,'Done',5,1,'BGN',1.00),(5,'2021-06-10','2021-06-10','Customer needs his car in 3 hours and he\'s paying for it',0,'Done',5,1,'BGN',1.00),(6,'2021-06-10','2021-06-11','The car may need more refrigerant',0,'Done',1,1,'EUR',0.51),(7,'2021-06-10','2021-06-11','Check engine light won\'t go off',0,'Done',1,1,'EUR',0.51),(8,'2021-06-10','2021-06-12','Customer brought his own cable, tax less',0,'Progressing',5,1,'BGN',1.00);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_have_services`
--

DROP TABLE IF EXISTS `orders_have_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_have_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `services_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_has_services_orders1_idx` (`orders_id`),
  KEY `fk_orders_has_services_services1_idx` (`services_id`),
  CONSTRAINT `fk_orders_has_services_orders1` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_has_services_services1` FOREIGN KEY (`services_id`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_have_services`
--

LOCK TABLES `orders_have_services` WRITE;
/*!40000 ALTER TABLE `orders_have_services` DISABLE KEYS */;
INSERT INTO `orders_have_services` VALUES (1,1,2),(2,2,3),(3,2,6),(4,3,3),(5,4,14),(6,4,16),(7,5,9),(8,6,6),(9,7,4),(10,8,9);
/*!40000 ALTER TABLE `orders_have_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reset_tokens`
--

DROP TABLE IF EXISTS `reset_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reset_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_time` datetime NOT NULL DEFAULT current_timestamp(),
  `expiry_time` datetime NOT NULL DEFAULT (current_timestamp() + interval 1 hour),
  `token` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_reset_passwords_users_idx` (`user_id`),
  CONSTRAINT `fk_reset_passwords_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reset_tokens`
--

LOCK TABLES `reset_tokens` WRITE;
/*!40000 ALTER TABLE `reset_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `reset_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` int(6) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'Oil change',30,0,'Pick only the highest quaility oil and change it on every 10 000 kilometres to extend your engine\'s life.'),(2,'Tyre change',40,0,'Change the tyres of your car for best performance and safety throughout the different seasons.'),(3,'Battery replacement',15,0,'Your car won\'t start in the cold weather? We can test your battery charge and change the battery for you.'),(4,'Diagnostics',20,0,'Ugh, that scary \"Check Engine\" light came on again? Lets run a quick diagnostic test and find the issue.'),(5,'Brake pads replacement',40,0,'If you hear that squeaky sound each time you stop on traffic lights, then its about time to change those brake pads.'),(6,'Air conditioner repair',70,0,'The summer is ready for you, but your car isn\'t? Let\'s see what is wrong with your A/C and cool the temperatures down.'),(7,'Suspension maintenance',100,0,'Going through bumps makes you feel like your car is about to fall apart or jump like a rubber ball? Maintaned suspension is the main key to a safe trip. '),(8,'Headlights replacement',120,0,'Headlights wear out as the time passes. Make sure to have yourself equiped with good lights so you are prepared for that late night trip.'),(9,'Throttle cable repair',30,0,'Your gas pedal feels loose? That 20 year old cable needs replacement, especially if you often push your car to the limits.'),(10,'Alternator repair',50,0,'Alternator parts experience a lot of friction and that causes the voltage output to decrease, risking the life of your new battery.'),(11,'Airbag maintenance',60,0,'Safety first! Sometimes you need to prepare for the worst, so always have all your airbags fully functional.'),(12,'Doorlock repair',35,0,'Noone wants to wake up just to find out their car is missing. It\'s time to repair that door lock you have been ignoring.'),(13,'Sunroof maintenance',150,0,'The rubber seals on your sunroof get burned by the sun as the years pass. Lets change those and stop turning your car into an aquarium every time it rains.'),(14,'Headgasket change',300,0,'You floored the car only three times and it already starts overheating? Blown headgaskets are a common issue, so save your engine\'s life on time.'),(15,'Heater maintenance',130,0,'We will disassemble the whole dashboard, fix all the vents, pipes and flaps so you can have warm feet in the winter.'),(16,'Fuel system maintenance',80,0,'Don\'t waste your fuel on the driveway or risk getting yourself on fire from the fuel line leaks.'),(17,'Mirror sensors replacement',40,0,'Mirror sensors fail, causing your mirrors to stop reacting to the adjuster. A few fine touches and they will be back on track.'),(18,'Water pump replacement',60,0,'Always make sure to have the water pump working perfectly, otherwise you will overheat your car.'),(19,'Radiator replacement',60,0,'Changed the water pump and the head gasket, but your coolant keeps disappearing and the car keeps trying to overheat. Radiators are fundamental for the cooling system.'),(20,'A/C recharge',120,0,'Refrigerant expires as you use your A/C, so prepare for the summer on time.'),(21,'Exhaust welding',90,0,'Hot gasses can sometimes create holes in your exhaust system or destroy the insides of your muffler, making your car sound awfully loud.'),(22,'Hydraulic pump change',40,0,'If your steering wheel feels stiffer than usual while taking turns and your car whines in a strange way, then your hydraulic pump has failed.'),(23,'Bearing replacement',75,0,'Wheel bearings need to be maintaned in order to prevent the wheels from bloking while moving with high speed.'),(29,'Rims polishment',180,0,'Bring back the shine of your old rusty rims.');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(10) NOT NULL,
  `password` varchar(500) NOT NULL,
  `role` enum('employee','customer') NOT NULL DEFAULT 'customer',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `addresses_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_addresses_idx` (`addresses_id`),
  CONSTRAINT `fk_users_addresses` FOREIGN KEY (`addresses_id`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Ivo','Kovachev','ivo.kovachev99@gmail.com','0897685979','$2b$10$J9BGGdq5AqNXwYkja85hb.5YJdZOOXeH/2RBBCxUjEyBjERhvrpVW','employee',0,1),(2,'Teresa','Hristova','terry92@gmail.com','0887887881','$2b$10$tURs86F8WFdSroqgqLftLOElPqydiWdI8XTpnBnS9K2fEAN.UZ2qy','customer',0,2),(3,'Georgi','Hristov','georgi_hristov15@gmail.com','0877134456','$2b$10$2nz1Eg2vO.B3P995kj2jMOxD6KrbKB0VDHy1lWl8krCyPxyCO/aXG','customer',0,3),(4,'Mariya','Todorova','m.todorova1@abv.bg','0899132321','$2b$10$T2RtyOOxPiHy2P5008iqpuid8Q.X75U7KOEvZZIj8gUYEEMJ9imuW','customer',0,4),(5,'Grigov','Ivanov','grisho93@mail.bg','0988412316','$2b$10$Av654Xvth06n2s06/ZH4iueUA1kGUtWN17zxWwqS6wj.pNEC/zoj6','customer',0,5),(6,'Hristo','Georgiev','nymst36@gmail.com','0988543385','$2b$10$miMFpDhCB3t1Zy800Ao0BOAHEkVO5bqhyvIqBJtyBaQj1YV0wbzhe','customer',0,6);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_plate` varchar(45) NOT NULL,
  `year` year(4) NOT NULL,
  `class` enum('A','B','C','D','E','F','S') NOT NULL,
  `vin` varchar(17) NOT NULL,
  `transmissions` enum('manual','auto') NOT NULL,
  `engine` enum('petrol','diesel','hybrid') NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `models_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vin_UNIQUE` (`vin`),
  KEY `fk_vehicle_models1_idx` (`models_id`),
  KEY `FK_vehicle_users` (`customer_id`),
  CONSTRAINT `FK_vehicle_users` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_vehicle_models1` FOREIGN KEY (`models_id`) REFERENCES `models` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicles`
--

LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` VALUES (1,'M0144BT',2003,'C','RVHBT51GP4U205563','manual','petrol',0,355,1),(2,'BP2010TP',1999,'A','RVHBT51GP4U205514','manual','petrol',0,276,3),(3,'B1332PT',2000,'B','RVHBT51GP4U205524','manual','hybrid',0,16,4),(4,'M1452AP',2000,'E','RVHBT51GP4U205509','manual','petrol',0,159,2),(5,'P3409BP',1989,'E','RVHBT51GP4U205594','manual','petrol',0,353,6);
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-10  8:16:36
